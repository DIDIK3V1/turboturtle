/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Katarina, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/#include "mainwindow.h"

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QLabel>
#include <QStyle>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setWindowIcon(QIcon("image/logo_turboturtle.ico"));

    QTranslator translator;
    const QStringList ui_languages = QLocale::system().uiLanguages();
    for (const QString &locale : ui_languages) {
        const QString base_name = "ihm_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + base_name)) {
            a.installTranslator(&translator);
            break;
        }
    }
    qApp->setStyle("fusion");
#ifdef Q_OS_WIN64
    qApp->setPalette(qApp->style()->standardPalette());
#endif

    MainWindow w;

    w.setWindowTitle("TurboTurtle");
    w.setWindowState(w.windowState() ^ Qt::WindowMaximized);

    w.show();
    w.set_first_picture();
    return a.exec();
}
