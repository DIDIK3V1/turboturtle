/**

@author Mattia Basaglia

@section License

    Copyright (C) 2013-2014 Mattia Basaglia

    This file is part of TurboTurtle.

    TurboTurtle is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TurboTurtle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TurboTurtle.  If not, see http://www.gnu.org/licenses/.
*/


#include "../include/color_wheel.hpp"

#include <QMouseEvent>
#include <QPainter>
#include <QLineF>
#include <qmath.h>

namespace lib_color_selector{
enum MouseStatus
{
    Nothing,
    Drag_Circle,
    Drag_Square
};

class ColorWheel::Private
{

private:

    ColorWheel * const w;

public:

    qreal huem, sat, val;
    unsigned int wheel_width;
    MouseStatus mouse_status;
    QPixmap hue_ring;
    QImage sat_val_square;

    Private(ColorWheel *widget)
        : w(widget), huem(0), sat(0), val(0),
        wheel_width(20), mouse_status(Nothing)
    { }

    /// Calculate outer wheel radius from idget center
    qreal outer_radius() const
    {
        return qMin(w->geometry().width(), w->geometry().height())/2;
    }

    /// Calculate inner wheel radius from idget center
    qreal inner_radius() const
    {
        return outer_radius()-wheel_width;
    }

    /// Calculate the edge length of the inner square
    qreal square_size() const
    {
        return inner_radius()*qSqrt(2);
    }

    /// return line from center to given point
    QLineF line_to_point(const QPoint &p) const
    {
        return QLineF (w->geometry().width()/2, w->geometry().height()/2, p.x(), p.y());
    }

    /// Updates the inner square that displays the saturation-value selector
    void render_rectangle()
    {
        int sz = square_size();
        sat_val_square = QImage(sz, sz, QImage::Format_RGB32);

        for(int i = 0; i < sz; ++i)
        {
            for(int j = 0;j < sz; ++j)
            {
                sat_val_square.setPixel( i,j,
                        QColor::fromHsvF(huem,double(i)/sz,double(j)/sz).rgb());
            }
        }
    }

    /// Updates the outer ring that displays the hue selector
    void render_ring()
    {
        hue_ring = QPixmap(outer_radius()*2,outer_radius()*2);
        hue_ring.fill(Qt::transparent);
        QPainter painter(&hue_ring);
        painter.setRenderHint(QPainter::Antialiasing);
        painter.setCompositionMode(QPainter::CompositionMode_Source);


        const int hue_stops = 24;
        static QConicalGradient gradient_hue(0, 0, 0);
        if ( gradient_hue.stops().size() < hue_stops )
        {
            for ( double a = 0; a < 1.0; a+=1.0/(hue_stops-1) )
            {
                gradient_hue.setColorAt(a,QColor::fromHsvF(a,1,1));
            }
            gradient_hue.setColorAt(1,QColor::fromHsvF(0,1,1));
        }

        painter.translate(outer_radius(),outer_radius());

        painter.setPen(Qt::NoPen);
        painter.setBrush(QBrush(gradient_hue));
        painter.drawEllipse(QPointF(0,0),outer_radius(),outer_radius());

        painter.setBrush(Qt::transparent);//palette().background());
        painter.drawEllipse(QPointF(0,0),inner_radius(),inner_radius());
    }
};

ColorWheel::ColorWheel(QWidget *parent) :
    QWidget(parent), m_p(new Private(this))
{}

ColorWheel::~ColorWheel()
{
    delete m_p;
}

QColor ColorWheel::color() const
{
    return QColor::fromHsvF(m_p->huem, m_p->sat, m_p->val);
}

QSize ColorWheel::size_hint() const
{
    return QSize(m_p->wheel_width*5, m_p->wheel_width*5);
}

qreal ColorWheel::hue() const
{
    return m_p->huem;
}

qreal ColorWheel::saturation() const
{
    return m_p->sat;
}

qreal ColorWheel::value() const
{
    return m_p->val;
}

unsigned int ColorWheel::wheel_width() const
{
    return m_p->wheel_width;
}

void ColorWheel::set_wheel_width(unsigned int w)
{
    m_p->wheel_width = w;
    m_p->render_rectangle();
    update();
}

void ColorWheel::paintEvent(QPaintEvent *)
{
    double selector_w = 6;

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.translate(geometry().width()/2,geometry().height()/2);

    // hue wheel
    if(m_p->hue_ring.isNull())
        m_p->render_ring();

    painter.drawPixmap(-m_p->outer_radius(), -m_p->outer_radius(), m_p->hue_ring);

    // hue selector
    painter.setPen(QPen(Qt::black,3));
    painter.setBrush(Qt::NoBrush);
    QLineF ray(0, 0, m_p->outer_radius(), 0);
    ray.setAngle(m_p->huem*360);
    QPointF h1 = ray.p2();
    ray.setLength(m_p->inner_radius());
    QPointF h2 = ray.p2();
    painter.drawLine(h1,h2);

    // lum-sat square
    if(m_p->sat_val_square.isNull())
        m_p->render_rectangle();

    painter.rotate(-m_p->huem*360-45);
    ray.setLength(m_p->inner_radius());
    ray.setAngle(135);
    painter.drawImage(ray.p2(), m_p->sat_val_square);

    // lum-sat selector
    //painter.rotate(135);
    painter.setPen(QPen(m_p->val > 0.5 ? Qt::black : Qt::white, 3));
    painter.setBrush(Qt::NoBrush);
    double max_dist = m_p->square_size();
    painter.drawEllipse(QPointF(m_p->sat*max_dist-max_dist/2,
                                m_p->val*max_dist-max_dist/2
                               ),
                        selector_w, selector_w);
}

void ColorWheel::mouseMoveEvent(QMouseEvent *ev)
{
    if (m_p->mouse_status == Drag_Circle )
    {
        m_p->huem = m_p->line_to_point(ev->pos()).angle()/360.0;
        m_p->render_rectangle();

        emit color_selected(color());
        emit color_changed(color());
        emit on_drag();
        update();
    }
    else if(m_p->mouse_status == Drag_Square)
    {
        QLineF glob_mouse_ln = m_p->line_to_point(ev->pos());
        QLineF center_mouse_ln ( QPointF(0,0),
                                 glob_mouse_ln.p2() - glob_mouse_ln.p1() );
        center_mouse_ln.setAngle(center_mouse_ln.angle()-m_p->huem*360-45);

        m_p->sat = qBound(0.0, center_mouse_ln.x2()/m_p->square_size()+0.5, 1.0);

        m_p->val = qBound(0.0, center_mouse_ln.y2()/m_p->square_size()+0.5, 1.0);

        emit color_selected(color());
        emit color_changed(color());
        emit on_drag();

        update();
    }
}

void ColorWheel::mousePressEvent(QMouseEvent *ev)
{
    if ( ev->buttons() &Qt::LeftButton )
    {
        QLineF ray = m_p->line_to_point(ev->pos());
        if ( ray.length() <=m_p->inner_radius() )
            m_p->mouse_status = Drag_Square;
        else if ( ray.length() <= m_p->outer_radius() )
            m_p->mouse_status = Drag_Circle;
    }
}

void ColorWheel::mouseReleaseEvent(QMouseEvent *ev)
{
    if (m_p->mouse_status == Drag_Circle )
    {
        m_p->huem = m_p->line_to_point(ev->pos()).angle()/360.0;
        m_p->render_rectangle();

        emit color_selected(color());
        emit color_changed(color());
        emit on_drag();
        emit on_click_release();
        update();
    }
    else if(m_p->mouse_status == Drag_Square)
    {
        QLineF glob_mouse_ln = m_p->line_to_point(ev->pos());
        QLineF center_mouse_ln ( QPointF(0,0),
                                 glob_mouse_ln.p2() - glob_mouse_ln.p1() );
        center_mouse_ln.setAngle(center_mouse_ln.angle()-m_p->huem*360-45);

        m_p->sat = qBound(0.0, center_mouse_ln.x2()/m_p->square_size()+0.5, 1.0);

        m_p->val = qBound(0.0, center_mouse_ln.y2()/m_p->square_size()+0.5, 1.0);

        emit color_selected(color());
        emit color_changed(color());
        emit on_drag();
        emit on_click_release();
        update();
    }

}

void ColorWheel::resizeEvent(QResizeEvent *)
{
    m_p->render_ring();
    m_p->render_rectangle();
}

void ColorWheel::set_color(const QColor &c)
{
    qreal oldh = m_p->huem;
    m_p->huem = qMax(0.0, c.hueF());
    m_p->sat = c.saturationF();
    m_p->val = c.valueF();
    if (!qFuzzyCompare(oldh+1, m_p->huem+1))
        m_p->render_rectangle();
    update();
    emit color_changed(c);
}

void ColorWheel::set_hue(qreal h)
{
    m_p->huem = qBound(0.0, h, 1.0);
    m_p->render_rectangle();
    update();
}

void ColorWheel::set_saturation(qreal s)
{
    m_p->sat = qBound(0.0, s, 1.0);
    update();
}

void ColorWheel::set_value(qreal v)
{
    m_p->val = qBound(0.0, v, 1.0);
    update();
}

}
