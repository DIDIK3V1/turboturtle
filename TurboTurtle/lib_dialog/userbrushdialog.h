/**
 @authors Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef USERBRUSHDIALOG_H
#define USERBRUSHDIALOG_H

#include <QDialog>
#include <QDir>
#include "drawingtools/drawingtools.h"
#include "drawingtools/brush.h"
#include "lib_dialog/widgets/userbrushbutton.h"

namespace Ui {
    class userBrushDialog;
}

//namespace lib_dialog {

    class userBrushDialog : public QDialog {
    Q_OBJECT

    public:
        /**
             * @brief Constructor of userBrushDialog
             * @param parent of the class
             */
        explicit userBrushDialog(QWidget *parent = nullptr);

        ~userBrushDialog();

    private slots:
        /**
         * @brief This slot will ask the user to choose a brush in his files and create a brush button for it
         */
        void on_addButton_clicked();

        /**
         * @brief This slot will take the brush you have selected as the brush to use in the canvas
         */
        void on_butOk_clicked();

        /**
         * @brief This slot will delete the selected brush (if it exist)
         */
        void on_butDelete_clicked();

        /**
         * @brief Is called by every brush button when clicked, indicate wich brush
         * is selected for the ok and delete button
         */
        void selectButton();

    signals:

        /**
         * @brief Send the selected brush to the drawingtools class
         * @param p the path of the selected brush
         */
        void user_brush_selected(QString p);

    private:

        /**
         * @brief This function will copy the brush image given and create the associated brush
         * @param file the path to the selected brush image
         */
        void addBrush(QString file);

        /**
         * @brief This function will get all of the brushes previously added and their associated button
         */
        void getAllBrushes();

        /**
         * @brief ui;
         */
        Ui::userBrushDialog *ui;

        /**
         * @brief the current number of buttons (and brushes) the window have to display
         */
        int m_numButtons;

        /**
         * @brief The currently selected button used by delete and ok buttons
         */
        lib_dialog::userBrushButton *m_selectedButton;

        /**
         * @brief The list of all the brushes buttons
         */
        QList<lib_dialog::userBrushButton *> m_buttons;

        /**
         * @brief The directory where all the brushes image are stored
         */
        QDir m_brush_directory;

    };

//} // namespace lib_dialog

#endif // USERBRUSHDIALOG_H
