/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Creerio, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BRUSH_H
#define BRUSH_H

#include "qevent.h"
#include "qimage.h"
#include <mutex>
#include <random>
#include <thread>

namespace lib_drawing_tools {
/**
 * @brief The drawingTool enum for which tool is selected
 */
enum DrawingTool { BRUSH, ERASER, COLORPICKER, FILLBUCKET, HANDSELECT, RECTANGLE_SELECTOR, POLYGONAL_SELECTOR, HAND };
/**

@brief Class representing a brush for painting. */
class Brush
{
public:
    /**
    *@brief Constructs a Brush object with a QImage and a number of random variations.
    *@param img The image used as the brush
    *@param nb_var Number of random variations used
    */
    Brush(const QImage &img, int nb_var = 10);
    /**
    *@brief Copy constructor.
    *@param brush The Brush object to copy from
    */
    Brush(const Brush &brush);
    /**
    *@brief Destroys the Brush object.
     */
    ~Brush();
    /**
    *@brief Gets a randomly selected QImage of the brush to draw with.
    *@return The QImage
    */
    QImage const get_brush();
    /**
    *@brief Changes the width of all brush variations, resetting the chosen color.
    *@param width Width of the brush to scale
    */
    void change_width(int width);
    /**
    *@brief Changes the color of all brush variations.
    *@param color Color chosen for the brush
    */
    void change_color(const QColor &color);
    /**
    *@brief Rotates the brush based on the position of the stylus (tilt).
    *@param event TabletEvent to compute the pencil values
    */
    void rotate(const QTabletEvent *event);
    /**
    *@brief Changes the color of the brush and rotates it based on the position of the stylus (tilt).
    *@param color The color chosen for the brush
    *@param event TabletEvent to compute the pencil values
    */
    void tablet_change_color_and_rotate(const QColor &color, const QTabletEvent *event);
    /**
    *@brief Gets the border of the brush to be displayed on the canvas.
    *@return The brush border as a QImage
    */
    QImage const brush_border() const { return m_brush_border; }
    /**
    *@brief Gets the QRect of the brush.
    *@return The QRect of the brush
    */
    QRect const rect() { return get_brush().rect(); }
    /**
    *@brief Overloaded operator=.
    *@param brush The Brush object to copy
    *@return The copied Brush object or this
    */
    Brush operator=(const Brush &brush);
    /**
    *@brief Scales the cursor brush according to the pen pressure.
    *@param event TabletEvent to compute the pencil values
    *@param width Width of the brush to scale
    */
    void tablet_scale(const QTabletEvent *event, int width);
    /**
    *@brief Changes the color of the brush and rotates it based on the position of the stylus (tilt), while scaling it based on pen pressure.
    *@param color The color chosen for the brush
    */
    void rotate_scale_change_color(const QColor &color);
    /**
    *@brief Rotates the brush using threads.
    *@param uncolored uncolored brush
    *@param colored colored brush
    */
    void compute_rotation(QImage *uncolored, QImage *colored);
    /**
    *@brief Gets the border of the brush with color.
    *@param color The color to use for the brush border
    */
    void extract_border(const QColor &color);

private:
    /**
     * @brief m_random_brushes Vector of randomly generated brushes
     */
    std::vector<QImage> m_random_brushes;
    /**
     * @brief m_current_uncolored_brushes Vector of current uncolored brushes.
     */
    std::vector<QImage> m_current_uncolored_brushes;
    /**
     * @brief m_current_colored_brushes Vector of current colored brushes.
     */
    std::vector<QImage> m_current_colored_brushes;
    /**
     * @brief m_brush_border The brush border to be displayed on the canvas.
     */
    QImage m_brush_border;
    /**
     * @brief m_nb_generated Number of generated variations.
     */
    int m_nb_generated;
    /**
     * @brief m_current_color The current brush color.
     */
    QColor m_current_color;
    /**
     * @brief m_current_width The current brush width.
     */
    int m_current_width;
    /**
     * @brief m_current_rotation The current brush rotation angle.
     */
    int m_current_rotation;
    /**
     * @brief m_nb_threads The number of threads used for the brush.
     */
    int m_nb_threads;
    /**
     * @brief m_threads  Vector of threads used for the brush.
     */
    std::vector<std::thread*> m_threads;

    /**
    * @brief Computes and returns the size of the brush depending on its pressure.
    * @param event TabletEvent to compute the pencil values
    * @param width Width of the brush
    * @return A qreal representing the size of the brush
    */
    qreal pressure_to_width(const QTabletEvent *event, int width);

    /**
    * @brief Private method to rotate the brush.
    */
    void private_rotate();

    /**
    * @brief Private method to change the brush color.
    */
    void private_change_color();

    /**
    * @brief Private method to change the width of the brush.
    */
    void private_change_width();

    /**
    * @brief Computes the brush by changing its color, scaling it, and coloring it.
    * @param brush The QImage of the brush
    * @param retBrush Pointer to the new QImage of the brush
    * @param retScaled Pointer to the new scaled QImage of the brush
    * @param retColored Pointer to the new colored QImage of the brush
   */
    void compute_brush(const QImage &brush, QImage *retBrush, QImage *retScaled, QImage *retColored);

    /**
    * @brief Colors the brush.
    * @param uncolored Pointer to the uncolored QImage of the brush
    * @param colored Pointer to the colored QImage of the brush
    */
    void compute_brush_color(QImage *uncolored, QImage *colored);
};

} // namespace lib_drawing_tools

#endif // BRUSH_H
