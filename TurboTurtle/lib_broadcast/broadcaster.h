/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Aurélien LAPLAUD, Katarina

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIB_BROADCAST_BROADCASTER_H
#define LIB_BROADCAST_BROADCASTER_H

#include <QObject>
#include <QTimer>
#include "lib_animation/animation.h"
#include "lib_broadcast/lib_ndi/ndimanager.h"
#include "lib_canvas/frame.h"
#include <optional>

namespace lib_broadcast {
/**
 * @brief The BroadCaster class used to stream via ndi and via an another window
 */
class BroadCaster : public QObject
{
    Q_OBJECT
public:

    /**
     * @brief Enumerator defining the implemented broadcasting modes
     */
    enum broadcast_mode {SEQUENTIAL, COMPOSITION};

    /**
     * @brief Constructor of the class
     * @param parent of the broadcaster
     */
    BroadCaster(QObject *parent = nullptr);

    /**
     * @brief Constructor by copy deleted
     */
    BroadCaster(const BroadCaster &) = delete;
    /**
     * @brief Operator deleted.
     * @return
     */
    BroadCaster &operator=(const BroadCaster &) = delete;
    /**
    * @brief Destructor of the class
    */
    ~BroadCaster();

    void set_broadcast_mode(broadcast_mode mode);
    void set_composing_mode(QPainter::CompositionMode mode);
    void start_stop_ndi(const bool state);


public slots:
    /**
     * @brief Start stop broadacast and set framerate.
     * @param Frame per sec wanted
     */
    void start_stop_broadcast(int fps);
    /**
     * @brief Starts and stop live drawing
     * @param State of the live_drawing
     */
    void start_stop_live_drawing(bool state);
    /**
     * @brief Send new frame to broadcast
     */
    void update_live_broadcast();
    /**
     * @brief Set the new list of frames; the list is composed of one black frame if anim == std::nullopt
     * @param Contains an animation or std::nullopt
     */
    void process_frames_from_cb(const std::optional<std::vector<lib_animation::Animation*>> &anim);
    /**
     * @brief Modify the current frame to broadcast it
     * @param Current frame
     */
    void update_live_drawing(lib_frame::Frame &frame);

private:

    /**
     * @brief defines the broadcasting mode of animations
     */
    broadcast_mode m_broadcast_mode;

    /**
     * @brief based on the QPainter composition enum
     */
    QPainter::CompositionMode m_composition_mode;

    /**
     * @brief true if broadcasting mode is set to composition
     */
    bool m_is_composition_broadcast;

    /**
     * @brief logic of the composition broadcast mode, creates new frames
     * @param vector of all animations in the composing bench
     */
    void frame_composition(const std::vector<lib_animation::Animation*> &animations);

    /**
     * @brief Called to delete frames created by the broadcaster
     */
    void delete_broadcast_frames();

    /**
     * @brief Defines the frame time of each frame for live broadcast
     */
    int m_frame_time;

    /**
     * @brief Contains the necessary to stream an image via ndi
     */
    lib_ndi::ndimanager *m_ndi;

    /**
     * @brief Current position inside the animation
     */
    size_t m_pos_anim;
    /**
     * @brief List of frames reprensenting an animation
     */
    lib_animation::Animation m_animation;
    /**
     * @brief Default frame created only to have a blackscreen when needed
     */
    lib_frame::Frame *m_blackscreen;

    /**
     * @brief pointer to the current frame to broadcast
     */
    lib_frame::Frame *m_frame_to_send;
    /**
     * @brief Bool used to know if we're living
     */
    bool m_is_live;
    /**
     * @brief Used to update frames
     */
    QTimer *m_live_timer;

    /**
     * @brief m_ndi_state to get ndi state
     */
    bool m_ndi_state;
signals:
    /**
     * @brief Signal emited to ask the composing bench for frames
     */
    void ask_for_frames();
    /**
     * @brief Signal emited when start_stop_broadast is called
     * @param State of the broadast
     */
    void anim_started(bool state);
    /**
     * @brief Signal emited to ask the canvas for the current_frames
     */
    void get_canvas_frame();
};

} // namespace lib_broadcast

#endif // LIB_BROADCAST_BROADCASTER_H
