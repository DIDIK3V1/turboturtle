/**
 @author Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QPushButton>
#include <QApplication>

namespace lib_widgets {

    /**
     * @brief Button with an image
     */
    class ImageButton : public QPushButton {
        Q_OBJECT

    public:
        /**
         * @brief Default constructor, no image
         * @param parent Parent of the object
         */
        explicit ImageButton(QString mime_type = "", QWidget *parent = nullptr, const int pos = -1);

        // Disable child induced constructors
        explicit ImageButton(const QString &text, QWidget *parent = nullptr) = delete;
        ImageButton(const QIcon &icon, const QString &text, QWidget *parent = nullptr) = delete;

        /**
         * @brief Getter used to have the button's thumbnail
         */
        inline const QImage get_thumbnail() const;

        /**
         * @brief Setter used to change the button's thumbnail
         * @param thumbnail Thumbnail to use
         */
        void set_thumbnail(const QImage &thumbnail);

        /**
         * @brief Returns the position of the ImageButton in the storing vector
         * @return Position in the vector storing it
         */
        const int get_pos() const;

        /**
         * @brief Returns the position of the ImageButton in the storing vector
         * @param new_pos New position in it's storing vector
         */
        void set_pos(const size_t &new_pos);

        /**
         * @brief Used to set a border effect on the button
         * @param enabled If it should be enabled or not
         */
        void set_border(const bool &enabled);

        /**
         * @brief Gives the MIME type of the current class
         * @return String corresponding to the MIME type stored
         */
        const QString get_mime_type() const;

        /**
         * @brief Used for logs
         * @return Name of the class
         */
        virtual const std::string get_name() const = 0;

    private:
        /**
         * @brief Button thumbnail
         */
        QImage m_thumbnail;

        /**
         * @brief Item position in the vector
         */
        int position;

        /**
         * @brief Mime type used by the button
         */
        QString m_mime_type;

        QPoint m_dragStartPosition;

    private slots:
        /**
         * @brief Slot called when the mouse does some things
         * @param e Event given by QT
         */
        void mouseReleaseEvent(QMouseEvent *e);
        void mouseMoveEvent(QMouseEvent *e);

    signals:
        /**
         * @brief Signal launched when a left click is made (by mousePressEvent)
         * @param pos Position of the ImageButton in the vector
         */
        void on_left_clicked(const int pos);

        /**
         * @brief Signal launched when a middle click is made (by mousePressEvent)
         * @param pos Position of the ImageButton in the vector
         */
        void on_middle_click(const int pos);

        /**
         * @brief Signal launched when a right click is made (by mousePressEvent)
         * @param pos Position of the ImageButton in the vector
         */
        void on_right_click(const QPointF mousePosition, const int pos);

        /**
         * @brief Signal launched when a ctrl + left click is made (by mousePressEvent)
         * @param pos Position of the ImageButton in the vector
         */
        void on_ctrl_left_clicked(const int pos);
    };
}

