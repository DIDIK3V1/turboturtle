/**
 @authors Creerio, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ANIMATIONLIBRARY_H
#define ANIMATIONLIBRARY_H

#include <QTimer>
#include <QThread>
#include "lib_widgets/templates/templatedhlayout.h"
#include "lib_animation/animation.h"
#include "lib_library/widgets/animationbutton.h"

namespace lib_library {
    class AnimationLayout : public lib_widgets::TemplatedHLayout<lib_library::AnimationButton, lib_animation::Animation>
    {
    Q_OBJECT

    public :
        explicit AnimationLayout(QWidget *parent = nullptr);
        ~AnimationLayout();

        // Overwrites priority to these implementations
        const bool add(lib_animation::Animation *anim) override;

        const bool insert(lib_animation::Animation *anim, const size_t pos) override;

        const bool remove(const int &pos) override;

        const bool move_item(const size_t &from, size_t to) override;

        void clear(const bool is_delete = false) override;

        void unselect(const int pos) override;

        /**
         * Used to launch the animation_to_cb signal, sending an animation to the composing bench
         * Will not do anything, should the position be out of range
         *
         * @param pos
         * Position of the animation to send
         */
        void send_animation_to_cb(int &pos);

        /**
         * @brief Loads the last animation
         */
        void load_last_animation();

        const std::string get_name() const final
        {
            return "AnimationLayout";
        }
        /**
         * @brief save all the animation on the computer
         */
        void save_all_png();

        /**
         * @brief save all the animations as animated GIFs
         */
        void save_all_gif();

        /**
         * @brief save_session Saves the current session to a provided folder
         * @param zip_path Path to the zip file to write to
         */
        void save_session(const QString &zip_path);

        /**
         * @brief load_session Loads a session from a provided zip file
         * @param zip_path Path to the zip file to read
         */
        void load_session(const QString &zip_path);

        /**
         * @brief  import an animation from a folder
         * @param import_folder the path of the folder containing the images
         * @param is_backup Indicates if this import is from a backup, to avoid selecting an animation
         */
        void import_from_png(const QString &import_folder, const bool is_backup = false);

        /**
         * @brief import_from_gif import an animation from a gif file
         * @param image_file Path to the file
         */
        void import_from_gif(const QString &image_file);

        /**
         * @brief import_from_session import an animation from a gif file
         * @param image_file Path to the session file
         */
        void import_from_session(const QString &session_file);

    protected slots:
        // Overrides to launch a backup of the animation
        void save_loaded_item(QWidget *item);

    private:

        /**
         * @brief Timer used to backup the animations
         */
        QTimer backup_timer;

        /**
         * @brief backupAnimations Makes a backup of the current animations
         */
        void backupAnimations();

        /**
         * @brief Empties the backup_location folder
         */
        void clear_backup_animations();

        /**
         * @brief backup_mutex Mutex used when doing a backup, avoids unloading an animation while a backup is done
         */
        std::mutex backup_mutex;

    signals:
        /**
         * @brief Signal sent to the composing bench, contains the animation and it's current position
         * @param anim Pointer to the animation
         * @param pos Animation position
         */
        void animation_to_cb(lib_animation::Animation *anim, int &pos);

        /**
         * @brief Signal sent to the composing bench to remove the animation from it since it uses a pointer
         * @param pos Animation position
         */
        void animation_removed_to_cb(const int &pos);

        /**
         * @brief Signal sent to the composing bench to update the animation id when it is moved
         * @param old_pos Old position
         * @param new_pos New position
         */
        void animation_new_id_to_cb(const size_t &old_pos, const size_t &new_pos);

    };
}


#endif // ANIMATIONLIBRARY_H
