/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Creerio, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "composingbench.h"
#include "lib_composing_bench/broadcastbutton.h"
#include "qdebug.h"
#include "qevent.h"
#include <QMessageBox>
#include "qmimedata.h"
#include "qstylepainter.h"
#include <QTimer>
#include "ui_composingbench.h"
#include <opencv2/opencv.hpp>

namespace lib_composing_bench {

ComposingBench::ComposingBench(QWidget *parent)
    : QWidget(parent)
    , m_ui(new Ui::ComposingBench)
    , m_broadcaster(new lib_broadcast::BroadCaster)
{
    m_ui->setupUi(this);
    m_hovered = false;
    setAcceptDrops(true);
    m_is_live = false;
    m_is_live_drawing = false;
    m_fps = 12;
    m_ui->radBut12fps->setChecked(true);
    QObject::connect(this,
                     &lib_composing_bench::ComposingBench::start_broadcast,
                     m_broadcaster,
                     &lib_broadcast::BroadCaster::start_stop_broadcast);

    QObject::connect(m_broadcaster,
                     &lib_broadcast::BroadCaster::ask_for_frames,
                     this,
                     &lib_composing_bench::ComposingBench::get_asked_for_frames);

    QObject::connect(this,
                     &lib_composing_bench::ComposingBench::frames,
                     m_broadcaster,
                     &lib_broadcast::BroadCaster::process_frames_from_cb);

    QObject::connect(m_broadcaster,
                     &lib_broadcast::BroadCaster::get_canvas_frame,
                     this,
                     &lib_composing_bench::ComposingBench::ask_frame_live_drawing);

    QObject::connect(this,
                     &lib_composing_bench::ComposingBench::send_frame,
                     m_broadcaster,
                     &lib_broadcast::BroadCaster::update_live_drawing);
}

ComposingBench::~ComposingBench()
{
    qDebug() << "trying to delete Composing Bench";
    send_ndi_state(false);
    delete m_ui;
    m_broadcaster->deleteLater();
    //delete m_broadcaster;
    qDebug() << "composingBench deleted correctly";
}

void ComposingBench::paint(QPainter &painter, const QRect &rect) const
{
    if (m_hovered) {
        QColor tmpColor = this->palette().color(QPalette::Highlight);
        painter.fillRect(1, 1, rect.width(), rect.height(), tmpColor);
    }
}

void ComposingBench::paintEvent(QPaintEvent *)
{
    QStylePainter painter(this);
    paint(painter, geometry());
}

void ComposingBench::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-anim-data")) {
        m_hovered = true;
        qDebug("composingBench : composingBench survolé");
        update();
        event->acceptProposedAction();
    }
}

void ComposingBench::dragLeaveEvent(QDragLeaveEvent *event)
{
    m_hovered = false;
    qDebug("composingBench : composingBench Quitté");
    update();
    event->ignore();
}

void ComposingBench::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-anim-data")) {
        QByteArray frameData = event->mimeData()->data("application/x-anim-data");
        QDataStream dataStream(&frameData, QIODevice::ReadOnly);
        int animNb;
        dataStream >> animNb;
        qDebug() << "anim" << animNb << "dropped";
        m_hovered = false;
        update();
        event->acceptProposedAction();
        emit get_anim(animNb);
        if (m_is_live_drawing) {
            emit stop_live_drawing();
        }
        if (m_is_live) {
            emit start_broadcast(m_fps);
            emit start_broadcast(m_fps);
        } else {
            emit start_broadcast(m_fps);
            m_is_live = true;
        }

        //lib_animation::Animation = BroadcastButton bb = new BroadcastButton();
    }
}

void ComposingBench::get_animation(lib_animation::Animation *anim, int &animNb)
{
    qDebug() << "Composing_Bench : anim " << animNb << " created";

    BroadcastButton *bb = new BroadcastButton(anim, animNb);
    m_animations.push_back(bb);
    m_ui->composingBenchArea->addWidget(bb);
}

void ComposingBench::remove_animation(const int &anim_id)
{
    if (!m_animations.empty()) {
        qDebug() << "Composing_Bench : Animation with id " << anim_id << " has been removed, trying to remove it from the composing bench to avoid issues";
        BroadcastButton *to_remove = nullptr;
        int pos;
        for(size_t i = 0; i < m_animations.size(); ++i){
            if (m_animations[i]->get_id() == anim_id){
                to_remove = m_animations[i];
                pos = int(i);
                break;
            }
        }

        if (to_remove != nullptr) {
            m_ui->composingBenchArea->removeWidget(to_remove);
            to_remove->hide();
            m_animations.erase(m_animations.begin() + pos);
            // No need to delete, qt owns the button, so it will delete it
        }

        // No animation left to play, basically everything has been deleted. We can let on_butDeleteAll_clicked do the rest
        if (m_animations.empty())
            on_butDeleteAll_clicked();
    }
}

void ComposingBench::update_animation(const lib_animation::Animation *anim)
{
    if (!m_animations.empty()) {
        BroadcastButton *updated = nullptr;
        for(auto & m_animation : m_animations){
            if (m_animation->get_animation() == anim){
                updated = m_animation;
                break;
            }
        }

        if (updated != nullptr) {
            qDebug() << "Composing_Bench : Found animation with id " << updated->get_id() << " calling the broadcaster to update the animations";
            emit frames(get_animation_in_bench());
        }
    }
}

void ComposingBench::update_animation_id(const size_t &old_id, const size_t &new_id)
{
    if (!m_animations.empty()) {
        BroadcastButton *to_update = nullptr;
        for(auto & m_animation : m_animations){
            if (m_animation->get_id() == old_id){
                to_update = m_animation;
                break;
            }
        }

        if (to_update != nullptr) {
            to_update->set_id(new_id);
            qDebug() << "Composing_Bench : Animation with id " << old_id << " has been updated to id " << new_id;
        }
    }
}


std::optional<std::vector<lib_animation::Animation*>> ComposingBench::get_animation_in_bench()
{
    if (!m_animations.empty()) {
        std::vector<lib_animation::Animation*> anims;
        for(size_t i = 0; i < m_animations.size(); ++i){
            anims.push_back(m_animations[i]->get_animation());
        }
        return anims;
    } else {
        return std::nullopt;
    }
}

void ComposingBench::on_butBroadcast_clicked()
{
    qDebug() << "broadcast button clicked";
    m_is_live = !m_is_live;
    emit start_broadcast(m_fps);
    //m_broadcaster->start();
}

void ComposingBench::get_asked_for_frames()
{
    emit frames(get_animation_in_bench());
}


void ComposingBench::on_radBut12fps_clicked()
{
    if (m_fps != 12) {
        m_fps = 12;
        if (m_is_live) {
            emit start_broadcast(m_fps);
            emit start_broadcast(m_fps);
        }
    }
}

void ComposingBench::on_radBut24fps_clicked()
{
    if (m_fps != 24) {
        m_fps = 24;

        if (m_is_live) {
            emit start_broadcast(m_fps);
            emit start_broadcast(m_fps);
        }
    }
}

void ComposingBench::start_stop_live_drawing(bool b)
{
    m_is_live_drawing = b;
    if (b) {
        on_butDeleteAll_clicked();
        m_broadcaster->start_stop_live_drawing(b);
    } else {
        m_broadcaster->start_stop_live_drawing(b);
    }
}

void ComposingBench::ask_frame_live_drawing()
{
    emit get_canvas_frame();
}

void ComposingBench::get_frame_live_drawing(lib_frame::Frame &frame)
{
    emit send_frame(frame);
}

void ComposingBench::on_butDeleteAll_clicked()
{
    qDebug() << "Composing_Bench : Deleting all animations loaded";
    if (m_is_live) {
        m_is_live = false;
        emit start_broadcast(m_fps);
    }
    if (!m_animations.empty()) {
        for (auto & m_animation : m_animations) {
            m_animation->get_animation()->set_used_in_composing_bench(false);
            m_ui->composingBenchArea->removeWidget(m_animation);
            m_animation->hide();
            // No need to delete, qt owns the button, so it will delete it
        }
        m_animations.clear();
    }
}

int ComposingBench::get_active_animation_number(size_t index)
{
    for (size_t i = 0; i < m_animations.size(); ++i) {
        if (index == m_animations[i]->get_id()) {
            return i;
        }
    }
    return -1;
}


void ComposingBench::on_broadcast_mode_activated(int index)
{
    switch (index)
    {
    case 0:
        m_broadcaster->set_broadcast_mode(lib_broadcast::BroadCaster::broadcast_mode::SEQUENTIAL);
        m_ui->composition_mode->setEnabled(false);
        break;

    case 1:
        m_broadcaster->set_broadcast_mode(lib_broadcast::BroadCaster::broadcast_mode::COMPOSITION);
        m_ui->composition_mode->setEnabled(true);
        break;

        default:
        break;
    }

    emit start_broadcast(m_fps);
    emit start_broadcast(m_fps);
}

void ComposingBench::send_ndi_state(const bool state)
{
    m_broadcaster->start_stop_ndi(state);
}


void ComposingBench::on_composition_mode_activated(int index)
{
    switch (index)
    {
        case 0 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_Multiply);
            break;

        case 1 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_Plus);
        break;

        case 2 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_Xor);
            break;

        case 3 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_Screen);
        break;

        case 4 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_Overlay);
            break;

        case 5 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_Darken);
        break;

        case 6 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_Lighten);
            break;

        case 7 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_ColorDodge);
        break;

        case 8 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_ColorBurn);
            break;

        case 9 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_HardLight);
        break;

        case 10 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_SoftLight);
        break;

        case 11 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_Difference);
        break;

        case 12 : m_broadcaster->set_composing_mode(QPainter::CompositionMode_Exclusion);
        break;

        default:
            break;
    }

    emit start_broadcast(m_fps);
    emit start_broadcast(m_fps);
}

} // namespace lib_composing_bench
