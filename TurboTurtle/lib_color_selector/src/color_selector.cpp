/**
@author Julien BLANCO, Samuel GOUBEAU, Kévin DIDIER, Idaïa METZ, Lucas PLANCHET, Alexis BOUE, Aurélien LAPLAUD, Brice SECHER, Katia-Tanina BOULOUFA

@section License

    Copyright (C) 2023 Julien BLANCO, Samuel GOUBEAU, Kévin DIDIER, Idaïa METZ, Lucas PLANCHET
    Copyright (C) 2023 Alexis BOUE, Aurélien LAPLAUD, Brice SECHER, Katia-Tanina BOULOUFA

    This file is part of TurboTurtle.

    TurboTurtle is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TurboTurtle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TurboTurtle.  If not, see http://www.gnu.org/licenses/.
*/

#include "../include/color_selector.h"
#include "ui_color_selector.h"
#include <iostream>
#include <ostream>
#include <sstream>
namespace lib_color_selector{

ColorSelector::ColorSelector(QWidget *parent) :
    QWidget(parent),
    m_ui(new Ui::ColorSelector)
{
    m_ui->setupUi(this);
    m_ui->slide_alpha->setValue(255);
    m_ui->spin_alpha->setValue(255);
    m_button_index=0;

    m_ui->butBlack->change_color(QColor(0,0,0,255));
    m_ui->butBlue->change_color(QColor(0,0,255,255));
    m_ui->butRed->change_color(QColor(255,0,0,255));
    m_ui->butGreen->change_color(QColor(0,255,0,255));
    m_ui->butMagenta->change_color(QColor(255,0,255,255));
    m_ui->butYellow->change_color(QColor(255,255,0,255));
    m_ui->butCyan->change_color(QColor(0,255,255,255));
    m_ui->butWhite->change_color(QColor(255,255,255,255));
    m_ui->butOrange->change_color(QColor (255,180,0,255));
}

ColorSelector::~ColorSelector()
{
    delete m_ui;
}

void ColorSelector::last_colors_used_modify()
{
    switch (m_button_index) {

    case 0 : {
        m_ui->butCustom1->change_color(color());
        m_button_index++;
        break;
    }
    case 1 :{
        m_ui->butCustom2->change_color(color());
        m_button_index++;
        break;
    }
    case 2 : {
        m_ui->butCustom3->change_color(color());
        m_button_index=0;
        break;
    }
        default :
            m_button_index=0;
            last_colors_used_modify();
    }
}

void ColorSelector::on_wheel_on_drag()
{
    update_widgets();
}

QColor ColorSelector::color() const
{
    QColor col = this->m_ui->wheel->color();
    col.setAlpha(this->m_ui->slide_alpha->value());
    return col;
}

void ColorSelector::update_widgets()
{
    bool blocked = signalsBlocked();
    foreach(QWidget *w, findChildren<QWidget*>())
        w->blockSignals(true);

    QColor col = color();

    //change color slider
    QColor alpha_color = col;
    alpha_color.setAlpha(0);
    m_ui->slide_alpha->set_first_color(alpha_color);
    alpha_color.setAlpha(255);
    m_ui->slide_alpha->set_last_color(alpha_color);
    m_ui->spin_alpha->setValue(m_ui->slide_alpha->value());
    col.setAlpha(m_ui->spin_alpha->value());

    m_ui->edit_hex->setText(col.name(QColor::HexArgb));


    m_ui->preview->set_color(col);

    blockSignals(blocked);
    foreach(QWidget *w, findChildren<QWidget*>())
        w->blockSignals(false);

    emit color_changed(col);
}

void ColorSelector::set_color_internal(const QColor &c)
{
    // Note. The difference between this method and setColor, is that setColor
    // sets the official starting color of the dialog, while this is used to update
    // the current color which might not be the final selected color.
    //std::cout<<"alpha trouvé : "<<c.alpha()<<std::endl;
    m_ui->slide_alpha->setValue(c.alpha());
    m_ui->wheel->set_color(c);
    update_widgets();
}

void ColorSelector::update_hex()
{
    QString xs = this->m_ui->edit_hex->text().trimmed();
    QString tmp = xs;
    static QRegularExpression qRegExp("^[0-9a-fA-f]+$");
    tmp.remove('#');

    if ( tmp.isEmpty() )
        return;

    if ( xs.indexOf(qRegExp) == -1 )
    {
        int red=0;
        int green=0;
        int blue=0;
        int alpha=255;
        //std::cout<<"string : "<< xs.toStdString() <<std::endl;
        if(xs.size()>=3){
            //std::cout<<"red : "<< xs.toStdString().at(1) << xs.toStdString().at(2) <<std::endl;
            std::stringstream hexstring;
            hexstring << xs.toStdString().at(1)<< xs.toStdString().at(2);
            red = (int) strtol(hexstring.str().c_str(), NULL, 16);
        }
        if(xs.size()>=5){
            //std::cout<<"green : "<< xs.toStdString().at(3) << xs.toStdString().at(4) <<std::endl;
            std::stringstream hexstring;
            hexstring << xs.toStdString().at(3)<< xs.toStdString().at(4);
            green = (int) strtol(hexstring.str().c_str(), NULL, 16);
        }
        if(xs.size()>=7){
            //std::cout<<"blue : "<< xs.toStdString().at(5) << xs.toStdString().at(6) <<std::endl;
            std::stringstream hexstring;
            hexstring << xs.toStdString().at(5)<< xs.toStdString().at(6);
            blue = (int) strtol(hexstring.str().c_str(), NULL, 16);

        }
        if(xs.size()>=9){
            std::stringstream hexstring;
            hexstring << xs.toStdString().at(1)<< xs.toStdString().at(2);
            alpha = (int) strtol(hexstring.str().c_str(), NULL, 16);

            hexstring = std::stringstream();
            hexstring << xs.toStdString().at(3)<< xs.toStdString().at(4);
            red = (int) strtol(hexstring.str().c_str(), NULL, 16);

            hexstring = std::stringstream();
            hexstring << xs.toStdString().at(5)<< xs.toStdString().at(6);
            green = (int) strtol(hexstring.str().c_str(), NULL, 16);

            hexstring = std::stringstream();
            hexstring << xs.toStdString().at(7)<< xs.toStdString().at(8);
            blue = (int) strtol(hexstring.str().c_str(), NULL, 16);
        }
        QColor c(red,green,blue,alpha);
        set_color_internal(c);
    }
}

void ColorSelector::on_edit_hex_textEdited(const QString &arg1)
{
    int cursor = this->m_ui->edit_hex->cursorPosition();
    //std::cout << cursor << std::endl;
    update_hex();
    //edit_hex->blockSignals(true);
    m_ui->edit_hex->setText(arg1);
    //edit_hex->blockSignals(false);
    m_ui->edit_hex->setCursorPosition(cursor);
}

void ColorSelector::on_slide_alpha_sliderMoved(int position)
{
    m_ui->spin_alpha->setValue(position);
    update_widgets();
}

void ColorSelector::on_butWhite_left_clicked()
{
    //std::cout<<"white mon poto"<<std::endl;
    QColor c = this->m_ui->butWhite->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butRed_left_clicked()
{
    QColor c = this->m_ui->butRed->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butOrange_left_clicked()
{
   QColor c = this->m_ui->butOrange->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butYellow_left_clicked()
{
    QColor c = this->m_ui->butYellow->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butGreen_left_clicked()
{
    QColor c = this->m_ui->butGreen->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butCyan_left_clicked()
{
    QColor c = this->m_ui->butCyan->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butBlue_left_clicked()
{
    QColor c = this->m_ui->butBlue->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butMagenta_left_clicked()
{
    QColor c = this->m_ui->butMagenta->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butBlack_left_clicked()
{
    QColor c = this->m_ui->butBlack->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butCustom1_left_clicked()
{
    QColor c = this->m_ui->butCustom1->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butCustom2_left_clicked()
{
    QColor c = this->m_ui->butCustom2->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butCustom3_left_clicked()
{
    QColor c = this->m_ui->butCustom3->get_color();
    this->set_color_internal(c);

}

void ColorSelector::on_spin_alpha_valueChanged(int arg1)
{
    this->m_ui->slide_alpha->setValue(arg1);
    update_widgets();
}

void ColorSelector::on_wheel_on_click_release()
{
   last_colors_used_modify();
   emit color_changed(color());
}

void ColorSelector::on_color_picked(const QColor &color){
    this->set_color_internal(color);
    update_widgets();
}

void ColorSelector::on_butBackground_right_clicked()
{
    m_ui->butBackground->change_color(color());
}

void ColorSelector::on_butForeground_right_clicked()
{
    m_ui->butForeground->change_color(color());
}

void ColorSelector::on_butBackground_left_clicked()
{
    QColor c = this->m_ui->butBackground->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_butForeground_left_clicked()
{
    QColor c = this->m_ui->butForeground->get_color();
    this->set_color_internal(c);
}

void ColorSelector::on_slide_alpha_valueChanged(int value)
{
    m_ui->spin_alpha->setValue(value);
    update_widgets();
}

void ColorSelector::increase_alpha(unsigned inc)
{
    m_ui->spin_alpha->setValue(m_ui->spin_alpha->value() + inc);
}

void ColorSelector::decrease_alpha(unsigned dec)
{
    m_ui->spin_alpha->setValue(m_ui->spin_alpha->value() - dec);
}

}
