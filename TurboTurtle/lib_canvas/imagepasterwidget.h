/**
 @author Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef IMAGEPASTERWIDGET_H
#define IMAGEPASTERWIDGET_H
#include "imagepaster.h"
#include <QRubberBand>
#include <QWidget>
#include <QImage>
#include <QHBoxLayout>
#include <QSizeGrip>
#include <QDrag>
#include <QMimeData>
#include <QMouseEvent>

namespace lib_canvas {
class ImagePasterWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ImagePasterWidget(const QImage &, QWidget * = nullptr);
    ~ImagePasterWidget();

    QImage get_image();

    /**
     * @brief get the offset of the image displayed in m_rubberband
     * @return a pair containing the two offset coordinates (upper left/lower right)
     */
    QPair<QPoint, QPoint> get_image_offsets();

    /**
     * @brief get_cursor getter of the cursor
     * @param flag to keep the aspect ratio
     */
    void keep_aspect_ratio(bool);

private:
    /**
     * @brief QRubberband redefinition containing the image
     */
    ImagePaster* m_rubberband;

    /**
     * @brief QGridLayout containing the resize handles of the widget
     */
    QGridLayout* m_layout;

    /**
     * @brief QPoint containing the original position of the widget before resizing
     */
    QPoint m_original_position;

    /**
     * @brief QImage to be displayed in the QRubberband
     */
    QImage m_img;

    /**
     * @brief qreal containing the aspect ratio of the image
     */
    qreal m_aspect_ratio;

    /**
     * @brief bool to keep the aspect ratio
     */
    bool m_keep_aspect_ratio;

    void resizeEvent(QResizeEvent *);
    void enterEvent(QEnterEvent *);
    void mouseReleaseEvent(QMouseEvent *);
};
}

#endif // IMAGEPASTERWIDGET_H
