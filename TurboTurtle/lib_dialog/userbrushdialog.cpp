/**
 @authors Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "lib_dialog/userbrushdialog.h"
#include "ui_userbrushdialog.h"
#include <QFileDialog>
#include <QStandardPaths>

/*lib_dialog::*/
userBrushDialog::userBrushDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::userBrushDialog),
    m_numButtons(1),
    m_selectedButton(nullptr)
{
    ui->setupUi(this);

    /*
    if(!QDir("brushes").exists()) {
        QDir().mkdir("brushes");
    }
*/
    if(QDir().mkpath(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/brushes")) {
        m_brush_directory = QDir(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/brushes");
    } else {
        //if the app configuration file is unavailable for some reason
        QDir().mkdir("brushes");
        m_brush_directory = QDir("brushes");
    }

    getAllBrushes();
}

/*lib_dialog::*/
userBrushDialog::~userBrushDialog()
{
    delete ui;
}

void userBrushDialog::on_addButton_clicked()
{

    QString fileName = QFileDialog::getOpenFileName(this, tr("Selectionner un fichier"), QDir::homePath(), tr("*.png *.jpg *.jpeg"));
    if(fileName.isEmpty())
    {
        qDebug() <<"il faut selectionner un fichier";
    }
    else
    {
        qDebug() <<"Fichier selectionné";

        // get file name
        const QFileInfo info(fileName);
        const QString file(info.fileName());

        //copy file in the brushes directory
        const QString new_brush = m_brush_directory.absolutePath() + "/" + file;
        QFile::copy(fileName, new_brush);

        addBrush(new_brush);
    }
}

void userBrushDialog::selectButton()
{
    //update m_selectedButton
    m_selectedButton = qobject_cast<lib_dialog::userBrushButton *>(sender());
}


void userBrushDialog::on_butOk_clicked()
{
    if(m_selectedButton != nullptr)
    {
        emit user_brush_selected(m_selectedButton->path);
        accept();
    }else {
        reject();
    }

}


void userBrushDialog::on_butDelete_clicked()
{
    if (m_selectedButton != nullptr)
    {
        qDebug() << "buttons list before delete \n" << m_buttons;

        m_selectedButton->DeleteImg();
        m_selectedButton = nullptr;

        for (lib_dialog::userBrushButton *button : m_buttons){
            qDebug() << "deleting a button";
            ui->gridLayout->removeWidget(button);
            button->hide();
        }
        qDebug() << "buttons list before delete \n" << m_buttons;
        m_buttons.clear();
        m_numButtons = 1;

        getAllBrushes();
        qDebug() << "buttons list after delete \n" << m_buttons;
    }
}

void userBrushDialog::addBrush(const QString file)
{
    qDebug() << "button path is " << file;
    int row = m_numButtons / 5;
    int col = m_numButtons % 5;
    lib_dialog::userBrushButton *button = new lib_dialog::userBrushButton(file, this);
    qDebug() << "(row,col) = " << row << "," << col;

    button->setFixedSize(QSize(50, 50));
    button->setIconSize(QSize(30, 50));
    ui->gridLayout->addWidget(button, row, col );
    m_numButtons++;
    m_buttons << button;

    connect(button, &lib_dialog::userBrushButton::clicked, this, &userBrushDialog::selectButton);
}

void userBrushDialog::getAllBrushes()
{
    //const QDir brushes("brushes");
    const QStringList images_brushes = m_brush_directory.entryList(QStringList() << "*.jpg" << "*.jpeg" << "*.png",QDir::Files);
    foreach(QString brush, images_brushes)
    {
       addBrush(m_brush_directory.absolutePath() + "/" + brush);
    }
}


