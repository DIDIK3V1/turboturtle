/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "animation.h"
#include "lib_canvas/canvas.h"
#include "lib_widgets/statusbar.h"
#include <QDir>
#include <QBuffer>
#include <QMovie>
#include <QMessageBox>
#include <QProcess>
#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <QProgressBar>

namespace lib_animation {
    size_t Animation::ANIM_COUNTER = 0;
    const QString Animation::BACKUP_LOCATION = QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/TurboTurtle/backup/";


    Animation::Animation() : std::vector<lib_frame::Frame*>() {
        // Give animation name
        m_name.append("animation").append(std::to_string(ANIM_COUNTER));

        // Increment counter
        ANIM_COUNTER++;
    }

    Animation::~Animation() {
        for (lib_frame::Frame *frame : *this) {
            delete frame;
        }
        QDir backupFolder(lib_animation::Animation::BACKUP_LOCATION + QString::fromStdString(m_name));
        backupFolder.removeRecursively();
    }

    Animation::Animation(Animation &anim) : vector(anim) {
        // Give animation name
        m_name.append("animation").append(std::to_string(ANIM_COUNTER));

        // Increment counter
        ANIM_COUNTER++;

        // Backup old animation to save it
        anim.backup();

        this->clear();

        // Copy frames from other folder
        QString cp_location(BACKUP_LOCATION + QString::fromStdString(anim.m_name));
        QString curr_location(BACKUP_LOCATION + QString::fromStdString(m_name));

        QDirIterator dir_it(cp_location, QDir::Dirs|QDir::Files|QDir::NoSymLinks|QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

        while (dir_it.hasNext()) {
            QString next_path(dir_it.next());
            QString destination(curr_location + dir_it.filePath().remove(0, cp_location.size()));

            // Directory, create it
            if (dir_it.fileInfo().isDir())
                QDir().mkpath(destination);
            // File, copy it
            else {
                QFile::copy(next_path, destination);
            }
        }
    }

    Animation* Animation::new_animation() {
        return new_animation(Qt::white);
    }

    Animation* Animation::new_animation(Qt::GlobalColor color) {
        Animation *new_animation = new Animation();
        new_animation->push_back(new lib_frame::Frame(color));
        return new_animation;
    }

    Animation* Animation::copy_memory(const Animation &anim) {
        Animation *res = new Animation();

        // Copy frames with memory
        for (auto i : anim) {
            res->push_back(new lib_frame::Frame(*i));
        }

        // Give animation name
        res->m_name.append("animation").append(std::to_string(ANIM_COUNTER));

        // Increment counter
        ANIM_COUNTER++;

        return res;
    }

    Animation *Animation::import_from_gif(const QString &image_file) {
        if (!QFile::exists(image_file)) {
            QMessageBox::information(nullptr, "Information", "The file ("+ image_file +") does not exist !");
            qWarning() << "Failed to import" << image_file << "as an animation, the file doesn't exist";
            return new_animation();
        }

        Animation *res = new Animation();

        // Load gif
        QMovie movie(image_file);
        movie.start();

        std::vector<std::thread*> threads;
        size_t i = 0;

        // Convert frames to QImages
        for (int frame_index = 0; frame_index < movie.frameCount(); ++frame_index) {
            movie.jumpToFrame(frame_index);
            QImage frame = movie.currentImage();

            if(!frame.isNull()) {
                frame = frame.scaled(lib_canvas::Canvas::s_canvas_size);
                frame.convertTo(QImage::Format_RGBA8888);

                res->push_back(new lib_frame::Frame(frame));

                std::thread* thread = new std::thread([&res, i]() {
                    // Also backup the frame so that we don't need to do that later
                    (*res)[i]->save(BACKUP_LOCATION + QString::fromStdString(res->get_name()), i);
                    (*res)[i]->is_modified = false;
                });
                threads.push_back(thread);
                i++;
            }
        }
        lib_widgets::StatusBar::getInstance()->showMessage("Done", 5 * 1000);

        for(std::thread* thread : threads) {
            thread->join();
            delete thread;
        }

        // Unload frame
        res->unload();


        return res;
    }

    Animation *Animation::import_from_png(const QString &import_folder, const bool &backup_recover) {
        // Get all paths
        QString images_path(import_folder + "/images");
        QString background_path(import_folder + "/backgrounds");

        // Image folder does not exist, the user may have given a path to some images... in that case we'll use that as our base
        if(!QDir(images_path).exists()){
            images_path = import_folder;
        }

        // No background folder. In that case, we'll consider that the background is the image itself
        if (!QDir(background_path).exists()) {
            background_path = images_path;
        }

        // Prepare supported file extensions, allows for more in case of a user modification
        QStringList file_extensions;
        file_extensions << "*.png" << "*.jpeg" << "*.jpg";

        QDir image_dir(images_path);
        QDir background_dir(background_path);

        // Get entries, sort them correctly
        QCollator collator;
        collator.setNumericMode(true);
        QStringList image_files = image_dir.entryList( file_extensions , QDir::Files);
        QStringList background_files = background_dir.entryList( file_extensions , QDir::Files);
        std::ranges::sort(image_files.begin(), image_files.end(), collator);
        std::ranges::sort(background_files.begin(), background_files.end(), collator);

        if (image_files.empty()) {
            if (!backup_recover)
                QMessageBox::information(nullptr, "Information", "The folder ("+ images_path +") does not contain any images!");
            qWarning() << images_path << "doesn't contain any images.";
            return nullptr;
        }

        Animation *res = new Animation();

        // Prepare backup folder before, this should avoid any issue later
        const QString path = BACKUP_LOCATION + QString::fromStdString(res->get_name());
        QDir destination_images_dir(path + "/images");
        QDir destination_backgrounds_dir(path + "/backgrounds");

        // If it cannot create the folders, save that information for the threads
        destination_images_dir.mkpath(".");
        destination_backgrounds_dir.mkpath(".");

        //QProgressBar bar(nullptr);
        //bar.setRange(0, (int) image_files.size() - 1);

        std::vector<std::thread*> threads;
        bool error = false;

        for (int i = 0; i < image_files.size(); ++i) {
            std::thread* thread = new std::thread([&images_path, &image_files, &background_path, &background_files, i, &error, &destination_images_dir, &destination_backgrounds_dir]() {
                QString image_file_path(images_path + "/" + image_files[i]);
                QString background_file_path(background_path + "/" + background_files[i]);
                QString image_destination_path(destination_images_dir.absolutePath() + "/" + image_files[i]);
                QString background_destination_path(destination_backgrounds_dir.absolutePath() + "/" + background_files[i]);

                // Copy the frame files to their location
                if (!QFile::copy(image_file_path, image_destination_path) || !QFile::copy(background_file_path, background_destination_path)) {
                    error = true;
                    return;
                }
            });
            threads.push_back(thread);
        }

        for(std::thread* thread : threads) {
            thread->join();
            delete thread;
        }

        if (error) {
            if (!backup_recover)
                QMessageBox::information(nullptr, "Information", "The folder does not contain any images!");

            delete res;
            return nullptr;
        }
        if (!backup_recover)
            emit lib_widgets::StatusBar::getInstance()->sendMessage("Done", 5 * 1000);

        return res;
    }

    QImage Animation::get_thumbnail() const {
        // QImage from file, or default if no file associated
        if (!is_loaded()) {
            QString img_path(BACKUP_LOCATION + QString::fromStdString(m_name) + "/images/image0.png");

            // No file? Default image
            if (!QFile::exists(img_path))
                return {lib_canvas::Canvas::s_canvas_size, QImage::Format_RGBA8888};

            return QImage(img_path);
        }
        else {
            return (*this)[0]->get_thumbnail();
        }
    }

    std::string Animation::get_name() const {
        return m_name;
    }

    bool Animation::is_loaded() const {
        return !empty();
    }

    bool Animation::unload() {
        if (m_in_timeline || m_in_composing_bench || !is_loaded())
            return false;

        // Clear frames from memory
        for (lib_frame::Frame *frame : *this) {
            delete frame;
        }
        this->clear();

        return true;
    }

    void Animation::set_used_in_timeline(const bool &is_used) {
        m_in_timeline = is_used;
    }

    void Animation::set_used_in_composing_bench(const bool &is_used) {
        m_in_composing_bench = is_used;
    }

    void Animation::move_frame(const size_t &start_index, const size_t &end_index)
    {
        lib_frame::Frame *tmp = this->at(start_index);
        if (end_index >= this->size()) {
            this->erase(this->begin() + int(start_index));
            this->push_back(tmp);
        } else {
            this->erase(this->begin() + int(start_index));
            this->insert(this->begin() + int(end_index), tmp);
        }
    }

    void Animation::save_to_png(const QString& path, const bool &log) {
        qDebug() << "Preparing to save as png" << QString::fromStdString(m_name) << "at" << path;

        // Save any modification done if the animation is loaded
        if (is_loaded())
            backup();

        // Remove path if it exists, then create the folder
        QDir(path).removeRecursively();

        if (!QDir().mkpath(path)) {
            qDebug() << "Failed to create folder" << path << " for animation " << QString::fromStdString(m_name);
            if (log)
                emit lib_widgets::StatusBar::getInstance()->sendMessage("Failed to save the animation. The path cannot be created", 5 * 1000);
        }

        QString backup_location(BACKUP_LOCATION + QString::fromStdString(m_name));

        QDirIterator dir_it(backup_location, QDir::Dirs|QDir::Files|QDir::NoSymLinks|QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

        while (dir_it.hasNext()) {
            QString next_path(dir_it.next());
            QString destination(path + dir_it.filePath().remove(0, backup_location.size()));

            bool err;

            // Directory, create it
            if (dir_it.fileInfo().isDir())
                err = !QDir().mkpath(destination);
            // File, copy it
            else {
                err = !QFile::copy(next_path, destination);
            }

            if (err) {
                qDebug() << "Failed to export animation" << QString::fromStdString(m_name) << "to path" << path;
                if (log)
                    emit lib_widgets::StatusBar::getInstance()->sendMessage("Failed to save the animation", 5 * 1000);

                return;
            }
        }

        qDebug() << "Png" << QString::fromStdString(m_name) << "are now saved at" << path;

        // Log is used to block the information display to the user (in case of a gif/video export)
        if (log)
            emit lib_widgets::StatusBar::getInstance()->sendMessage("Done. Saved at : " + path, 5 * 1000);
    }

    void Animation::save_to_gif(const QString &file_name, const double &framerate) {
        std::string prog("ffmpeg -h ");
        #ifdef Q_OS_WIN
        prog.append("> nul");
        #else
        prog.append("&> /dev/null");
        #endif
        if (system(prog.c_str()) != 0) {
            QMessageBox::warning(nullptr, "Gif export", "ffmpeg was not found");
            return;
        }

        // Execute code in other thread, avoids freezing the app
        QFuture<void> fut = QtConcurrent::run([this, framerate, file_name]() {
            qDebug() << "Preparing to save as gif" << QString::fromStdString(m_name);

            // Prepare location to read from
            QString backup_location(BACKUP_LOCATION + QString::fromStdString(m_name) + "/");

            // Temporary files
            QString images_dir = backup_location + "images/image%d.png";
            QString palette_file = BACKUP_LOCATION + QString::fromStdString(m_name) + "_palette.png";

            //generating a color palette to preserve image quality and reduce filesize
            QStringList args;
            args << "-y"
                 << "-i" << images_dir
                 << "-vf" << "palettegen" << palette_file;

            QProcess palette_process;
            palette_process.start("ffmpeg", args);
            palette_process.waitForFinished();
            qDebug() << "gif color palette generated";

            //generating the output gif
            QStringList arguments;
            arguments << "-y"                                                       //set to automatic overwrite if output file exists
                      << "-r" << QString::number(framerate)                         //input framerate
                      << "-i" << images_dir                                         //input: image sequence of name format frame_%d.png (i.e. frame_1.png, frame_2.png, ... frame_n.png)
                      << "-i" << palette_file                                       //input: color palette generated from the image sequence
                      << "-filter_complex"                                          //filter complex to set fps, scale and apply the generated color palette
                      << "scale=720:-1:flags=lanczos[x];[x][1:v]paletteuse"         //parameters of the filter (default: 10fps, 720px)
                      << "-r" << QString::number(framerate)                         //output framerate
                      << file_name;                                               //output file name
            qDebug() << arguments;

            QProcess convert_process;
            convert_process.start("ffmpeg", arguments);
            convert_process.waitForFinished();

            // Remove palette file
            QFile::remove(palette_file);

            qDebug() << "Gif" << QString::fromStdString(m_name) << "is now saved at" << file_name;
            emit lib_widgets::StatusBar::getInstance()->sendMessage("Done. Saved at : " + file_name, 5 * 1000);
        });
    }

    void Animation::save_to_mp4(const QString &file_name, const double &framerate) {
        std::string prog("ffmpeg -h ");
        #ifdef Q_OS_WIN
        prog.append("> nul");
        #else
        prog.append("&> /dev/null");
        #endif
        if (system(prog.c_str()) != 0) {
            QMessageBox::warning(nullptr, "Video export", "ffmpeg was not found");
            return;
        }

        // Execute code in other thread, avoids freezing the app
        QFuture<void> fut = QtConcurrent::run([this, framerate, file_name]() {
            qDebug() << "Preparing to save as mp4" << QString::fromStdString(m_name);

            // Prepare location to read from
            QString backup_location(BACKUP_LOCATION + QString::fromStdString(m_name) + "/");

            // Temporary files
            QString images_dir = backup_location + "images/image%d.png";

            // Args for mp4
            QStringList arguments;
            arguments << "-y"                       //set to automatic overwrite if output file exists
                      << "-r" << QString::number(framerate)
                      << "-i" << images_dir         //input: image sequence of name format frame_%d.png (i.e. frame_1.png, frame_2.png, ... frame_n.png)
                      << "-c:v" << "libx264"        //vcodec (default: AVC - h264)
                      << "-crf" << "22"             //quality preset (default: crf 22)
                      << file_name;               //output file name (default container: MPEG-4 Part 14)
            qDebug() << arguments;

            QProcess convert_process;
            convert_process.start("ffmpeg", arguments);
            convert_process.waitForFinished();

            qDebug() << "Mp4" << QString::fromStdString(m_name) << "is now saved at" << file_name;
            emit lib_widgets::StatusBar::getInstance()->sendMessage("Done. Saved at : " + file_name, 5 * 1000);
        });
    }

    void Animation::backup() {
        if (!is_loaded() || !is_modified)
            return;

        qDebug() << QString::fromStdString(m_name) << "starting backup";

        // Get all paths
        QString path = QString(BACKUP_LOCATION + QString::fromStdString(m_name));
        QString destination_images_dir(path + "/images");
        QString destination_backgrounds_dir(path + "/backgrounds");

        // Check their existence, if they exist it won't fail
        if (!QDir().mkpath(destination_images_dir) || !QDir().mkpath(destination_backgrounds_dir)) {
            qWarning() << QString::fromStdString(get_name()) << "Failed to create backup location. No backup will be done";
            return;
        }

        // Prepare threads
        std::vector<std::thread*> threads;

        for(int i = 0; i < this->size(); ++i){
            // Save only modified images, else ignore them
            if ((*this)[i]->is_modified) {
                std::thread* thread = new std::thread([this, i, &path]() {
                    // Also backup the frame so that we don't need to do that later
                    (*this)[i]->is_modified = false;
                    (*this)[i]->unsafe_save(path, i);
                });
                threads.push_back(thread);
            }
        }

        for(std::thread* thread : threads) {
            thread->join();
            delete thread;
        }

        // Check for deleted frames
        QStringList file_extensions;
        file_extensions << "*.png" << "*.jpeg" << "*.jpg";
        QCollator collator;
        collator.setNumericMode(true);
        QStringList image_files = QDir(destination_images_dir).entryList( file_extensions , QDir::Files);
        QStringList background_files = QDir(destination_backgrounds_dir).entryList( file_extensions , QDir::Files);
        std::ranges::sort(image_files.begin(), image_files.end(), collator);
        std::ranges::sort(background_files.begin(), background_files.end(), collator);

        for (size_t i = 0; i < size(); ++i) {
            image_files.remove(0);
            background_files.remove(0);
        }

        // Too much images, one frame must have been removed
        if (!image_files.empty()) {
            for (const QString &str : image_files) {
                qDebug() << str;
                QFile::remove(destination_images_dir + "/" + str);
            }
        }
        if (!background_files.empty()) {
            for (const QString &str : background_files) {
                QFile::remove(destination_backgrounds_dir + "/" + str);
            }
        }

        qDebug() << QString::fromStdString(m_name) << "backup done";
        is_modified = false;
    }

    void Animation::backup_frame(const int &pos) {
        if (empty() || pos < 0 || pos >= this->size()) {
            qWarning() << "Tried to backup frame with position" << pos << "which is outside of range";
            return;
        }

        // Return if not modified
        if (!(*this)[pos]->is_modified)
            return;

        // Get all paths
        QString path = QString(BACKUP_LOCATION + QString::fromStdString(m_name));
        QString destination_images_dir(path + "/images");
        QString destination_backgrounds_dir(path + "/backgrounds");

        // Check their existence, if they exist it won't fail
        if (!QDir().mkpath(destination_images_dir) || !QDir().mkpath(destination_backgrounds_dir)) {
            qWarning() << QString::fromStdString(get_name()) << "Failed to create backup location. No backup will be done";
            return;
        }

        // Save modifications
        (*this)[pos]->is_modified = false;
        (*this)[pos]->unsafe_save(path, pos);
    }

    bool Animation::reload_from_backup() {
        const QString import_folder(BACKUP_LOCATION + QString::fromStdString(m_name));

        // Get all paths
        QString images_path(import_folder + "/images");
        QString background_path(import_folder + "/backgrounds");

        // Image folder does not exist, the user may have given a path to some images... in that case we'll use that as our base
        if(!QDir(images_path).exists()){
            images_path = import_folder;
        }

        // No background folder. In that case, we'll consider that the background is the image itself
        if (!QDir(background_path).exists()) {
            background_path = images_path;
        }

        // Prepare supported file extensions, allows for more in case of a user modification
        QStringList file_extensions;
        file_extensions << "*.png" << "*.jpeg" << "*.jpg";

        QDir image_dir(images_path);
        QDir background_dir(background_path);

        // Get entries, sort them correctly
        QCollator collator;
        collator.setNumericMode(true);
        QStringList image_files = image_dir.entryList( file_extensions , QDir::Files);
        QStringList background_files = background_dir.entryList( file_extensions , QDir::Files);
        std::ranges::sort(image_files.begin(), image_files.end(), collator);
        std::ranges::sort(background_files.begin(), background_files.end(), collator);

        if (image_files.empty()) {
            qWarning() << images_path << "doesn't contain any images.";
            return false;
        }

        // Prepare the animation size for our threads
        this->resize(image_files.size(), nullptr);

        //QProgressBar bar(nullptr);
        //bar.setRange(0, (int) image_files.size() - 1);

        std::vector<std::thread*> threads;
        bool error = false;

        for (int i = 0; i < image_files.size(); ++i) {
            std::thread* thread = new std::thread([this, &images_path, &image_files, &background_path, &background_files, i, &error]() {
                QString image_file_path = images_path + "/" + image_files[i];
                QString background_file_path = background_path + "/" + background_files[i];

                QImage image(image_file_path);
                QImage background(background_file_path);

                // Scale image with default 1280/720 size
                image = image.scaled(1280, 720);
                background = background.scaled(1280, 720);
                image.convertTo(QImage::Format_RGBA8888);
                background.convertTo(QImage::Format_RGBA8888);

                if(image.isNull()){
                    error = true;
                    return;
                }

                // Use image as background since no background is available for the image
                if(background.isNull()){
                    background = image.copy();
                }

                (*this)[i] = new lib_frame::Frame(image, background);
            });
            threads.push_back(thread);
        }

        for(std::thread* thread : threads) {
            thread->join();
            delete thread;
        }

        if (error) {
            qDebug() << "Failed to load the animation from backup";
            for (lib_frame::Frame *frame : *this) {
                if (frame != nullptr)
                    delete frame;
            }
            this->clear();
            return false;
        }

        emit lib_widgets::StatusBar::getInstance()->sendMessage("Reloaded frame", 5 * 1000);


        return false;
    }
} // namespace lib_animation
