/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Creerio, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "brush.h"
#include <QDebug>
#include <functional>
#include <iostream>
#include <ostream>
#include <random>
#include <thread>
namespace lib_drawing_tools {
Brush::Brush(const QImage &brush, int nb_var)
{
    m_nb_generated = nb_var;
    m_random_brushes = std::vector<QImage>(m_nb_generated);
    m_current_uncolored_brushes = std::vector<QImage>(m_nb_generated);
    m_current_colored_brushes = std::vector<QImage>(m_nb_generated);
    m_current_rotation=0;
    m_current_color = QColor(Qt::black);

    m_nb_threads = std::thread::hardware_concurrency();
    m_threads = std::vector<std::thread*>();

    int nbrand=0;
    int index_brushes =0;

    for(; nbrand<m_nb_generated; nbrand+=m_nb_threads){

        int values = m_nb_generated-nbrand;
        if(values > m_nb_threads){
            values = m_nb_threads;
        }
        for(int i=0; i < values ; ++i){
            m_threads.push_back(new std::thread([this,brush,index_brushes]{
                this->compute_brush(
                    brush.copy(),
                    &m_random_brushes[index_brushes],
                    &m_current_uncolored_brushes[index_brushes],
                    &m_current_colored_brushes[index_brushes]);
            }));
            index_brushes++;
        }

        for(std::thread *thread : m_threads) {
            if(thread->joinable())
                thread->join();
        }

        for(std::thread *thread : m_threads) {
            delete thread;
        }

        m_threads.clear();
    }

    extract_border(m_current_color);
}



void Brush::compute_brush(
                          const QImage &brush,
                          QImage *ret_brush,
                          QImage *ret_scaled,
                          QImage *ret_colored
                          ){


    // Generate random variations of the brush color and alpha values
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dist_color(0, 0.5);
    std::uniform_real_distribution<float> dist_alpha(-0.5, 0.5);

    QImage rand_brush = brush.copy();
    // Loop through each pixel in the custom brush and apply the color and alpha variations
    for (int iX = 0; iX < rand_brush.width(); ++iX)
    {
        for (int iY = 0; iY < rand_brush.height(); ++iY)
        {
            QColor pixel_color = rand_brush.pixelColor(iX, iY);
            float color_coef = pixel_color.blackF();
            float alpha_coef = pixel_color.alphaF();

            // Generate random variations for color and alpha
            if(alpha_coef != 0) {
                alpha_coef += dist_alpha(gen);
                color_coef += 0.4;
            }
            if(color_coef != 0) {
                color_coef += dist_color(gen);
            }

            // Ensure that color and alpha values are within bounds
            if(alpha_coef < 0 || alpha_coef > 1 ){
                alpha_coef = pixel_color.alphaF();
            }
            if(color_coef > 1 || color_coef < 0) {
                color_coef = pixel_color.blackF();
            }

            // Set the new color and alpha values for the pixel
            QColor newColor(Qt::black);
            newColor.setHsv(newColor.hue(), newColor.saturation(), newColor.value(),newColor.alpha() * alpha_coef * 0.3);
            rand_brush.setPixelColor(iX, iY, newColor);
        }
    }

    *ret_brush = rand_brush.copy();
    QImage tmpbrush(rand_brush.scaled(1,1,Qt::KeepAspectRatio));
    *ret_scaled = tmpbrush.copy();
    *ret_colored = tmpbrush.copy();

}

Brush::Brush(const Brush &b){
    m_nb_generated=b.m_nb_generated;
    m_random_brushes= std::vector<QImage>(b.m_random_brushes);
    m_current_uncolored_brushes = std::vector<QImage>(b.m_current_uncolored_brushes);
    m_current_colored_brushes = std::vector<QImage>(b.m_current_colored_brushes);
    m_current_rotation = b.m_current_rotation;
    m_nb_threads = b.m_nb_threads;
    m_current_color = b.m_current_color;
    m_current_width = b.m_current_width;
    extract_border(m_current_color);
}

Brush::~Brush()
{
    qDebug() << "trying to delete brush";
    for (size_t i = 0; i < m_threads.size(); i++) {
        delete m_threads[i];
    }

    qDebug() << "brush deleted correctly";
}

Brush Brush::operator=(const Brush &b){
    if (this != &b) {
        m_nb_generated = b.m_nb_generated;
        m_random_brushes= std::vector<QImage>(b.m_random_brushes);
        m_current_uncolored_brushes = std::vector<QImage>(b.m_current_uncolored_brushes);
        m_current_colored_brushes = std::vector<QImage>(b.m_current_colored_brushes);
        m_current_rotation = b.m_current_rotation;
        m_nb_threads = b.m_nb_threads;
        private_change_width();
        private_change_color();
        private_rotate();
    }

    return *this;
}

QImage const Brush::get_brush()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> dist_brush(0, m_nb_generated-1);
    QImage brush = m_current_colored_brushes[dist_brush(gen)];
    return brush;
}

void Brush::change_width(int width)
{
    if(width != m_current_width){
        m_current_width=width;
        private_change_width();
        m_current_color=QColor(Qt::black);
    }
}

void Brush::compute_brush_color(QImage *uncolored,
                         QImage *colored
                         )
{
    int height = uncolored->height();
    int width =  uncolored->width();
    QColor tmp_col = m_current_color;

    for(int y = 0; y < height; y++){
        for(int x= 0; x < width; x++){
            if(uncolored->pixelColor(x,y).alphaF() != 0){
                tmp_col.setAlpha(uncolored->pixelColor(x,y).alphaF()*m_current_color.alpha());
                colored->setPixelColor(x,y,tmp_col);
                tmp_col=m_current_color;
            }
        }
    }
}

void Brush::compute_rotation(QImage *uncolored,QImage *colored)
{
    *uncolored = uncolored->transformed(QTransform().rotate(m_current_rotation));
    *colored  = colored->transformed(QTransform().rotate(m_current_rotation));
}

void Brush::change_color(const QColor &color){
    if(m_current_color.name(QColor::HexArgb)!= color.name(QColor::HexArgb)){
        m_current_color=color;

        int nbcolored=0;
        int index_brushes =0;

        for(; nbcolored<m_nb_generated; nbcolored+=m_nb_threads){
            int values = m_nb_generated-nbcolored;
            if(values > m_nb_threads){
                values = m_nb_threads;
            }

            for(int i=0; i < values ; ++i){
                m_threads.push_back(new std::thread([this,index_brushes]{
                    this->compute_brush_color(
                        &m_current_uncolored_brushes[index_brushes],
                        &m_current_colored_brushes[index_brushes]);
                }));
                index_brushes++;
            }

            for(std::thread *thread : m_threads) {
                if(thread->joinable())
                    thread->join();
            }

            for(std::thread *thread : m_threads) {
                delete thread;
            }

            m_threads.clear();
        }
    }

}

void Brush::tablet_change_color_and_rotate(const QColor &color, const QTabletEvent *event){

    int rotation = -abs(event->xTilt()) + abs(event->yTilt());
    bool change_rotation = false;
    if(m_current_rotation != rotation){
        change_rotation = true;
    }
    else{
           m_current_rotation= rotation;
    }

    if(m_current_color.name(QColor::HexArgb) != color.name(QColor::HexArgb)){
        m_current_color=color;

        int nbcolored=0;
        int index_brushes =0;

        for(; nbcolored<m_nb_generated; nbcolored+=m_nb_threads){
            int values = m_nb_generated-nbcolored;
            if(values > m_nb_threads){
                values = m_nb_threads;
            }

            if(change_rotation){
                for(int i=0; i < values ; ++i){
                    m_threads.push_back(new std::thread([this, index_brushes] {
                        this->compute_brush_color(&m_current_uncolored_brushes[index_brushes],
                                                  &m_current_colored_brushes[index_brushes]);
                        this->compute_rotation(&m_current_uncolored_brushes[index_brushes],
                                               &m_current_colored_brushes[index_brushes]);
                    }));
                    index_brushes++;
                }
            }else{
                for(int i=0; i < values ; ++i){
                    m_threads.push_back(new std::thread([this,index_brushes] {
                        this->compute_brush_color(
                            &m_current_uncolored_brushes[index_brushes],
                            &m_current_colored_brushes[index_brushes]);
                    }));
                    index_brushes++;
                }
            }

            for(std::thread *thread : m_threads) {
                if(thread->joinable())
                    thread->join();
            }

            for(std::thread *thread : m_threads) {
                delete thread;
            }

            m_threads.clear();
        }
    }
}

void Brush::rotate(const QTabletEvent *event){

    int rotation = -abs(event->xTilt()) + abs(event->yTilt());
    if(rotation != m_current_rotation){
        m_current_rotation = rotation;
        int nbrotated = 0;
        int index_brushes = 0;
        for(; nbrotated<m_nb_generated; nbrotated += m_nb_threads){
            int values = m_nb_generated-nbrotated;
            if(values > m_nb_threads){
                values = m_nb_threads;
            }

            for(int i=0; i < values ; ++i){
                m_threads.push_back(new std::thread([this,index_brushes]{
                    m_current_uncolored_brushes[index_brushes] = m_current_uncolored_brushes[index_brushes].transformed(QTransform().rotate(m_current_rotation));
                    m_current_colored_brushes[index_brushes]  = m_current_colored_brushes[index_brushes].transformed(QTransform().rotate(m_current_rotation));
                }));
                index_brushes++;
            }

            for(std::thread *thread : m_threads) {
                if(thread->joinable())
                    thread->join();
            }

            for(std::thread *thread : m_threads) {
                delete thread;
            }

            m_threads.clear();
        }
    }
}

void Brush::private_rotate()
{
    for(int i = 0; i < m_nb_generated; ++i){
        m_current_uncolored_brushes[i] = m_current_uncolored_brushes[i].transformed(QTransform().rotate(m_current_rotation));
        m_current_colored_brushes[i]  = m_current_colored_brushes[i].transformed(QTransform().rotate(m_current_rotation));
    }
}

void Brush::private_change_color()
{
    int height = m_current_uncolored_brushes[0].height();
    int width =  m_current_uncolored_brushes[0].width();
    QColor tmp_col = m_current_color;
    for(int i =0; i < m_nb_generated; ++i){

        for(int y = 0; y < height; y++){
            for(int x= 0; x < width; x++){
                if(m_current_uncolored_brushes[i].pixelColor(x,y).alphaF()!=0){
                    tmp_col.setAlpha( m_current_uncolored_brushes[i].pixelColor(x,y).alphaF()*m_current_color.alpha());
                    m_current_colored_brushes[i].setPixelColor(x,y,tmp_col);
                    tmp_col=m_current_color;
                }
            }
        }
    }
}

void Brush::private_change_width()
{
    for(int i = 0; i < m_nb_generated; ++i){
        QImage scaled_random_brush = m_random_brushes[i].scaled(QSize(m_current_width,m_current_width),Qt::KeepAspectRatio);
        m_current_uncolored_brushes[i] = scaled_random_brush;
        m_current_colored_brushes[i] = scaled_random_brush;
    }
    extract_border(m_current_color);
}

void Brush::extract_border(const QColor &color)
{
    QColor border_color = QColor(255-color.red(),255-color.green(),255-color.blue());
    QImage image = m_current_colored_brushes[0];
    QImage border(image.size(), QImage::Format_ARGB32);
    border.fill(Qt::transparent);

    for (int y = 0; y < image.height(); ++y) {
        for (int x = 0; x < image.width(); ++x) {
            QRgb pixel = image.pixel(x, y);
            if (qAlpha(pixel) > 0) {
                border.setPixel(x, y, border_color.rgb());
                break;
            }
        }
        for (int x = image.width() - 1; x >= 0; --x) {
            QRgb pixel = image.pixel(x, y);
            if (qAlpha(pixel) > 0) {
                border.setPixel(x, y,  border_color.rgb());
                break;
            }
        }
    }

    for (int x = 0; x < image.width(); ++x) {
        for (int y = 0; y < image.height(); ++y) {
            QRgb pixel = image.pixel(x, y);
            if (qAlpha(pixel) > 0) {
                border.setPixel(x, y,  border_color.rgb());
                break;
            }
        }
        for (int y = image.height() - 1; y >= 0; --y) {
            QRgb pixel = image.pixel(x, y);
            if (qAlpha(pixel) > 0) {
                border.setPixel(x, y,  border_color.rgb());
                break;
            }
        }
    }

    m_brush_border=border;
}

qreal Brush::pressure_to_width(const QTabletEvent *event,int width)
{
    return event->pressure() * width;
}

void Brush::tablet_scale(const QTabletEvent *event, int width)
{
    change_width(pressure_to_width(event,width));
}

void Brush::rotate_scale_change_color(const QColor &color)
{
    if(m_current_color.name(QColor::HexArgb) != color.name(QColor::HexArgb)){
        m_current_color=color;
        int nb_threads = std::thread::hardware_concurrency();

        int nbcolored=0;
        int index_brushes =0;

        for(; nbcolored < m_nb_generated; nbcolored += nb_threads){

            int values = m_nb_generated-nbcolored;
            if(values > nb_threads){
                values = nb_threads;
            }

            for(int i=0; i < values ; ++i){
                m_threads.push_back(new std::thread([this,index_brushes] {
                    this->compute_brush_color(
                        &m_current_uncolored_brushes[index_brushes],
                        &m_current_colored_brushes[index_brushes]);
                }));
                index_brushes++;
            }

            for(std::thread *thread : m_threads) {
                if(thread->joinable())
                    thread->join();
            }

            for(std::thread *thread : m_threads) {
                delete thread;
            }

            m_threads.clear();
        }
    }
}
} // namespace lib_drawing_tools
