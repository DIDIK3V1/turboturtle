/**

@author Mattia Basaglia

@section License

    Copyright (C) 2013-2014 Mattia Basaglia
    Copyright (C) 2014 Calle Laakkonen
    Copyright (C) 2023 Alexis BOUE, Aurélien LAPLAUD, Brice SECHER, Katia-Tanina BOULOUFA

    This file is part of TurboTurtle.

    TurboTurtle is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TurboTurtle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TurboTurtle.  If not, see http://www.gnu.org/licenses/.
*/

#include "../include/gradient_slider.hpp"
#include "../include/paint_border.hpp"

#include <QPainter>
#include <QStyleOptionSlider>
#include <QLinearGradient>
#include <sstream>

namespace lib_color_selector{
class GradientSlider::Private
{

public:

    QLinearGradient gradient;
    QBrush back;

    Private() :
        back(Qt::darkGray, Qt::DiagCrossPattern)
    {
        back.setTexture(QPixmap(QLatin1StringView(":/alphaback.png")));
        gradient.setCoordinateMode(QGradient::StretchToDeviceMode);
    }

};

GradientSlider::GradientSlider(QWidget *parent) :
    QSlider(Qt::Horizontal, parent), m_p(new Private)
{
    QColor tmpColor=this->palette().color(QPalette::Highlight);
    std::stringstream ss;
    ss<<"QSlider { height: 30px; }" <<
        "::groove { border: 0; }" <<
        "::handle { background:"<<
        tmpColor.name().toStdString()<<
        "; width:10px; height:30px;}";

    this->setStyleSheet(ss.str().c_str() );

}

GradientSlider::GradientSlider(const Qt::Orientation &orientation, QWidget *parent) :
    QSlider(orientation, parent), m_p(new Private)
{}

GradientSlider::~GradientSlider()
{
    delete m_p;
}

QBrush GradientSlider::background() const
{
    return m_p->back;
}

void GradientSlider::set_background(const QBrush &bg)
{
    m_p->back = bg;
    update();
}

QGradientStops GradientSlider::colors() const
{
    return m_p->gradient.stops();
}

void GradientSlider::set_colors(const QGradientStops &colors)
{
    m_p->gradient.setStops(colors);
    update();
}

QLinearGradient GradientSlider::get_gradient() const
{
    return m_p->gradient;
}

void GradientSlider::set_gradient(const QLinearGradient &gradient)
{
    m_p->gradient = gradient;
    update();
}

void GradientSlider::set_colors(const QVector<QColor> &colors)
{
    QGradientStops stops;
    stops.reserve(colors.size());

    double c = colors.size() - 1;
    if(c==0) {
        stops.append(QGradientStop(0, colors.at(0)));

    } else {
        for(int i=0;i<colors.size();++i) {
            stops.append(QGradientStop(i/c, colors.at(i)));
        }
    }
    set_colors(stops);
}

void GradientSlider::set_first_color(const QColor &c)
{
    QGradientStops stops = m_p->gradient.stops();
    if(stops.isEmpty())
        stops.push_back(QGradientStop(0.0, c));
    else
        stops.front().second = c;
    m_p->gradient.setStops(stops);

    update();
}

void GradientSlider::set_last_color(const QColor &c)
{
    QGradientStops stops = m_p->gradient.stops();
    if(stops.size()<2)
        stops.push_back(QGradientStop(1.0, c));
    else
        stops.back().second = c;
    m_p->gradient.setStops(stops);
    update();
}

QColor GradientSlider::first_color() const
{
    QGradientStops s = colors();
    return s.empty() ? QColor() : s.front().second;
}

QColor GradientSlider::last_color() const
{
    QGradientStops s = colors();
    return s.empty() ? QColor() : s.back().second;
}

void GradientSlider::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    if(orientation() == Qt::Horizontal)
        m_p->gradient.setFinalStop(1, 0);
    else
        m_p->gradient.setFinalStop(0, 1);

    painter.setPen(Qt::NoPen);
    painter.setBrush(m_p->back);
    painter.drawRect(1,1,geometry().width()-2,geometry().height()-2);
    painter.setBrush(m_p->gradient);
    painter.drawRect(1,1,geometry().width()-2,geometry().height()-2);

    paint_tl_border(painter,size(),palette().color(QPalette::Mid),0);
    /*paint_tl_border(painter,size(),palette().color(QPalette::Dark),1);

    paint_br_border(painter,size(),palette().color(QPalette::Light),1);*/
    paint_br_border(painter,size(),palette().color(QPalette::Midlight),0);

    QStyleOptionSlider opt_slider;
    initStyleOption(&opt_slider);
    opt_slider.subControls = QStyle::SC_SliderHandle;
    if (isSliderDown())
        opt_slider.state |= QStyle::State_Sunken;
    style()->drawComplexControl(QStyle::CC_Slider, &opt_slider, &painter, this);

    /*QStyleOptionFrameV3 opt_frame;
    opt_frame.init(this);
    opt_frame.frameShape = QFrame::StyledPanel;
    opt_frame.rect = geometry();
    opt_frame.state = QStyle::State_Sunken;

    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::NoBrush);
    painter.translate(-geometry().topLeft());
    style()->drawControl(QStyle::CE_ShapedFrame, &opt_frame, &painter, this);*/
}

void GradientSlider::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        if (Qt::Orientation() == Qt::Vertical)
            setValue(minimum() + ((maximum() - minimum()) * (height() - event->position().y())) / height());
        else
            setValue(minimum() + ((maximum() - minimum()) * event->position().x()) / width());

        event->accept();
    }
    QSlider::mousePressEvent(event);
}
} // namespace lib_color_selector
