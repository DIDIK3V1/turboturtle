/**
 @authors Creerio, Alexis BOUE, Aurélien LAPLAUD, Katarina

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QFileDialog>
#include "animationlibrary.h"
#include "ui_animationlibrary.h"
#include "QMessageBox"

namespace lib_library {
    AnimationLibrary::AnimationLibrary(QWidget *parent) :
        QWidget(parent)
    {
        m_ui = new Ui::AnimationLibrary();
        m_ui->setupUi(this);

        // Link buttons to m_layout
        m_ui->widButtons->set_layout(m_ui->widLibrary);
    }

    AnimationLibrary::~AnimationLibrary() {
        m_ui->widLibrary->clear(true);
        delete m_ui;
    }

    void AnimationLibrary::addAnimation() {
        m_ui->widLibrary->add(lib_animation::Animation::new_animation());
        m_ui->widLibrary->clear_selection();
        m_ui->widLibrary->load_last_animation();
        /* For testing
        m_ui->widLibrary->add(lib_animation::Animation::new_animation(Qt::black));
        m_ui->widLibrary->add(lib_animation::Animation::new_animation(Qt::blue));
        m_ui->widLibrary->add(lib_animation::Animation::new_animation(Qt::red));
        m_ui->widLibrary->add(lib_animation::Animation::new_animation(Qt::yellow));
        m_ui->widLibrary->add(lib_animation::Animation::new_animation(Qt::gray));
        m_ui->widLibrary->add(lib_animation::Animation::new_animation(Qt::darkBlue));
        m_ui->widLibrary->add(lib_animation::Animation::new_animation(Qt::darkMagenta));
        */
    }

    void AnimationLibrary::save_all_png(){
        m_ui->widLibrary->save_all_png();
    }

    void AnimationLibrary::save_all_gif(){
        m_ui->widLibrary->save_all_gif();
    }

    void AnimationLibrary::save_session(const QString &zip_path){
        m_ui->widLibrary->save_session(zip_path);
    }

    void AnimationLibrary::load_session(const QString &zip_path) {
        m_ui->widLibrary->load_session(zip_path);
    }

    void AnimationLibrary::import_from_png(){
        QString import_folder = QFileDialog::getExistingDirectory(this, "Select the folder of the animation to import", QDir::homePath());
        if (import_folder.isEmpty()) {return;}
        m_ui->widLibrary->import_from_png(import_folder);
    }

    void AnimationLibrary::import_from_gif(){
        QString import_file = QFileDialog::getOpenFileName(this, "Select a GIF file to import", QDir::homePath(), "GIF Files (*.gif);;");
        if (import_file.isEmpty()) {return;}
        if(QFileInfo file_info(import_file); file_info.suffix() != "gif"){
            QMessageBox::information(this, "Information", "Wrong file type, expected GIF.");
            return;
        }

        m_ui->widLibrary->import_from_gif(import_file);
    }

    lib_library::AnimationLayout* AnimationLibrary::get_layout() {
        return m_ui->widLibrary;
    }
}
