/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QDialog>

namespace lib_dialog {
namespace Ui {
class OnionSkinningDialog;
}

/**
 * @brief The OnionSkinningDialog class used to choose how many frame before and after will be displayed for onion skinning
 */
class OnionSkinningDialog : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief constructor of the class
     * @param parent
     */
    explicit OnionSkinningDialog(QWidget *parent = nullptr);
    /**
     * @brief destructor of the class
     */
    ~OnionSkinningDialog();

    /**
     * @brief sets skin values
     */
    void setValues(const int before, const int after);

    /**
     * @brief updates timeline skin values
     */
    void updateValues();

    const static int nb_frames_before = 3;
    const static int nb_frames_after = 3;

private slots:

    /**
     * @brief when the user clicks on the cancel button
     */
    void on_buttonBox_rejected();

private:
    /**
     * @brief the ui of the dialog
     */
    Ui::OnionSkinningDialog *m_ui;

    /**
     * @brief the number of frame before the current frame
     */
    size_t m_frame_before = nb_frames_before;

    /**
     * @brief the number of frame after the current frame
     */
    size_t m_frame_after = nb_frames_after;

    /**
     * @brief the minimum value of the slider
     */
    size_t m_min_value = 0;

    /**
     * @brief the maximum value of the slider
     */
    size_t m_max_value = 5;

signals:

    /**
     * @brief signal to send the value of the slider to the timeline
     * @param before number of frames before current
     * @param after number of frames after current
     */
    void value_changed(const size_t before, const size_t after);
};
} // namespace lib_dialog
