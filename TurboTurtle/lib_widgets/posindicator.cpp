/**
 @author Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "posindicator.h"
#include "ui_posindicator.h"

#include <QPainter>
#include <QStylePainter>
#include <QDropEvent>
#include <QMimeData>


namespace lib_widgets {
posindicator::posindicator(const QString format, const size_t number, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::posindicator),
    m_itemFormat(format),
    m_number(number),
    m_hovered(false)
{
    ui->setupUi(this);
    setAcceptDrops(true);

}

posindicator::~posindicator()
{
    delete ui;
}

void posindicator::set_number(const size_t val)
{
    m_number = val;
    update();
}

void posindicator::paint(QPainter &painter, const QRect &rect) const
{

    if(m_hovered){
        QColor tmp_color=this->palette().color(QPalette::Highlight);
        painter.fillRect(1, 1, rect.width(), rect.height(),tmp_color) ;
    }

}

void posindicator::paintEvent(QPaintEvent *)
{
    QStylePainter painter(this);
    paint(painter, geometry());
}

void posindicator::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat(m_itemFormat)) {

        m_hovered=true;
        update();
        event->acceptProposedAction();
    }
}

void posindicator::dragLeaveEvent(QDragLeaveEvent *event)
{
    m_hovered=false;
    update();
    event->ignore();

}

void posindicator::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasFormat(m_itemFormat)) {
        QByteArray item_data = event->mimeData()->data(m_itemFormat);
        QDataStream data_stream(&item_data, QIODevice::ReadOnly);

        int itemPos;

        data_stream >> itemPos;

        emit itemDropped(itemPos, m_number);

        m_hovered = false;
        update();
        event->acceptProposedAction();
    }
}

void posindicator::mousePressEvent(QMouseEvent *e) {
    qDebug() << "pos indicator with number " <<  m_number;
}

}
