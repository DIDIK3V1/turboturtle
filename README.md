# TurboTurtle

## Description

This software allows you to draw animations and create stop motion animations while being able to draw on the pictures taken, for a live concert to display using the NDI protocol.


## Contributing
As of now, we do not accept contributors to this project as it is an educational project

## Dependencies

- C++ 20
- [Qt](https://www.qt.io/) Creator
- [Qt](https://www.qt.io/) 6.5.2
- [Qt](https://www.qt.io/) Multimedia
- [OpenCV](https://opencv.org/) 4.8
- [NDI SDK](https://ndi.video/download-ndi-sdk/)
- [zlib](https://www.zlib.net/)
- [Quazip](https://stachenov.github.io/quazip/)

### Windows

Go to the [QT website](https://www.qt.io/download) to get and install QT.

After installing it, go to the [NDI](https://ndi.video/download-ndi-sdk/) website to get and install their SDK in the default folder (C:/Program Files/NDI).

Then you have to install [OpenCV](https://opencv.org/) in the root project folder (where [CMakeLists.txt](TurboTurtle/CMakeLists.txt) is).

For [zlib](https://www.zlib.net/) and [Quazip](https://stachenov.github.io/quazip/), you have to go and compile them. See their documentation for that.

### Linux

No matter your linux distro, please run the following command :
```bash
sudo ln -s /usr/include/opencv4/opencv2/ /usr/include/opencv2
```

This will link opencv properly, allowing you when compiling/running the app to use the library seamlessly.

### Debian/Ubuntu

Debian version requirement : bullseye (11) with [backport](https://backports.debian.org/) support/bookworm (12)
Ubuntu version requirement : 23.04+

Almost everything should be installed using the following command :
```bash
apt install build-essential cmake libopencv-dev qtcreator qt6-multimedia-dev qt6-tools-dev libquazip1-qt6-dev
```

For NDI, go to the second part of [Other distributions](#other-distributions) and run the script.

Warning : While this DOES compile properly, it won't run for unknown reasons. Any pull request fixing this is welcome.

#### Fedora

This has been tested on fedora 39 and may not work with other versions.

```bash
sudo dnf install "qt6-*"
sudo dnf install qt-creator opencv-devel ndi-sdk-devel libndi-sdk quazip-qt6-devel
sudo ln -s /usr/lib64/libndi.so.5 /usr/lib64/libndi.so
```

Warning : While this DOES compile properly, it won't run for unknown reasons. Any pull request fixing this is welcome.

#### Arch/Manjaro

Everything should be installed using the following commands :
```bash
pacman -S qtcreator qt6-multimedia opencv quazip-qt6
pamac install ndi-sdk
```

You can of course substitute `pamac` for your current [AUR helper](https://wiki.archlinux.org/title/AUR_helpers)


### Other distributions

1. Find how your to install on your distribution of choice QT Creator, OpenCV and QT Multimedia, then install them

2. Run the following script to get NDI SDK :
```bash
#!/bin/bash
set -e
INSTALLER_NAME="Install_NDI_SDK_v5_Linux"
INSTALLER_FILE="$INSTALLER_NAME.tar.gz"
pushd /tmp
sudo apt-get install curl
curl -L -o $INSTALLER_FILE https://downloads.ndi.tv/SDK/NDI_SDK_Linux/$INSTALLER_FILE -f --retry 5
tar -xf $INSTALLER_FILE
yes | PAGER="cat" sh $INSTALLER_NAME.sh
rm -rf ndisdk
mv "NDI SDK for Linux" ndisdk
sudo cp -P ndisdk/lib/x86_64-linux-gnu/* /usr/local/lib/
sudo ldconfig
echo libndi installed to /usr/local/lib/
ls -la /usr/local/lib/libndi*
sudo cp -P ndisdk/include/* /usr/include/
echo include files installed to /usr/include
ls -la /usr/include/Processing.NDI.*
rm -rf ndisdk
popd
```

## Compiling

### Windows

In case of any compiling error, please take the DLLs from NDI and OpenCV (Processing.NDI.lib.x64.dll in `C:\Program Files\NDI\NDI 5 SDK\Bin\x64` and Opencv_world480.dll in your OpenCV install) to the same folder as your executable.

### Linux

Fedora only => Modify the [CMakeLists.txt](TurboTurtle/CMakeLists.txt) file to include ndi-sdk like this :
```cmake
if(UNIX AND NOT MACOS)
    target_include_directories(TurboTurtle PRIVATE "/usr/include/ndi-sdk")
    target_link_libraries(TurboTurtle PRIVATE
        ndi
    )
endif()
```

1. Open a terminal in the current folder
2. Run `cd TurboTurtle/` to move to the project's folder
3. `mkdir build/` to create the project's output
4. `cd build/` to move into the created folder
5. `cmake ../` to create the basic make files for the project
6. `make` to build the program


## License

    Copyright (C) 2023-2024 TurboTurtle Contributors

    This file is part of TurboTurtle.

    TurboTurtle is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TurboTurtle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.

## Project status
Active developpement
