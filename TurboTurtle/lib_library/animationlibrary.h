/**
 @author Creerio, Alexis BOUE, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "lib_library/widgets/animationlayout.h"

namespace Ui { class AnimationLibrary; }

namespace lib_library {
    class AnimationLibrary : public QWidget {
        Q_OBJECT

    public:
        explicit AnimationLibrary(QWidget *parent = nullptr);
        ~AnimationLibrary();

        /**
         * @brief addAnimation Used to add one animation. Selects the first animation
         */
        void addAnimation();

        /**
         * @brief save all the animations on the computer as a sequence of png images
         */
        void save_all_png();

        /**
         * @brief save all the animations on the computer as animated GIFs
         */
        void save_all_gif();

        /**
         * @brief Used to save a session
         * @param zip_path Path to the zip file to write to
         */
        void save_session(const QString &zip_path);

        /**
         * @brief Imports a session from a given zip file
         * @param zip_path Path to the zip file to read
         */
        void load_session(const QString &zip_path);

        lib_library::AnimationLayout* get_layout();


    public slots :
        /**
         * @brief loads an animation on the computer
         */
        void import_from_png();

        /**
         * @brief import_from_gif Loads an animation from a gif file
         */
        void import_from_gif();

    private:
        Ui::AnimationLibrary *m_ui;
    };
}
