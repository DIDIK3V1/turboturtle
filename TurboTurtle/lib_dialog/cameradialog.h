/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIB_DIALOG_CAMERADIALOG_H
#define LIB_DIALOG_CAMERADIALOG_H

#include <QDialog>

namespace lib_dialog {

namespace Ui {
class CameraDialog;
}
/**
 * @brief The CameraDialog class used to ask for a camera to take pictures from
 */
class CameraDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CameraDialog(QWidget *parent = nullptr);
    ~CameraDialog();

private slots:
    /**
     * @brief called when accept button is clicked
     */
    void on_buttonBox_accepted();
signals:
    /**
     * @brief send the id of the camera chosen
     * @param camera id
     */
    void camera_chosen(int camera_id);

private:
    /**
     * @brief ui
     */
    Ui::CameraDialog *ui;
};


} // namespace lib_dialog
#endif // LIB_DIALOG_CAMERADIALOG_H
