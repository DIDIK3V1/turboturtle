/**
 @author Creerio

 @section License

 Copyright (C) 2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "aboutdialog.h"
#include "ui_aboutdialog.h"

namespace lib_dialog {
    AboutDialog::AboutDialog(QWidget *parent)
        : QDialog(parent)
        , m_ui(new Ui::AboutDialog)
    {
        m_ui->setupUi(this);
    }

    AboutDialog::~AboutDialog()
    {
        delete m_ui;
    }

    void AboutDialog::on_btn_close_released()
    {
        this->accept();
    }
}
