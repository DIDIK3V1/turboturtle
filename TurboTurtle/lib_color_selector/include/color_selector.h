/**

 @author Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU

 @section License

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef COLOR_SELECTOR_H
#define COLOR_SELECTOR_H


#include <QWidget>




namespace Ui{
class ColorSelector;
}

namespace lib_color_selector{

class ColorSelector : public QWidget
{
    Q_OBJECT;

public:
    explicit ColorSelector(QWidget *parent = nullptr);
    ~ColorSelector();
    QColor color() const;

private slots :
    /**
     * @brief last_colors_used_modify
     */
    void last_colors_used_modify();
    /**
     * @brief on_wheel_on_drag
     */
    void on_wheel_on_drag();
    /**
     * @brief on_edit_hex_textEdited
     * @param arg1
     */
    void on_edit_hex_textEdited(const QString &arg1);
    /**
     * @brief update_widgets
     */
    void update_widgets();
    /**
     * @brief set_color_internal
     * @param c
     */
    void set_color_internal(const QColor &c);
    /**
     * @brief update_hex
     */
    void update_hex();
    /**
     * @brief on_butWhite_left_clicked
     */
    void on_butWhite_left_clicked();
    /**
     * @brief on_butRed_left_clicked
     */
    void on_butRed_left_clicked();
    /**
     * @brief on_butOrange_left_clicked
     */
    void on_butOrange_left_clicked();
    /**
     * @brief on_butYellow_left_clicked
     */
    void on_butYellow_left_clicked();
    /**
     * @brief on_butGreen_left_clicked
     */
    void on_butGreen_left_clicked();
    /**
     * @brief on_butCyan_left_clicked
     */
    void on_butCyan_left_clicked();
    /**
     * @brief on_butBlue_left_clicked
     */
    void on_butBlue_left_clicked();
    /**
     * @brief on_butMagenta_left_clicked
     */
    void on_butMagenta_left_clicked();
    /**
     * @brief on_butBlack_left_clicked
     */
    void on_butBlack_left_clicked();
    /**
     * @brief on_butCustom1_left_clicked
     */
    void on_butCustom1_left_clicked();
    /**
     * @brief on_butCustom2_left_clicked
     */
    void on_butCustom2_left_clicked();
    void on_butCustom3_left_clicked();
    void on_spin_alpha_valueChanged(int arg1);
    void on_wheel_on_click_release();
    void on_butBackground_right_clicked();
    void on_butForeground_right_clicked();
    void on_butBackground_left_clicked();
    void on_butForeground_left_clicked();
    void on_slide_alpha_sliderMoved(int);

public slots :
    void on_color_picked(const QColor &color);

    void on_slide_alpha_valueChanged(int value);

    void increase_alpha(unsigned);

    void decrease_alpha(unsigned);

signals:
    void color_changed(QColor);

private:
    Ui::ColorSelector *m_ui;
    unsigned short m_button_index;
    void m_last_colors_used_modify();
};
}
#endif // COLOR_SELECTOR_H


