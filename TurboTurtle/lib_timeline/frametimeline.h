/**
 @authors Creerio, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QWidget>
#include "lib_animation/animation.h"
#include "lib_dialog/onionskinningdialog.h"
#include "lib_timeline/widgets/timelinelayout.h"

namespace Ui { class FrameTimeline; }

namespace lib_timeline {
    class FrameTimeline : public QWidget
    {
        Q_OBJECT

    public:
        explicit FrameTimeline(QWidget *parent = nullptr);
        ~FrameTimeline();

        lib_timeline::TimelineLayout* get_layout();

        /**
         * Gives the number of frames before
         * @return an int with the number of frames before
         */
        const int get_frames_before() const;

        /**
         * Gives the number of frames after
         * @return an int with the number of frames after
         */
        const int get_frames_after() const;

        /**
         * @brief Sets if the animation is enabled or not
         * Used when playing an application
         * @param is_started If the animation is started or not
         */
        void set_anim_started(bool is_started);

        /**
         * @brief Sets if the camera is enabled or not
         *
         * @param is_started
         * If the camera is started or not
         */
        void set_cam_started(bool is_started);

        /**
         * @brief Uses multiple frames to create a new frame with onion skinned effects
         * @return A frame with onion skinning
         */
        lib_frame::Frame* get_onion_skinned_frame(int pos);

        void add_new_frame();

    private:
        /**
         * @brief UI used by QT
         */
        Ui::FrameTimeline *m_ui;

        /**
         * @brief If the animation is launched or not
         */
        bool m_anim_started = false;

        /**
         * @brief If the camera is currently used or not
         */
        bool m_cam_started = false;

        /**
         * @brief Number of skins for the onion skinning, frames before
         */
        int m_nb_skins_before = lib_dialog::OnionSkinningDialog::nb_frames_before;

        /**
         * @brief Number of skins for the onion skinning, frames after
         */
        int m_nb_skins_after = lib_dialog::OnionSkinningDialog::nb_frames_after;

    signals:
        /**
         * @brief animation_loaded Signal launched when an animation is loaded
         * @param anim QWidget of the animation
         */
        void animation_loaded(QWidget *anim);

        /**
         * @brief Signal used for the canvas
         * Gives a hidden frame, containing the frame itself without onion skinning and a displayed with onion skinning
         * @param frame_hidden Frame without onion skinning
         * @param frame_displayed Same frame, with onion skinning
         */
        void load_into_canvas(lib_frame::Frame *frame_hidden, lib_frame::Frame *frame_displayed);

    public slots:
        /**
         * @brief Loads an animation into the timeline
         * @param anim Animation to load
         */
        void load_animation(QWidget *anim);

        /**
         * @brief Updates onion skinning values
         * @param before Number of layers before
         * @param after Number of layers after
         */
        void update_skins(const size_t before, const size_t after);

        /**
         * @brief Updates the frame thumbnail of the modified frame
         * @param frame Frame modified
         */
        void update_frame_thumbnail(lib_frame::Frame const *frame, const bool reloadFrame = false) const;

    private slots:
        /**
         * @brief Used to retrieve the frame button and send the frame with onion skinning to the canvas
         * @param btn Button with frame to recover
         */
        void get_frame_to_canvas(QWidget *btn);
    };


}
