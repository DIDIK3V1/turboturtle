/**
 @author Creerio

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QFileDialog>
#include <QMessageBox>
#include "framemenu.h"
#include "lib_timeline/widgets/timelinelayout.h"

namespace lib_timeline {
    FrameMenu::FrameMenu(FrameButton *parent) : QMenu((QWidget*)parent), m_btn(parent), m_layout(m_btn->layout), m_loaded(std::bit_cast<const FrameButton*>(m_layout->get_loaded())) {
        // Actions allowed
        this->addSection("Timeline");
        this->addAction("Insert new before", this, &FrameMenu::insert_before);
        this->addAction("Insert new after", this, &FrameMenu::insert_after);
        this->addAction("Duplicate", this, &FrameMenu::duplicate);
        this->addAction("Delete", this, &FrameMenu::remove);

        this->addSection("Frame");
        this->addAction("Load", this, &FrameMenu::load);
        this->addAction("Save to PNG", this, &FrameMenu::save_png);
    }

    FrameMenu::~FrameMenu() {
        if (m_btn != nullptr)
            m_btn->set_border(false);
        if (m_loaded != nullptr) {
            m_layout->clear_selection();
            m_layout->select(m_loaded->get_pos());
        }
    }

    void FrameMenu::insert_before() {
        m_layout->insert(new lib_frame::Frame(Qt::white), m_btn->get_pos());
    }

    void FrameMenu::insert_after() {
        m_layout->insert(new lib_frame::Frame(Qt::white), m_btn->get_pos() + 1);
    }

    void FrameMenu::duplicate() {
        m_layout->insert(new lib_frame::Frame(*m_btn->get()), m_btn->get_pos() + 1);
    }

    void FrameMenu::remove() {
        QMessageBox msgBox;
        msgBox.setText("Do you want to remove the currently selected frame?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::No);
        if (int ret = msgBox.exec(); ret != QMessageBox::Yes)
            return;


        const bool is_same_loaded = m_btn == m_loaded;
        if (m_layout->remove(m_btn->get_pos())) {
            m_btn = nullptr;

            // Mark the current loaded button as null, because it was deleted
            if (is_same_loaded)
                m_loaded = nullptr;
            else
                m_layout->reload_frame();
        }
    }

    void FrameMenu::load() {
        emit m_layout->load_item(m_btn);
        m_loaded = m_btn;
    }

    void FrameMenu::save_png() {
        // Get file path for save
        QUrl file_path = QFileDialog::getSaveFileUrl(nullptr, "Select where the frame will be saved", QDir::homePath() + "/untitled.png", "Images (*.png)");
        if (file_path.isEmpty()) {return;}

        m_btn->get()->image.save(file_path.toLocalFile(), "PNG");
    }
}
