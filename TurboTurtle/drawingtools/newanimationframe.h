/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef NEWANIMATIONFRAME_H
#define NEWANIMATIONFRAME_H

#include <QWidget>

namespace Ui {
class NewAnimationFrame;
}

namespace lib_drawing_tools {
/**
 * @brief The NewAnimationFrame class, manage 2 buttons : "New Animation" et "+ Frame"
 */
class NewAnimationFrame : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief NewAnimationFrame constructor
     * @param parent of the class
     */
    explicit NewAnimationFrame(QWidget *parent = nullptr);


    ~NewAnimationFrame();

public slots :
    /**
     * @brief Slot that sends a signal to MainWindow to create a new frame
     */
    void on_butAddFrame_clicked();

    /**
     * @brief Slot that sends a signal to MainWindow to create a new animation
     */
    void on_butNewAnimation_clicked();

signals:

    /**
     * @brief Signal for adding a new frame connected to a slot in MainWindow
     */
    void add_frame();

    /**
     * @brief Signal for adding a new animation connected to a slot in MainWindow
     */
    void new_animation();

private:
    /**
     * @brief ui, 2 buttons
     */
    Ui::NewAnimationFrame *ui;
};
} // namespace lib_drawing_tools
#endif // NEWANIMATIONFRAME_H
