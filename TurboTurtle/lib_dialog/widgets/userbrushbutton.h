/**
 @author Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef TURBOTURTLE_USERBRUSHBUTTON_H
#define TURBOTURTLE_USERBRUSHBUTTON_H

#include <QPushButton>

namespace lib_dialog {

    class userBrushButton : public QPushButton {
        Q_OBJECT


    public:

        /**
         * @brief The path to the image used by the button in the user brushes directory
         */
        const QString path;


        /**
         * @brief This function will delete the image associated to this button
         */
        void DeleteImg();

        userBrushButton(QString image_path, QWidget *parent = nullptr);

    };

} // lib_dialog

#endif //TURBOTURTLE_USERBRUSHBUTTON_H
