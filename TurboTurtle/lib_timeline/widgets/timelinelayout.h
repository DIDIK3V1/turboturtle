/**
 @authors Creerio, Katarina, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "lib_widgets/templates/templatedhlayout.h"
#include "lib_animation/animation.h"
#include "framebutton.h"
#include "qshortcut.h"
#include <stack>

namespace lib_timeline {

    class TimelineLayout : public lib_widgets::TemplatedHLayout<lib_timeline::FrameButton, lib_frame::Frame> {
    Q_OBJECT

    public:
        explicit TimelineLayout(QWidget * parent = nullptr);

        ~TimelineLayout() = default;

        void load_frame(int pos);

        /**
         * @brief Loads the first frame for the canvas
         * Should be used when loading an animation
         */
        void load_first_frame();

        /**
         * @brief Loads the same frame for the canvas
         * Should be used when updating a specific frame
         */
        void reload_frame();

        /**
         * @brief Loads the last frame for the canvas
         * Should be used when loading an animation
         */
        void load_last_frame();

        /**
         * @brief Updates frame button's thumbnail
         *
         * @param frame
         * Frame updated
         */
        void update_frame_thumbnail(lib_frame::Frame const *frame) const;

        /**
         *  the function add, insert, remove and move_item
         *  override these function from the parent class
         */

        const bool add(lib_frame::Frame *frame);

        const bool insert(lib_frame::Frame *frame, const size_t pos);

        const bool remove(const int &pos);

        const bool move_item(const size_t &from, size_t to);

        void select(const int pos);

        void clear(const bool is_delete = false);

        /**
         * @brief Animation currently used
         */
        lib_animation::Animation *m_animation = nullptr;

        const std::string get_name() const final
        {
            return "TimelineLayout";
        }

        /**
         * @brief return the value of m_is_playing
         * @return m_is_playing
         */
        const bool is_playing() const;

        /**
         * @brief set the value of m_is_playing
         * @param playing
         */
        void set_is_playing(const bool playing);

        /**
         * @brief launch the animation
         * @param loop indicate if the animation must loop or not
         */
        void play_animation(bool loop);

        /**
         * @brief stop the playing animation
         */
        void end_animation();

    private:

        /**
         * @brief m_ins_playing boolean to know if the animation in the timeline is playing
         */
        bool m_is_playing;

        /**
         * @brief m_timer Timer used to update the animation
         */
        QTimer *m_timer;

        /**
         * @brief index of the current frame displayed on the canvas
         */
        size_t m_frame_displayed;

        /**
         * @brief boolean indicating if the animation is looping or not
         */
        bool m_is_looping;

        /**
         * @brief called by the timer to update the animation played in a loop
         */
        void update_loop();

        /**
         * @brief called by the timer to update the animation played
         */
        void update_once();

        /**
         * @brief Shortcuts used in the library
         */
        std::stack<std::unique_ptr<QShortcut>> m_shortcuts;

    private slots:
        /**
         * @brief load_previous Loads the previous frame
         */
        void load_previous();

        /**
         * @brief load_previous Loads the next frame
         */
        void load_next();

    signals:

        /**
         * @brief signal send to TimelineTools to indicate
         * that the animation has stopped playing
         */
        void stop_animation();

    };


}// namespace lib_timeline
