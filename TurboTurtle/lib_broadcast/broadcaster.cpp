/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "broadcaster.h"
#include <QMessageBox>
#include <opencv2/opencv.hpp>

namespace lib_broadcast {

BroadCaster::BroadCaster(QObject *parent)
    : QObject{parent}, m_frame_to_send(nullptr),
    m_broadcast_mode(broadcast_mode::SEQUENTIAL),
    m_composition_mode(QPainter::CompositionMode_Multiply),
    m_is_composition_broadcast(false)
{
    m_blackscreen = new lib_frame::Frame(Qt::black);

    m_live_timer = new QTimer(this);
    m_pos_anim = 0;
    m_is_live = false;
    m_frame_time = 1; //12 FPS
    m_ndi = new lib_ndi::ndimanager;
    m_ndi_state = true;
}

BroadCaster::~BroadCaster()
{
    qDebug() << "trying to delete Broadcaster";

    if(m_is_composition_broadcast)
        delete_broadcast_frames();

    if(m_is_live){
        disconnect(m_live_timer, SIGNAL(timeout()), this, SLOT(update_live_broadcast()));
    }

    delete m_live_timer;

    if (!m_ndi->isFinished()) {
        m_ndi->quit();
    }

    delete m_ndi;
    qDebug() << "Broadcaster deleted successfully";
}

void BroadCaster::update_live_broadcast()
{
    QImage img = m_animation[m_pos_anim % m_animation.size()]->image.rgbSwapped();

    if(m_ndi_state)
    {
        m_ndi->set_ndi_img(m_animation[m_pos_anim % m_animation.size()]);
    }
    m_pos_anim++;

    cv::Mat mat(img.height(),
                img.width(),
                CV_8UC4,
                const_cast<uchar *>(img.bits()),
                static_cast<size_t>(img.bytesPerLine()));

    imshow("TurboTurtle - Live Window", mat);
}

void BroadCaster::update_live_drawing(lib_frame::Frame &frame)
{
    if(m_frame_to_send == nullptr || m_frame_to_send != &frame) {
        m_frame_to_send = &frame;
        m_ndi->set_ndi_img(&frame);
    }
    QImage img = frame.image.rgbSwapped();
    if(m_ndi_state)
    {
        m_ndi->set_ndi_img(&frame);

        if(!m_ndi->isRunning())
        {
            m_ndi->start();
        }
    }
    else
    {
        if(m_ndi->isRunning())
        {
            m_ndi->set_ndi_img(m_blackscreen);
            m_ndi->quit();
        }
    }

    cv::Mat mat(img.height(),
                img.width(),
                CV_8UC4,
                const_cast<uchar *>(img.bits()),
                static_cast<size_t>(img.bytesPerLine()));

    imshow("TurboTurtle - Live Window", mat);
}

void BroadCaster::start_stop_live_drawing(bool b)
{
    if (b)
    {
        if(m_ndi_state)
        {
            m_ndi->set_ndi_img(m_blackscreen);
            m_ndi->start();
        }

        cv::namedWindow("TurboTurtle - Live Window", cv::WINDOW_NORMAL);

        connect(m_live_timer, &QTimer::timeout, this, &BroadCaster::get_canvas_frame);
        m_live_timer->stop();
        m_live_timer->start(42); // to live in 24fps
    } else {

        disconnect(m_live_timer, &QTimer::timeout, this, &BroadCaster::get_canvas_frame);

        cv::Mat mat(m_blackscreen->image.height(),
                    m_blackscreen->image.width(),
                    CV_8UC4,
                    const_cast<uchar *>(m_blackscreen->image.bits()),
                    static_cast<size_t>(m_blackscreen->image.bytesPerLine()));

        imshow("TurboTurtle - Live Window", mat);
        if(m_ndi_state)
        {
            m_ndi->set_ndi_img(m_blackscreen);
            m_ndi->quit();
        }
    }
}

void BroadCaster::start_stop_broadcast(int fps)
{
    m_frame_time = 1000 / fps;
    if (!m_is_live)
    {
        emit ask_for_frames();

        emit anim_started(true);
        m_is_live = true;

    } else
    {
        emit anim_started(false);

        disconnect(m_live_timer, SIGNAL(timeout()), this, SLOT(update_live_broadcast()));
        m_animation.clear();
        m_pos_anim = 0;
        m_is_live = false;

        cv::Mat mat(m_blackscreen->image.height(),
                    m_blackscreen->image.width(),
                    CV_8UC4,
                    const_cast<uchar *>(m_blackscreen->image.bits()),
                    static_cast<size_t>(m_blackscreen->image.bytesPerLine()));

        imshow("TurboTurtle - Live Window", mat);
        if(m_ndi_state)
        {
            m_ndi->set_ndi_img(m_blackscreen);
        m_ndi->quit();
        }
    }
}

void BroadCaster::frame_composition(const std::vector<lib_animation::Animation*> &animations) {

    int image_width = animations.at(0)->at(0)->image.width();
    int image_height = animations.at(0)->at(0)->image.height();

    // total length of the composition (longest animation)
    int total_length = 0;
    for(lib_animation::Animation *anim : animations) {
        if (anim->size() > total_length) total_length = anim->size();
    }

    for(int image_index = 0; image_index<total_length; image_index++) {
        QImage image(image_width,
                     image_height,
                     QImage::Format_RGBA8888);
        image.fill(Qt::transparent);

        QPainter painter(&image);

        painter.setCompositionMode(m_composition_mode);

        for(lib_animation::Animation *anim: animations) {
            if(image_index < anim->size()) {
                painter.drawImage(0, 0, anim->at(image_index)->image);
            }
        }

        lib_frame::Frame *frame = new lib_frame::Frame(image);
        m_animation.push_back(frame);
    }

    qDebug() << "composition length:" << total_length;
}

void BroadCaster::delete_broadcast_frames() {
    m_is_composition_broadcast = false;
    for(lib_frame::Frame *frame: m_animation) {
        delete frame;
    }

    m_animation.clear();
}

void BroadCaster::set_broadcast_mode(broadcast_mode mode)
{
    m_broadcast_mode = mode;
}

void BroadCaster::set_composing_mode(QPainter::CompositionMode mode)
{
    m_composition_mode = mode;
}

void BroadCaster::process_frames_from_cb(const std::optional<std::vector<lib_animation::Animation*>> &anim)
{
    delete_broadcast_frames();


    // animation broadcast logic here
    if (anim != std::nullopt) {


        switch(m_broadcast_mode)
        {

            case broadcast_mode::COMPOSITION:
                m_is_composition_broadcast = true;
                frame_composition(anim.value());
                break;
            case broadcast_mode::SEQUENTIAL:
                std::vector<lib_animation::Animation*> animations = anim.value();
                for(auto & animation : animations)
                {
                    for(lib_frame::Frame *frame : *animation)
                {
                        m_animation.push_back(frame);
                }
            }
            break;
        }

        // qDebug() << "Broadcaster : got frames from" << animations.size() << "anim";
    } else {
        m_animation.push_back(m_blackscreen);
        qDebug() << "Broadcaster : default frame constructed";
    }

    if (!this->m_animation.empty())
    {
        lib_frame::Frame *img = m_animation[0];
        if(m_ndi_state)
        {
            m_ndi->set_ndi_img(img);
        m_ndi->start();
        }
    }

    cv::namedWindow("TurboTurtle - Live Window", cv::WINDOW_NORMAL);

    connect(m_live_timer, SIGNAL(timeout()), this, SLOT(update_live_broadcast()));
    m_live_timer->stop();
    m_live_timer->start(m_frame_time);
    m_is_live = true;
}

void BroadCaster::start_stop_ndi(const bool state)
{
    if(m_is_live)
    {
        if(state)
        {
            m_ndi->start();
        }
        else
        {
            m_ndi->set_ndi_img(m_blackscreen);
            m_ndi->quit();
        }
    }

    m_ndi_state = state;
}
} // namespace lib_broadcast
