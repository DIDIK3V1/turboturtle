/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Katarina

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CANVASCURSOR_H
#define CANVASCURSOR_H

#include <QCursor>
#include <QPixmap>
#include "drawingtools/brush.h"
namespace lib_canvas {
/**
 * @brief Cursor wrapper used to know the cursor for the canvas
 */
class CanvasCursor
{
public:
    /**
     *  @brief CanvasCursor Constructor taking a cursor
     *  @param cursor used to display
     */
    CanvasCursor(const QCursor &cursor);

    /**
     * @brief CanvasCursor Default constructor
     */
    CanvasCursor();

    /**
     * @brief set_cursor setter of the cursor
     * @param cursor to set
     */
    void set_cursor(const QCursor &cursor);

    /**
     * @brief get_cursor getter of the cursor
     * @return the cursor
     */
    QCursor get_cursor() const;

    /**
     * @brief update_cursor Updates the cursor to the corresponging drawing tool
     * @param tool to get cursor from
     */
    void update_cursor(const lib_drawing_tools::DrawingTool &tool);

    /**
     * @brief update_thickness update the cursor with the corresponding size
     * @param thickness New thickness of the cursor
     */
    void update_thickness(int thickness);

private:

    /**
     * @brief m_cursor cursor of the class
     */
    QCursor m_cursor;

    /**
     * @brief m_tool tool currently selected
     */
    lib_drawing_tools::DrawingTool m_tool;

    /**
     * @brief m_thickness thickness (size) of the cursor
     */
    int m_thickness = 1;

    /**
     * @brief picker_cursor private function that generate the color picker cursor
     */
    void picker_cursor();

    /**
     * @brief picker_cursor private function that generate the fill bucket cursor
     */
    void bucket_cursor();

    /**
     * @brief picker_cursor private function that generate the eraser cursor
     */
    void eraser_cursor();

    /**
     * @brief sets the cursor as a selector cursor from an image
    */
    void rectangle_selector_cursor();
    /**
     * @brief sets the cursor as a moving hand
    */
    void hand_cursor();
};
} // namespace lib_canvas

#endif // CANVASCURSOR_H
