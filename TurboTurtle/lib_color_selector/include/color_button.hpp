/**

 @author Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU

 @section License

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COLOR_BUTTON_H
#define COLOR_BUTTON_H

#include <QPushButton>

namespace lib_color_selector{
class ColorButton : public QPushButton
{
    Q_OBJECT

public:
    /**
     * @brief ColorButton : button with a background color
     * @param parent : parent, nullptr by default
     * @param color : color of the button, white by default
     */
    ColorButton( QWidget *parent=nullptr, const QColor & = QColor(255,255,255,255));

    /**
     * @brief change_color : change the color of the button
     */
    void change_color(const QColor &);

    /**
     * @brief get_color
     * @return : returns the color of the button
     */
    QColor get_color() const;

    /**
     * @brief paint : paint the rectangle of the button
     * @param painter to change color
     * @param rectangle to draw on
     */
    void paint(QPainter &,QRect) const;

private slots:
    /**
     * @brief mousePressEvent : handle the mouse press on left or right click
     * @param emits right_clicked() or left_clicked()
     */
    void mousePressEvent(QMouseEvent *e);


signals:
    /**
     * @brief right_clicked : emitted when right clicked
     */
    void right_clicked();
    /**
     * @brief left_clicked : emitted when left clicked
     */
    void left_clicked();
protected :
    /**
     * @brief paintEvent : paint the background of the button
     * @param event
     */
    void paintEvent(QPaintEvent *event);
private :
    /**
     * @brief back : a brush to paint the color of the button
     */
    QBrush back;
};
}

#endif // COLOR_BUTTON_H

