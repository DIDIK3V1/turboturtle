/**

 @author Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef DRAWINGTOOLS_H
#define DRAWINGTOOLS_H

#include "drawingtools/brush.h"
#include <QGroupBox>
#include <QSlider>
#include <QSpinBox>
#include <iostream>
#include <QPushButton>
namespace Ui {
class DrawingTools;
}
namespace lib_drawing_tools {

    class DrawingTools : public QGroupBox
    {
        Q_OBJECT

    public:
        /**
         * @brief Constructor of DrawingTools
         * @param parent of the class
         */
        explicit DrawingTools(QWidget *parent = nullptr);

        /**
         * @brief Destructor of the class
         */
        ~DrawingTools();

        /**
         * @brief ui;
         */
        Ui::DrawingTools *m_ui;

        /**
         * @brief get the stylesheet of the clicked button
        */
        std::stringstream get_clicked_stylesheet();

    private:
        /**
         * @brief handles the thickeness of the brush
         */
        QSlider* m_slider_thickness;

        /**
         * @brief display the value of the thickeness of the brush
         */
        QSpinBox* m_spin_box_thickness;

        /**
         * @brief pencil brush image
         */
        lib_drawing_tools::Brush m_pencil_brush;

        /**
         * @brief paint brush image
         */
        lib_drawing_tools::Brush m_paint_brush;

        /**
         * @brief marker brush image
         */
        lib_drawing_tools::Brush m_marker_brush;

        /**
         * @brief user's brush image
         */
        lib_drawing_tools::Brush m_user_brush;

        QPushButton *m_selected_button;

    public slots:
        /**
         * @brief update the Slider when the spinbox is updated
         * @param width value
         */
        void updateSliderFromSpinBox(const size_t value);

        /**
         * @brief update the spinbox when the slider is updated
         * @param value width value
         */
        void updateSpinBoxFromSlider(const size_t value);

        /**
         * @brief change the brush to be a paintbrush
         */
        void on_butPaintbrush_clicked();

        /**
         * @brief change the brush to be a marker
         */

        void on_butMarker_clicked();
        /**
         * @brief change the brush to be a pencil
         */
        void on_butPencil_clicked();


        /**
         * @brief change the brush to be an eraser
         */
        void on_butEraser_clicked();

        /**
         * @brief change the brush to be the color picker
         */
        void on_butColorPicker_clicked();

        /**
         * @brief change the brush to be a fill bucket
         */
        void on_butBucket_clicked();

        /**
         * @brief undo the previous action on the canvas
         */
        void on_butUndo_clicked();

        /**
         * @brief redo the previous action on the canvas
         */
        void on_butRedo_clicked();

        /**
         * @brief add a border to the button clicked
         */
        void button_Clicked();


        /**
         * @brief increase the thickness of the brush
         */
        void increase_thickness(unsigned);

        /**
         * @brief decrease the thickness of the brush
         */
        void decrease_thickness(unsigned);

        /**
         * @brief open a window to create or select a brush imported by the user
         */
        void on_butUserbrush_clicked();

        /**
         * @brief selectUserBrush change the brush to the user's brush image
         * @param path of the brush's image
         */
        void selectUserBrush(QString path);

    signals:
        /**
         * @brief emited on brush thickness change
         * @param thickness value to send
         */
        void thicknessChange(size_t value);

        /**
         * @brief signal to emit when a brush is changed, send the brush texture
         * @param image to send
         */
        void brushChange(const lib_drawing_tools::Brush &);


        /**
         * @brief signal with the newly selected drawing tool
         */
        void toolChange(lib_drawing_tools::DrawingTool);

        /**
         * @brief signal to undo the previous action on the canvas
         */
        void undo();

        /**
         * @brief signal to redo the previous action on the canvas
         */
        void redo();
    };

} // namespace lib_drawing_tools
#endif // DRAWINGTOOLS_H
