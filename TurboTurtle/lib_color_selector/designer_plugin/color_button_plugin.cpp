/**

@author Mattia Basaglia

@section License

    Copyright (C) 2013 Mattia Basaglia

    This file is part of Color Widgets.

    Color Widgets is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Color Widgets is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Color Widgets.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "color_button_plugin.hpp"
#include "../include/color_button.hpp"
#include <QtPlugin>

Color_Button_Plugin::Color_Button_Plugin(QObject *parent)
    : QObject(parent), initialized(false)
{}


void Color_Button_Plugin::initialize(QDesignerFormEditorInterface *)
{
    if (initialized)
        return;

    initialized = true;
}

bool Color_Button_Plugin::isInitialized() const
{
    return initialized;
}

QWidget *Color_Button_Plugin::createWidget(QWidget *parent)
{
    return new lib_color_selector::ColorButton(parent);
}

QString Color_Button_Plugin::name() const
{
    return "ColorButton";
}

QString Color_Button_Plugin::group() const
{
    return "Color Widgets";
}

QIcon Color_Button_Plugin::icon() const
{
    return QIcon();
}

QString Color_Button_Plugin::toolTip() const
{
    return "Flat Button That display a color";
}

QString Color_Button_Plugin::whatsThis() const
{
    return toolTip();
}

bool Color_Button_Plugin::isContainer() const
{
    return false;
}

QString Color_Button_Plugin::domXml() const
{

    return "<ui language=\"c++\">\n"
           " <widget class=\"ColorPreview\" name=\"ColorPreview\">\n"
           " </widget>\n"
            "</ui>\n";
}

QString Color_Button_Plugin::includeFile() const
{
    return "Color_Preview";
}

//Q_EXPORT_PLUGIN2(color_widgets, Color_Preview_Plugin);



