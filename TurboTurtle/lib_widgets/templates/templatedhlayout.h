/**
 @authors Creerio, Katarina, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "lib_widgets/hlayout.h"
#include "lib_widgets/posindicator.h"
#include "templatedimagebutton.h"

namespace lib_widgets {
    /**
     * @brief The TemplatedHLayout class
     * Templated version of the HLayout class
     * Separated to allow the Q_Object macro to work as intended with our templated class
     */
    template<class Stored, class Given>
    class TemplatedHLayout : public HLayout {
        static_assert(std::is_base_of_v<TemplatedImageButton<Given>, Stored>, "Stored type must inherit from ImageButton");

    public:
        /**
         * @brief Default constructor of a HLayout object
         * @param parent Parent QWidget
         */
        explicit TemplatedHLayout(QString mime_type, QWidget *parent = nullptr);
        ~TemplatedHLayout();

        // No copy constructor : This should NOT be used to copy elements
        // No instanciation operator : This should NOT be used to instantiate a new HLayout

        /**
         * @brief Adds a given item, either an Animation or Frame
         * @param item Animation/Frame to store
         */
        virtual const bool add(Given *item);

        /**
         * @brief Inserts a given item at a given position, either an Animation or Frame
         * @param item Animation/Frame to store
         * @param pos Where to put it
         */
        virtual const bool insert(Given *item, const size_t pos);

        /**
         * @brief Removes a button from our storage using it's position
         * @param pos Position of the AnimationButton/FrameButton, can be negative
         */
        virtual const bool remove(const int &pos);

        /**
         * @brief Removes a button from our storage using the object itself
         * @param elem AnimationButton/FrameButton to remove
         */
        const bool remove(const Stored *elem);

        /**
         * @brief Removes the current selection of element
         */
        void remove_selection();

        /**
         * @brief Moves one button to another position
         * @param from Position of the AnimationButton/FrameButton
         * @param to Where to put it. A Copy is used to be able to modify it sometimes
         */
        virtual const bool move_item(const size_t &from, size_t to);

        /**
         * @brief Empties the QHBoxLayout containing the AnimationButtons/Frames
         * WARNING : This also DELETES every Animation/Frame stored !
         */
        virtual void clear(const bool is_delete = false);

        /**
         * @brief Empties the QHBoxLayout containing the AnimationButtons/Frames
         * This is only visual and keeps the Animations/Frames buttons stored
         */
        void clear_view();

        /**
         * @brief Empties the QHBoxLayout containing the AnimationButtons/Frames
         * This also removes any stored buttons, without affecting the Animations/Frames behind
         */
        void empty();

        /**
         * @brief Clears the QHBoxLayout containing the AnimationButtons/FrameButtons to add them back
         */
        void refresh();

        /**
         * @brief Returns a pointer to the currently selected button
         * @return A pointer to the selected button or nullptr if nothing is selected
         */
        const std::vector<const Stored*> get_current() const;

        /**
         * @brief Returns a copy of the currently selected item
         * Please make sure to delete the item when finished !
         * @return A copy of the selected item or nullptr if nothing is selected
         */
        std::vector<Given*> get_current_copy() const;

        /**
         * @brief Returns the 'maximum' position of the selected elements
         * @return An int defining what object is selected, -1 is for no object selected
         */
        const int get_current_pos() const;

        /**
         * @brief Selects an object
         * @param pos Position of the item to select
         */
        virtual void select(const int pos);


        /**
         * @brief Called when clicking on an object, selects one object
         * @param pos  Position of the item to select
         */
        void click_select(const int pos);

        /**
         * @brief Selects many objects
         * @param pos Position of the item to select
         */
        void ctrl_select(const int pos);

        /**
         * @brief Unselects an object
         * @param pos Position of the item to unselect
         */
        virtual void unselect(const int pos);

        /**
         * @brief clear the selection of elements in m_current
         */
        void clear_selection();

        /**
         * @brief is_only_selected Indicates if the given element is the only one selected
         */
        bool is_only_selected(const int pos);

        /**
         * @brief Indicates if the HLayout contains any element
         * @return True if no element is stored, else False
         */
        bool is_empty() const;

        virtual const std::string get_name() const = 0;

    protected:
        /**
         * @brief Storage of AnimationButtons or FrameButtons
         */
        std::vector<Stored*> m_items;

        // Force call to this variant of the function
        void save_loaded_item(QWidget *item);

    private :
        /**
         * @brief Separates AnimationButtons or FrameButtons, allowing them to be moved
         */
        std::vector<lib_widgets::posindicator*> m_item_separators;

        /**
         * @brief Currently selected objects indexes
         */
        std::vector<Stored*> m_current;

        /**
         * @brief m_mimeType
         */
        QString m_mimeType ="";

        /**
         * Gives an instance of the button connected with events
         *
         * @param item
         * Item to store into the button
         *
         * @param pos
         * Position of the button
         *
         * @return Instance of the button, ready to use
         */
        Stored* get_stored(Given *item, const size_t &pos = 0);

        /**
         * Gives a position indicator corrected with events
         *
         * @param pos
         * Position of the indicator
         *
         * @return Instance of a position indicator, ready to use
         */
        lib_widgets::posindicator* get_pos_indicator(const size_t &pos = 0);

        /**
         * Used to refresh the list order of the position indicators & buttons
         */
        void refresh_order() const;

    };
}

// Template methods
#include "templatedhlayout.tpp"
