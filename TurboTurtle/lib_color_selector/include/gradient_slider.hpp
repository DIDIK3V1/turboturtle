/**

@author Mattia Basaglia, Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU

@section License

    Copyright (C) 2013-2014 Mattia Basaglia
    Copyright (C) 2014 Calle Laakkonen

    This software is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This software is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Color Widgets.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GRADIENT_SLIDER_HPP
#define GRADIENT_SLIDER_HPP

#include <QGradient>
#include <QMouseEvent>
#include <QSlider>
namespace lib_color_selector {
/**
 * @brief A slider that mover on top of a gradient
 */
class GradientSlider : public QSlider
{
    Q_OBJECT
    Q_PROPERTY(QBrush background READ background WRITE set_background)
    Q_PROPERTY(QGradientStops colors READ colors WRITE set_colors DESIGNABLE false)
    Q_PROPERTY(QColor first_color READ first_color WRITE set_first_color STORED false)
    Q_PROPERTY(QColor last_color READ last_color WRITE set_last_color STORED false)
    Q_PROPERTY(QLinearGradient gradient READ get_gradient WRITE set_gradient)

public:
    explicit GradientSlider(QWidget *parent = 0);
    explicit GradientSlider(const Qt::Orientation &orientation, QWidget *parent = 0);
    ~GradientSlider();

    /**
     * @brief background Get the background, it's visible for transparent gradient stops
     *
     * @return QBrush() if no background is set
    */
    QBrush background() const;

    /**
     * @brief set_background Set the background, it's visible for transparent gradient stops
     * @param bg QBrush() to remove the background
    */
    void set_background(const QBrush &bg);

    /**
     * @brief Get the colors that make up the gradient
     * 
    */
    QGradientStops colors() const;
    /**
     * @brief Set the colors that make up the gradient
     * @param colors The colors
    */
    void set_colors(const QGradientStops &colors);

    /**
     * @brief Get the gradient
     * @return the gradient set
    */
    QLinearGradient get_gradient() const;
    /// Set the gradient
    void set_gradient(const QLinearGradient &gradient);

    /**
     * Overload: create an evenly distributed gradient of the given colors
     */
    void set_colors(const QVector<QColor> &colors);

    /**
     * Set the first color of the gradient
     *
     * If the gradient is currently empty it will create a stop with the given color
     */
    void set_first_color(const QColor &c);

    /**
     * @brief Set the last color of the gradient
     *
     * If the gradient is has less than two colors,
     * it will create a stop with the given color
     */
    void set_last_color(const QColor &c);

    /**
     * @brief Get the first color
     *
     * @returns QColor() con empty gradient
     */
    QColor first_color() const;

    /**
     * @brief Get the last color
     *
     * @return QColor() con empty gradient
     */
    QColor last_color() const;

protected:
    void paintEvent(QPaintEvent *ev) override;

    void mousePressEvent(QMouseEvent *ev) override;

private:
    class Private;
    Private *const m_p;
};
} // namespace lib_color_selector

#endif // GRADIENT_SLIDER_HPP
