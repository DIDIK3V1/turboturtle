/**
 @author Creerio

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <mutex>
#include "hlayout.h"
#include "ui_hlayout.h"

namespace lib_widgets {
    HLayout::HLayout(QWidget *parent) :
        QWidget(parent),
        m_ui(new Ui::widHLayout)
    {
        m_ui->setupUi(this);

        // Update loaded item when loading something
        QObject::connect(this, &HLayout::load_item, this, &HLayout::save_loaded_item);
    }

    HLayout::~HLayout() {
        delete m_ui;
    }

    const QWidget* HLayout::get_loaded() {
        return loaded_item;
    }

    void HLayout::finished_loading(QWidget *item) {
        std::scoped_lock<std::mutex> lock(item_loading_block);
        if (item == loaded_item)
            loading_item = false;
    }

    void HLayout::update_min_height(const size_t &height)
    {
        m_ui->scrollArea->setMinimumHeight(height + 35);
    }

    void HLayout::save_loaded_item(QWidget *item) {
        {
            std::scoped_lock<std::mutex> lock(item_loading_block);
            // Do not load item if it's already doing it
            if (loading_item)
                return;

            loading_item = true;
            loaded_item = item;
        }

        emit start_load_item(item);
    }
}
