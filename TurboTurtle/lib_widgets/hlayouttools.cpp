/**
 @authors Creerio, Katarina

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "hlayouttools.h"
#include "ui_hlayouttools.h"

namespace lib_widgets {
    HLayoutTools::HLayoutTools(QWidget *parent) :
        QWidget(parent)
    {
        m_ui = new Ui::HLayoutTools();
        m_ui->setupUi(this);
    }

    HLayoutTools::~HLayoutTools()
    {
        delete m_ui;
    }

    void HLayoutTools::addWidget(QWidget *widget)
    {
        m_ui->frameAddon->layout()->addWidget(widget);
        m_ui->frameAddon->layout()->setAlignment(widget, Qt::AlignCenter);
    }

    void HLayoutTools::addWidgetShortcut(const QList<QKeySequence> &shortcuts, const std::string &tooltip)
    {
        for (const QKeySequence &shortcut : shortcuts) {
            m_shortcuts.push(std::make_unique<QShortcut>(shortcut, this, SLOT(on_btnAdd_clicked())));
        }
        m_ui->btnAdd->setToolTip(QString::fromStdString(tooltip));
    }

    void HLayoutTools::duplicateWidgetShortcut(const QList<QKeySequence> &shortcuts, const std::string &tooltip)
    {
        for (const QKeySequence &shortcut : shortcuts) {
            m_shortcuts.push(std::make_unique<QShortcut>(shortcut, this, SLOT(on_btnDuplicate_clicked())));
        }
        m_ui->btnDuplicate->setToolTip(QString::fromStdString(tooltip));
    }

    void HLayoutTools::deleteWidgetShortcut(const QList<QKeySequence> &shortcuts, const std::string &tooltip)
    {
        for (const QKeySequence &shortcut : shortcuts)
        {
            m_shortcuts.push(std::make_unique<QShortcut>(shortcut, this, SLOT(on_btnDelete_clicked())));
        }
        m_ui->btnDelete->setToolTip(QString::fromStdString(tooltip));
    }

    void HLayoutTools::remove_delete()
    {
        m_ui->btnDelete->hide();
    }

    void HLayoutTools::remove_frame()
    {
        m_ui->frameAddon->hide();
    }

    void HLayoutTools::update_counter(const size_t &current_loaded, const size_t &nb_elements)
    {
        QString txt;
        txt.append(QString::number(current_loaded));
        txt.append("/");
        txt.append(QString::number(nb_elements));
        m_ui->label_loaded->setText(txt);
    }
}
