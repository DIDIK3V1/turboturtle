/**
@authors Julien BLANCO, Samuel GOUBEAU, Kévin DIDIER, Idaïa METZ, Lucas PLANCHET, Alexis BOUE, Creerio, Katarina, Aurélien LAPLAUD

@section License

    Copyright (C) 2023-2024 TurboTurtle Contributors

    This file is part of TurboTurtle.

    TurboTurtle is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TurboTurtle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TurboTurtle.  If not, see http://www.gnu.org/licenses/.
*/

#include "canvas.h"
#include <QMenu>
#include <QWindow>
#include <QPainter>
#include "ui_canvas.h"
#include <QClipboard>
#include <QPainterPath>

namespace lib_canvas {
QMutex Canvas::m_mutexImg;

Canvas::Canvas(QWidget *parent)
    : QFrame(parent)
    , ui(new Ui::Canvas)
    , m_brush(lib_drawing_tools::Brush(QImage(":/brushes/realBrush7"), 5))
    , m_rubberband(nullptr)
    , m_rectangle_selector_origin(0,0)
    , m_rectangle_selector_end(0,0)
    , m_imagepaster_origin(0,0)
    , m_imagepaster_end(0,0)
    , m_is_area_selected(false)
    , m_rubberband_offset(0,0)
    , m_imagepaster_offset(0,0)
    , m_is_move_rubberband(false)
    , m_is_move_imagepaster(false)
    , m_is_pasting(false)
    , m_imagepaster(nullptr)
    , m_is_polygon_selected(false)
    , m_tool_before_pasting(lib_drawing_tools::BRUSH)
{
    //setAttribute(Qt::WA_StaticContents);
    //enable tablet tracking
    setAttribute(Qt::WA_TabletTracking);
    //enable mouse tracking
    setAttribute(Qt::WA_MouseTracking, true);
    ui->setupUi(this);

    this->resize(1280, 720);
    //Initialiser cursor
    QImage initial_image = QImage(this->geometry().width(), this->geometry().height(), QImage::Format_RGBA8888);

    m_frame_hidden = new lib_frame::Frame(initial_image.copy());
    m_frame_displayed = new lib_frame::Frame(initial_image.copy());

    m_painter = new Painter(m_frame_hidden, m_frame_displayed);

    QObject::connect(m_painter, &Painter::frameModified, this, &Canvas::update_canvas);
    QObject::connect(m_painter, &Painter::updateCursor, this, &Canvas::update_cursor);

    m_painter->set_pen_width(20);

    m_brush.change_width(*m_painter->pen_width());
    m_brush.extract_border(QColor(255, 255, 255));

    m_cursor = CanvasCursor(QCursor(QPixmap::fromImage(m_brush.brush_border())));
    m_cursor.update_thickness(20);

    m_is_camera = false;
    m_onion_skinning_on = true;
    m_is_tablet = false;
    m_camera_reversed = false;

    this->setMinimumWidth(16);
    this->setMinimumHeight(9);
    Canvas::s_canvas_size = size();

    m_frame_camera_displayed = new lib_frame::Frame();
    m_frame_camera_hidden = new lib_frame::Frame();

    qDebug("Canvas created");
}

Canvas::~Canvas()
{
    qDebug() << "trying to delete canvas";
    delete ui;
    delete m_painter;
    delete m_frame_displayed;
    if(m_rubberband != nullptr)
        delete m_rubberband;
    if(m_imagepaster != nullptr)
        delete m_imagepaster;
    qDebug() << "canvas deleted correctly";

    delete m_frame_camera_displayed;
    delete m_frame_camera_hidden;
}

double Canvas::polygon_area(const QPolygon& polygon) {
    double area = 0.0;
    int j = polygon.size() - 1;
    for (int i = 0; i < polygon.size(); i++) {
        area += (polygon[i].x() * polygon[j].y() - polygon[j].x() * polygon[i].y());
        j = i;
    }

    // return the absolute value
    return (area / 2.0 < 0.0) ? 0 - (area / 2.0) : (area / 2.0);
}

void Canvas::paste_clipboard()
{
    const QClipboard *clipboard = QApplication::clipboard();
    const QMimeData *mime_data = clipboard->mimeData();
    QImage img;

    if(mime_data->hasImage()) {
        deselect_selection();
        img = qvariant_cast<QImage>(mime_data->imageData());
        m_imagepaster = new ImagePasterWidget(img, this);
        m_imagepaster->move(0, 0);
        m_imagepaster->resize(img.width(), img.height());
        m_imagepaster->setMinimumSize(10, 10);
        m_is_pasting = true;
        m_tool_before_pasting = m_tool;
        tool_changed(lib_drawing_tools::HAND);
        m_imagepaster_origin = m_imagepaster->geometry().topLeft();
        m_imagepaster_end = m_imagepaster->geometry().bottomRight();
    }
}

void Canvas::resize_keep_aspect_ratio(bool keep_ratio) {
    if(m_imagepaster)
        m_imagepaster->keep_aspect_ratio(keep_ratio);
}

void Canvas::delete_selection()
{
    if(m_rubberband && m_is_area_selected)
    {
        QRect area_to_delete(m_rectangle_selector_origin, m_rectangle_selector_end);
        m_painter->erase_selection(area_to_delete);
        m_frame_hidden->undoredo->add(m_frame_hidden->image.copy());
        m_frame_hidden->is_modified_composing_bench = true;
        deselect_selection();
        emit update_frame_button(m_frame_hidden);
    } else if (m_is_polygon_selected) {
        m_painter->erase_selection(m_polygon);
        m_frame_hidden->undoredo->add(m_frame_hidden->image.copy());
        m_frame_hidden->is_modified_composing_bench = true;
        deselect_selection();
        emit update_frame_button(m_frame_hidden);
    }
}

void Canvas::deselect_selection(bool is_frame_change)
{
    if(m_rubberband)
    {
        m_is_area_selected = false;
        m_rubberband->hide();
        emit update_frame_button(m_frame_hidden);
    }
    if(m_is_polygon_selected || !m_polygon.isEmpty()) {
        m_is_polygon_selected = false;
        m_polygon.clear();
        //force repaint of the canva to hide the selection polygon
        this->repaint();
        emit update_frame_button(m_frame_hidden);
    }
    if(m_is_pasting && m_imagepaster)
    {
        if(!is_frame_change)
            print_pasted_image();
        m_is_pasting = false;
        m_imagepaster->hide();
        m_is_drawing = false;
        m_frame_hidden->undoredo->add(m_frame_hidden->image.copy());
        m_frame_hidden->is_modified_composing_bench = true;
        emit update_frame_button(m_frame_hidden);

        // restore previous tool
        tool_changed(m_tool_before_pasting);
    }
}

void Canvas::print_pasted_image() {
    if (m_is_pasting && m_imagepaster)
    {
        //m_polygon.clear();
        //qDebug() <<"polygon efface";
        m_imagepaster_origin = m_imagepaster->geometry().topLeft();
        m_imagepaster_end = m_imagepaster->geometry().bottomRight();
        QPair<QPoint, QPoint> offsets = m_imagepaster->get_image_offsets();
        QRect rect(m_imagepaster_origin + offsets.first, m_imagepaster_end + offsets.second);
        m_painter->print_pasted_image(m_imagepaster->get_image(), rect);
    }

}


void Canvas::copy_rectangle_selection() {
    if(m_is_area_selected) {
        QImage img = this->m_frame_hidden
                         ->image.copy(QRect(m_rectangle_selector_origin, m_rectangle_selector_end));
        //stores the selected area as an image in the system clipboard
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setImage(img, QClipboard::Clipboard);
    }
}

void Canvas::copy_polygonal_selection() {
    if(m_is_polygon_selected) {
        QRect rect = m_polygon.boundingRect();
        QImage img = this->m_frame_hidden->image.copy(rect);
        //Mask from polygon
        QImage mask(img.size(), QImage::Format_ARGB32);
        mask.fill(Qt::transparent);
        QPainter maskPainter(&mask);
        maskPainter.setPen(Qt::NoPen);
        maskPainter.setBrush(Qt::white);

        maskPainter.drawPolygon(m_polygon.translated(-rect.topLeft()));

        //Draw the mask on the screen
        img.setAlphaChannel(mask);
        maskPainter.end();
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setImage(img, QClipboard::Clipboard);
    }
}

void Canvas::cut_rectangle_selection() {
    copy_rectangle_selection();
    delete_selection();
}

void Canvas::cut_polygonal_selection() {
    copy_polygonal_selection();
    delete_selection();
}

bool Canvas::get_is_rectangle_area_selected()
{
    return m_is_area_selected;
}

bool Canvas::get_is_polygon_area_selected()
{
    return m_is_polygon_selected;
}

void Canvas::normalize_rubberband_position()
{
    if (m_is_area_selected && m_rubberband)
    {
        if (m_rectangle_selector_end.x() < 0)
            m_rectangle_selector_end.setX(0);
        if (m_rectangle_selector_end.y() < 0)
            m_rectangle_selector_end.setY(0);

        if (m_rectangle_selector_end.x() > this->width())
            m_rectangle_selector_end.setX(this->width());
        if (m_rectangle_selector_end.y() > this->height())
            m_rectangle_selector_end.setY(this->height());

        if (m_rectangle_selector_origin.x() < 0)
            m_rectangle_selector_origin.setX(0);
        if (m_rectangle_selector_origin.y() < 0)
            m_rectangle_selector_origin.setY(0);

        if (m_rectangle_selector_origin.x() > this->width())
            m_rectangle_selector_origin.setX(this->width());
        if (m_rectangle_selector_origin.y() > this->height())
            m_rectangle_selector_origin.setY(this->height());
    }
}

void Canvas::set_image(const QImage &img)
{
    m_frame_displayed->image = img.copy();
    m_frame_hidden->image = img.copy();
    resize_current_frame();

    if(!m_is_camera)
    {
        emit update_frame_button(m_frame_hidden);
    }
}

void Canvas::set_camera_image(const QImage &img)
{
    m_frame_camera_displayed->image = img.copy();
    m_frame_camera_hidden->image = img.copy();
    resize_current_frame();

    if(!m_is_camera)
    {
        emit update_frame_button(m_frame_hidden);
    }
}

void Canvas::get_snaped()
{
    m_frame_hidden->image = m_frame_camera_hidden->image.copy();
    //m_frame_hidden->background = m_frame_camera_hidden->background.copy();
    m_frame_displayed->image = m_frame_camera_displayed->image.copy();
    //m_frame_displayed->background = m_frame_camera_displayed->background.copy();
    //put the image in the undo/redo
    m_frame_hidden->undoredo->add(m_frame_hidden->image.copy());
    m_frame_hidden->is_modified_composing_bench = true;
    emit update_frame_button(m_frame_hidden);
}

void Canvas::set_background(const QImage &img)
{
    m_frame_displayed->background = img.copy();
    m_frame_hidden->background = img.copy();
    resize_current_frame();
    update();
}

void Canvas::set_camera_background(const QImage &img)
{
    m_frame_camera_displayed->background = img.copy();
    m_frame_camera_hidden->background = img.copy();
    resize_current_frame();
    update();
}

QImage Canvas::get_image()
{
    return m_frame_hidden->image;
}

lib_frame::Frame *Canvas::get_frame()
{
    return m_frame_hidden;
}

void Canvas::set_frame(lib_frame::Frame *hidden, lib_frame::Frame *displayed)
{
    //check if the frame is different or just a refresh of the current frame
    bool is_different_image = true;
    if(m_frame_hidden == hidden)
        is_different_image = false;

    lib_frame::Frame *to_delete = m_frame_displayed;
    m_frame_displayed = displayed;
    delete to_delete;
    m_frame_hidden = hidden;
    m_painter->set_frame(m_frame_hidden, m_frame_displayed);
    update();
    resize_current_frame();

    // we consider that the selection tool
    // must be deselected when changing frame or animation
    if(is_different_image)
        deselect_selection(true);
    // if the image is different from what's in the undo, then add it
    if(hidden->undoredo->undo_top() != hidden->image) {
        hidden->undoredo->add(hidden->image.copy());
        hidden->is_modified_composing_bench = true;
    }
}

void Canvas::resizeEvent(QResizeEvent *event) {
    // get the size of the screen on which the widget is displayed
    QScreen *screen = this->window()->windowHandle()->screen();
    int initial_height = this->geometry().height();

    // get the aspect ratio of the screen
    double aspect_ratio = (double) screen->size().width() / (double) screen->size().height();

    // adjust canva size to match aspect ratio
    int width = this->geometry().width();
    int height = width / aspect_ratio;

    // adjust size to not overlap other widgets
    while(height > initial_height) {
        width = width - aspect_ratio;
        height = width / aspect_ratio;
    }
    // resize both canva and current frame
    this->resize(width, height);
    resize_current_frame();
    Canvas::s_canvas_size = size();
}

void Canvas::resize_current_frame() {
    m_frame_displayed->resize(size());
    m_frame_hidden->resize(size());
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    // Camera active, block drawing and right click
    if (m_is_camera)
        return;

    // lock frame access while writing on it
    m_mutexImg.lock();
    if (!m_is_tablet) {
        bool left_click = (event->button() == Qt::LeftButton);
        bool right_click = (event->button() == Qt::RightButton);
        if (right_click) {
            m_mutexImg.unlock();
            QMenu menu(this);
            QAction *action_skinning_off = nullptr;
            QAction *action_skinning_on = nullptr;
            if (m_onion_skinning_on) {
                action_skinning_off = menu.addAction("Turn off onion skinning");
            } else {
                action_skinning_on = menu.addAction("Turn on onion skinning");
            }
            QAction *selectedAction = menu.exec(event->globalPosition().toPoint());

            if (selectedAction == action_skinning_off) {
                m_onion_skinning_on = false;
                update_canvas();
            } else if (selectedAction == action_skinning_on) {
                m_onion_skinning_on = true;
                update_canvas();
            }
            m_mutexImg.lock();
        }
        else if (left_click) {

            QPoint point = event->position().toPoint();
            //m_is_area_selected = false;
            if (m_rubberband && !m_rubberband->isHidden() && m_rubberband->geometry().contains(event->position().toPoint())) {
                m_is_area_selected = true;
                emit update_frame_button(m_frame_hidden);
            }

            switch (m_tool) {
            case lib_drawing_tools::BRUSH: {
                m_last_point_hidden = event->position().toPoint();
                m_last_point_displayed = event->position().toPoint();
                m_is_drawing = true;

                break;
            }
            case lib_drawing_tools::ERASER: {
                m_last_point_hidden = event->position().toPoint();
                m_last_point_displayed = event->position().toPoint();
                m_is_drawing = true;

                break;
            }
            case lib_drawing_tools::COLORPICKER: {
                size_t x = point.x();
                size_t y = point.y();
                color_changed(m_frame_hidden->image.pixel(x, y));
                emit update_color_select(*m_painter->pen_color());
                break;
            }
            case lib_drawing_tools::FILLBUCKET: {
                m_is_drawing = true;
                // m_painter_displayed->fill_shape(event->position().toPoint() , m_frame_displayed->image.pixel(event->position().toPoint()), *m_painter_displayed->pen_color() );
                if(m_is_area_selected) {
                    if(m_rubberband->geometry().contains(event->position().toPoint())) {
                        m_painter->fill_shape(event->position().toPoint(),
                                              m_frame_hidden->image.pixel(event->position().toPoint()),
                                              *m_painter->pen_color(),
                                              m_rubberband->geometry());
                    }
                } else if (m_is_polygon_selected) {
                    if(m_polygon.containsPoint(event->position().toPoint(), Qt::OddEvenFill)) {
                        m_painter->fill_shape(event->position().toPoint(),
                                              m_frame_hidden->image.pixel(event->position().toPoint()),
                                              *m_painter->pen_color(),
                                              m_polygon);
                    }
                } else {
                    m_painter->fill_shape(event->position().toPoint(),
                                          m_frame_hidden->image.pixel(event->position().toPoint()),
                                          *m_painter->pen_color());
                }
                break;
            }
            case lib_drawing_tools::RECTANGLE_SELECTOR : {
                if(m_rubberband && !m_rubberband->isHidden() &&  m_rubberband->geometry().contains(event->position().toPoint()))
                {
                    m_rubberband_offset = event->position().toPoint() - m_rubberband->pos();
                    m_is_move_rubberband = true;
                }
                else
                {
                    emit update_frame_button(m_frame_hidden);
                    //m_tool = lib_drawing_tools::RECTANGLE_SELECTOR;
                    if (!m_rubberband)
                        m_rubberband = new QRubberBand(QRubberBand::Rectangle, this);
                    m_rectangle_selector_origin = event->position().toPoint();
                    m_rubberband->setGeometry(QRect(m_rectangle_selector_origin, QSize()));
                    m_rubberband->show();
                    m_is_area_selected = true;
                    m_is_drawing = true;
                }
                break;
            }

            case lib_drawing_tools::POLYGONAL_SELECTOR :
            {
                // unlock canvas access to avoid thread lock from the paint function
                m_mutexImg.unlock();
                deselect_selection();
                // lock canvas access again
                m_mutexImg.lock();
                QPoint mouse_point = event->position().toPoint();
                m_polygon << mouse_point;
                /*qDebug() << "mouse_point = " << mouse_point;
                qDebug() << "m_polygon = " << m_polygon;*/
                m_is_drawing = true;
                this->update();
                break;
            }

            case lib_drawing_tools::HAND : {
                //to move the image being pasted
                if (m_imagepaster && !m_imagepaster->isHidden() && m_imagepaster->geometry().contains(event->position().toPoint())) {
                    m_imagepaster_offset = event->position().toPoint() - m_imagepaster->pos();
                    m_is_move_imagepaster = true;
                }
                //paste the image if the user clicks outside the imagepaster
                else if (m_imagepaster && !m_imagepaster->isHidden() && !m_imagepaster->geometry().contains(event->position().toPoint())){
                    deselect_selection();
                }
            }
            default: {
                break;
            }
            }
        }
        else {


        }

        //draw_pointTo(event->position().toPoint());
        m_last_point_hidden = event->position().toPoint();
        m_last_point_displayed = event->position().toPoint();
        m_is_drawing = true;
    } else {
        m_is_tablet = false;
    }

    // unlock frame access
    m_mutexImg.unlock();
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    if (m_is_tablet) {
        return;
    }
    switch (m_tool) {
    case lib_drawing_tools::BRUSH:
        m_painter->set_cursor_color(m_frame_displayed, event->position().toPoint(), m_brush);
        m_cursor.set_cursor(QCursor(QPixmap::fromImage(m_brush.brush_border())));
        qApp->setOverrideCursor(m_cursor.get_cursor());

        if ((event->buttons() & Qt::LeftButton) && m_is_drawing) {
            if(m_is_area_selected) {
                m_painter->draw_line_to_selection(event->position().toPoint(), m_brush, m_last_point_displayed, m_rubberband->geometry());
            } else if (m_is_polygon_selected) {
                m_painter->draw_line_to_selection(event->position().toPoint(), m_brush, m_last_point_displayed, m_polygon);
            }
            else {
                m_painter->draw_line_to(event->position().toPoint(), m_brush, m_last_point_displayed);
            }
            m_frame_hidden->is_modified_composing_bench = true;
        }
        break;
    case lib_drawing_tools::ERASER:
        if ((event->buttons() & Qt::LeftButton) && m_is_drawing) {
            if(m_is_area_selected) {
                m_painter->erase_foreground_selection(event->position().toPoint(), m_last_point_displayed, m_rubberband->geometry());
            } else if (m_is_polygon_selected) {
                m_painter->erase_foreground_selection(event->position().toPoint(), m_last_point_displayed, m_polygon);
            } else {
                m_painter->erase_foreground(event->position().toPoint(), m_last_point_displayed);
            }
            m_frame_hidden->is_modified_composing_bench = true;
            emit update_frame_button(m_frame_hidden, true);
        }
        break;
    case lib_drawing_tools::RECTANGLE_SELECTOR:
        if(event->buttons() & Qt::LeftButton) {
            if(m_is_move_rubberband) {
                m_rubberband->move(event->position().toPoint() - m_rubberband_offset);
            } else {
                m_rubberband->setGeometry(QRect(m_rectangle_selector_origin, event->position().toPoint()).normalized());
            }
        }
        break;
    case lib_drawing_tools::POLYGONAL_SELECTOR: {
        if(!m_polygon.isEmpty() && !m_is_polygon_selected) {
            QPoint mouse_point = event->position().toPoint();
            m_polygon << mouse_point;
            this->update();
        }
        break;
    }
    case lib_drawing_tools::HAND:
        if(event->buttons() & Qt::LeftButton) {
            if(m_is_move_imagepaster) {
                m_imagepaster->move(event->position().toPoint() - m_imagepaster_offset);
            }
        }
        break;
    default:
        break;
    }


}

void Canvas::mouseReleaseEvent(QMouseEvent *event)
{
    // qDebug() << "mouse release";

    switch (m_tool) {
    case lib_drawing_tools::BRUSH:
        if (event->button() == Qt::LeftButton && m_is_drawing) {
            if(m_is_area_selected) {
                if(m_rubberband->geometry().contains(event->position().toPoint())) {
                    m_painter->draw_point_to_selection(event->position().toPoint(), m_brush, m_rubberband->geometry());
                }
            } else if (m_is_polygon_selected) {
                if(m_polygon.containsPoint(event->position().toPoint(), Qt::OddEvenFill)) {
                    m_painter->draw_point_to_selection(event->position().toPoint(), m_brush, m_polygon);
                }
            } else {
                m_painter->draw_point_to(event->position().toPoint(), m_brush);
            }
            m_frame_hidden->undoredo->add(m_frame_hidden->image.copy());
            m_frame_hidden->is_modified_composing_bench = true;
            m_is_drawing = false;
        }
        emit update_frame_button(m_frame_hidden, true);
        break;

    case lib_drawing_tools::ERASER:
        if (event->button() == Qt::LeftButton && m_is_drawing) {
            if(m_is_area_selected) {
                if(m_rubberband->geometry().contains(event->position().toPoint())) {
                    m_painter->erase_foreground_selection(event->position().toPoint(), m_last_point_displayed, m_rubberband->geometry());
                }
            } else if (m_is_polygon_selected) {
                if(m_polygon.containsPoint(event->position().toPoint(), Qt::OddEvenFill)) {
                    m_painter->erase_foreground_selection(event->position().toPoint(), m_last_point_displayed, m_polygon);
                }
            } else {
                m_painter->erase_foreground(event->position().toPoint(), m_last_point_displayed);
            }
            m_frame_hidden->undoredo->add(m_frame_hidden->image.copy());
            m_frame_hidden->is_modified_composing_bench = true;
            m_is_drawing = false;
            emit update_frame_button(m_frame_hidden, true);
        }
        break;

    case lib_drawing_tools::FILLBUCKET:
        if (event->button() == Qt::LeftButton && m_is_drawing) {
            m_is_drawing = false;
            m_frame_hidden->undoredo->add(m_frame_hidden->image.copy());
            m_frame_hidden->is_modified_composing_bench = true;
            //m_frame_displayed->undoredo->add(last_image_displayed);
        }
        emit update_frame_button(m_frame_hidden, true);
        break;
    case lib_drawing_tools::RECTANGLE_SELECTOR:
        if(m_rubberband != nullptr) {
            //m_rubberband->hide(); //use this to hide the selection rectangle
            //to select the area from the rectangle from the frame
            m_rectangle_selector_end = m_rubberband->geometry().bottomRight();
            m_rectangle_selector_origin = m_rubberband->geometry().topLeft();

            //normalization of the rectangle coordinates so they fit to the canvas.
            normalize_rubberband_position();
            //m_is_area_selected = true;
            m_is_drawing = false;
            m_is_move_rubberband = false;

            //consider the area deselected if the user clicks on the frame without selecting anything
            if(m_rubberband->size().width() <= 1 && m_rubberband->size().height() <= 1)
                m_is_area_selected = false;
        }
        break;

    case lib_drawing_tools::POLYGONAL_SELECTOR: {
        m_is_polygon_selected = true;
        m_is_drawing = false;

        if(m_polygon.size() < 2) {
            deselect_selection();
        }
        break;
    }
    case lib_drawing_tools::HAND:
        if (m_imagepaster) {
            m_imagepaster_origin = m_imagepaster->geometry().topLeft();
            m_imagepaster_end = m_imagepaster->geometry().bottomRight();
            m_is_drawing = false;
            m_is_move_imagepaster = false;
        }
        break;
    default:
        break;
    }

}

void Canvas::paintEvent(QPaintEvent *event)
{
    // lock the mutex while drawing so that NDImanager
    // doesn't work on the frame at the same time
    m_mutexImg.lock();

    QPainter painter(this);
    QRect rect (QPoint(0,0), QPoint(this->width(), this->height()));

    if (m_is_camera) {
        QImage &imageToDraw = (m_onion_skinning_on) ? m_frame_camera_displayed->image
                                                    : m_frame_camera_hidden->image;
        painter.drawImage(rect, imageToDraw);
    } else {
        QImage &imageToDraw = (m_onion_skinning_on) ? m_frame_displayed->image
                                                    : m_frame_hidden->image;
        painter.drawImage(rect, imageToDraw);
    }

    if(m_is_camera) {
        qreal adjust = 1.0;
        QRectF getRekt(rect.topLeft().rx() - adjust / 2.0,
                       rect.topLeft().ry() - adjust / 2.0,
                       rect.width(),
                       rect.height());
        painter.setPen(QPen(Qt::red, 6, Qt::SolidLine));
        painter.drawRect(getRekt);
    }

    int size =  m_polygon.size();
    if (size >= 2)
    {
        QColor fill_color = QColor(191, 215, 231);
        QColor border_color = QColor(119, 168, 139);

        fill_color.setAlpha(120);

        painter.setPen(QPen(border_color, 1));

        QPainterPath path;
        path.addPolygon(m_polygon);
        painter.fillPath(path, fill_color);
        painter.drawPolygon(m_polygon);
    }

    // unlock the mutex after drawing on the frame
    // NDImanager is now allowed to work on the frame
    m_mutexImg.unlock();
}

void Canvas::enterEvent(QEnterEvent *event)
{
    if (m_tool == lib_drawing_tools::HAND)
        qApp->setOverrideCursor(Qt::ArrowCursor);
    else
        qApp->setOverrideCursor(m_cursor.get_cursor());
    event->ignore();
}

void Canvas::leaveEvent(QEvent *event)
{
    while(qApp->overrideCursor())
        qApp->restoreOverrideCursor();
    event->ignore();
}

void Canvas::tabletEvent(QTabletEvent *event)
{
    switch (event->type()) {
    case QEvent::TabletPress: {
        m_is_tablet = true;
        QPoint point = event->position().toPoint();
        //m_is_area_selected = false;
        if (m_rubberband && !m_rubberband->isHidden() && m_rubberband->geometry().contains(event->position().toPoint())) {
            m_is_area_selected = true;
            emit update_frame_button(m_frame_hidden);
        }

        switch (m_tool) {
        case lib_drawing_tools::ERASER: {
            m_is_drawing = true;
            m_frame_hidden->is_modified_composing_bench = true;

            break;
        }
        case lib_drawing_tools::BRUSH: {
            m_is_drawing = true;
            m_frame_hidden->is_modified_composing_bench = true;

            break;
        }

        case lib_drawing_tools::COLORPICKER: {
            m_is_drawing = true;
            size_t x = event->position().x();
            size_t y = event->position().y();
            if (m_onion_skinning_on) {
                color_changed(m_frame_displayed->image.pixel(x, y));
            } else {
                color_changed(m_frame_hidden->image.pixel(x, y));
            }
            emit update_color_select(*m_painter->pen_color());
            break;
        }
        case lib_drawing_tools::FILLBUCKET: {
            m_is_drawing = true;
            if(m_is_area_selected) {
                if(m_rubberband->geometry().contains(event->position().toPoint())) {
                    m_painter->fill_shape(event->position().toPoint(),
                                          m_frame_hidden->image.pixel(event->position().toPoint()),
                                          *m_painter->pen_color(),
                                          m_rubberband->geometry());
                }
            } else if (m_is_polygon_selected) {
                if(m_polygon.containsPoint(event->position().toPoint(), Qt::OddEvenFill)) {
                    m_painter->fill_shape(event->position().toPoint(),
                                          m_frame_hidden->image.pixel(event->position().toPoint()),
                                          *m_painter->pen_color(),
                                          m_polygon);
                }
            } else {
                m_painter->fill_shape(event->position().toPoint(),
                                      m_frame_hidden->image.pixel(event->position().toPoint()),
                                          *m_painter->pen_color());
            }
            m_frame_hidden->is_modified_composing_bench = true;
            break;
        }
        case lib_drawing_tools::RECTANGLE_SELECTOR : {
            if(m_rubberband && !m_rubberband->isHidden() &&  m_rubberband->geometry().contains(event->position().toPoint()))
            {
                m_rubberband_offset = event->position().toPoint() - m_rubberband->pos();
                m_is_move_rubberband = true;
            }
            else
            {
                emit update_frame_button(m_frame_hidden);
                //m_tool = lib_drawing_tools::RECTANGLE_SELECTOR;
                if (!m_rubberband)
                    m_rubberband = new QRubberBand(QRubberBand::Rectangle, this);
                m_rectangle_selector_origin = event->position().toPoint();
                m_rubberband->setGeometry(QRect(m_rectangle_selector_origin, QSize()));
                m_rubberband->show();
                m_is_area_selected = true;
                m_is_drawing = true;
            }
            break;
        }

        case lib_drawing_tools::POLYGONAL_SELECTOR :
        {
            if(m_is_polygon_selected) {
                deselect_selection();
                break;
            }
            QPoint mouse_point = event->position().toPoint();
            m_polygon << mouse_point;
            /*qDebug() << "mouse_point = " << mouse_point;
                qDebug() << "m_polygon = " << m_polygon;*/
            m_is_drawing = true;
            this->update();
        }

        case lib_drawing_tools::HAND : {
            //to move the image being pasted
            if (m_imagepaster && !m_imagepaster->isHidden() && m_imagepaster->geometry().contains(event->position().toPoint())) {
                m_imagepaster_offset = event->position().toPoint() - m_imagepaster->pos();
                m_is_move_imagepaster = true;
            }
            //paste the image if the user clicks outside the imagepaster
            else if (m_imagepaster && !m_imagepaster->isHidden() && !m_imagepaster->geometry().contains(event->position().toPoint())){
                deselect_selection();
            }
        }
        default: {
            break;
        }
        }
        m_deviceDown = true;
        m_last_point_hidden = event->position().toPoint();
        m_last_point_displayed = event->position().toPoint();

        emit update_frame_button(m_frame_hidden);

        break;
    }
    case QEvent::TabletMove:
        //qDebug() << "tablet move";
        if (event->position().toPoint().x() > 5 && event->position().toPoint().y() > 5 && event->position().toPoint().y() < this->height() - 5
            && event->position().toPoint().x() < this->width() - 5) {
            qApp->setOverrideCursor(m_cursor.get_cursor());
        } else {
            qApp->setOverrideCursor(Qt::ArrowCursor);
        }
        if (m_deviceDown) {
            switch (m_tool) {
            case lib_drawing_tools::ERASER:
                if(m_is_area_selected) {
                    m_painter->erase_foreground_selection(event->position().toPoint(), m_last_point_displayed, m_rubberband->geometry());
                } else if (m_is_polygon_selected){
                    m_painter->erase_foreground_selection(event->position().toPoint(), m_last_point_displayed, m_polygon);
                } else {
                    m_painter->erase_foreground(event->position().toPoint(), m_last_point_displayed);
                }
                break;
            case lib_drawing_tools::BRUSH:
                if(m_is_area_selected) {
                    m_painter->tablet_draw_line_to_selection(event, m_brush, m_last_point_displayed, m_rubberband->geometry());
                } else if (m_is_polygon_selected) {
                    m_painter->tablet_draw_line_to_selection(event, m_brush, m_last_point_displayed, m_polygon);
                }
                m_painter->tablet_draw_line_to(event, m_brush, m_last_point_displayed);
                break;
            case lib_drawing_tools::RECTANGLE_SELECTOR:
                if(m_is_move_rubberband) {
                    m_rubberband->move(event->position().toPoint() - m_rubberband_offset);
                } else {
                    m_rubberband->setGeometry(QRect(m_rectangle_selector_origin, event->position().toPoint()).normalized());
                }
                break;
            case lib_drawing_tools::POLYGONAL_SELECTOR: {
                QPoint mouse_point = event->position().toPoint();
                m_polygon << mouse_point;
                m_is_drawing = true;
                this->update();
                break;
            }
            case lib_drawing_tools::HAND:
                if(m_is_move_imagepaster) {
                    m_imagepaster->move(event->position().toPoint() - m_imagepaster_offset);
                }
                break;
            default:
                break;
            }
        }

        break;
    case QEvent::TabletRelease:
        if(m_deviceDown) {
            switch(m_tool) {
            case lib_drawing_tools::RECTANGLE_SELECTOR:
                if(m_rubberband != nullptr) {
                    //m_rubberband->hide(); //use this to hide the selection rectangle
                    //to select the area from the rectangle from the frame
                    m_rectangle_selector_end = m_rubberband->geometry().bottomRight();
                    m_rectangle_selector_origin = m_rubberband->geometry().topLeft();

                    //normalization of the rectangle coordinates so they fit to the canvas.
                    normalize_rubberband_position();
                    //m_is_area_selected = true;
                    m_is_drawing = false;
                    m_is_move_rubberband = false;

                    //consider the area deselected if the user clicks on the frame without selecting anything
                    if(m_rubberband->size().width() <= 1 && m_rubberband->size().height() <= 1)
                        m_is_area_selected = false;
                }
                break;

            case lib_drawing_tools::POLYGONAL_SELECTOR: {
                // we consider that very small sized polygons were selected by error
                // for example while clicking somewhere else to deselect a selection
                // removing this code makes deselecting by clicking much harder

                m_is_polygon_selected = true;
                m_is_drawing = false;
                double poly_area = polygon_area(m_polygon);
                if(poly_area < K_MINIMUM_POLYGON_SIZE) {
                    deselect_selection();
                }
                break;
            }
            case lib_drawing_tools::HAND:
                if (m_imagepaster) {
                    m_imagepaster_origin = m_imagepaster->geometry().topLeft();
                    m_imagepaster_end = m_imagepaster->geometry().bottomRight();
                    m_is_drawing = false;
                    m_is_move_imagepaster = false;
                }
                break;
            default:
                break;
            }
        }
        update();
        emit update_frame_button(m_frame_hidden, true);

        if (m_deviceDown && event->buttons() == Qt::NoButton) {
            m_last_point_hidden = event->position().toPoint();
            m_last_point_displayed = event->position().toPoint();
            m_deviceDown = false;
            m_is_drawing = false;
            m_frame_hidden->undoredo->add(m_frame_hidden->image.copy());
            m_frame_displayed->undoredo->add(m_frame_displayed->image.copy());
            //            emit update_frame_button(m_frame_hidden);
        }
    default:
        break;
    }

    update_cursor();
    event->accept();
}

void Canvas::clear_image()
{
    m_frame_hidden->image.fill(Qt::white);
    m_frame_hidden->background.fill(Qt::white);
    m_frame_displayed->image.fill(Qt::white);
    m_frame_displayed->background.fill(Qt::white);
    m_frame_hidden->undoredo->add(m_frame_hidden->image.copy());
    m_frame_displayed->undoredo->add(m_frame_displayed->image.copy());
    update();
    emit update_frame_button(m_frame_hidden);
}

void Canvas::color_changed(const QColor &newColor)
{
    m_painter->set_pen_color(newColor);
}

void Canvas::thickness_change(int newWidth)
{
    m_painter->set_pen_width(newWidth);
    m_cursor.update_thickness(newWidth);

    switch (m_tool) {
    case lib_drawing_tools::BRUSH:
        m_painter->set_pen_width(newWidth);
        m_brush.change_width(*m_painter->pen_width());
        m_cursor.set_cursor(QCursor(QPixmap::fromImage(m_brush.brush_border())));
        break;

    default:
        break;
    }
}

void Canvas::brush_changed(const lib_drawing_tools::Brush &brush)
{
    m_tool = lib_drawing_tools::BRUSH;
    m_brush = brush;
    update_cursor();
}

void Canvas::tool_changed(const lib_drawing_tools::DrawingTool &tool)
{
    m_tool = tool;
    update_cursor();
    if(tool == lib_drawing_tools::RECTANGLE_SELECTOR
    || tool == lib_drawing_tools::POLYGONAL_SELECTOR) {
        deselect_selection();
        m_polygon.clear();
    }
}

void Canvas::on_undo()
{
    m_mutexImg.lock();
    if (!m_is_drawing) {
        m_frame_displayed->image = m_frame_displayed->undoredo->undo();
        m_frame_hidden->image = m_frame_hidden->undoredo->undo();
        //resize_image(&m_frame->image, m_frame->image.size().expandedTo(size()));

        update();
    }
    emit update_frame_button(m_frame_hidden, true);
    m_mutexImg.unlock();
}

void Canvas::on_redo()
{
    m_mutexImg.lock();
    if (!m_is_drawing) {
        m_frame_displayed->image = m_frame_displayed->undoredo->redo();
        m_frame_hidden->image = m_frame_hidden->undoredo->redo();
        //resize_image(&m_frame->image, m_frame->image.size().expandedTo(size()));
        update();
    }
    emit update_frame_button(m_frame_hidden, true);
    m_mutexImg.unlock();
}

void Canvas::update_canvas()
{
    update();
}

void Canvas::update_cursor()
{
    QPoint pos_cursor(mapFromGlobal(QCursor::pos()));
    if (m_tool == lib_drawing_tools::BRUSH) {
        if (pos_cursor.x() > 5 && pos_cursor.y() > 5 && pos_cursor.y() < this->height() - 5
            && pos_cursor.x() < this->width() - 5) {
            m_brush.extract_border(m_frame_displayed->image.pixelColor(pos_cursor));
        }
        m_cursor.set_cursor(QCursor(QPixmap::fromImage(m_brush.brush_border())));
    } else if (m_tool == lib_drawing_tools::HAND) {
        qApp->setOverrideCursor(Qt::ArrowCursor);
    } else {
        m_cursor.update_cursor(m_tool);
    }
}

void Canvas::set_camera_state(bool b)
{
    //clear camera frame
    m_frame_camera_hidden->image.fill(Qt::white);
    m_frame_camera_hidden->background.fill(Qt::white);

    m_frame_camera_displayed->image.fill(Qt::white);
    m_frame_camera_displayed->background.fill(Qt::white);

    this->m_is_camera = b;
    update();
    emit update_frame_button(m_frame_hidden, true);
}

void Canvas::onion_skinning_state(bool b)
{
    m_onion_skinning_on = b;
}

} // namespace lib_canvas
