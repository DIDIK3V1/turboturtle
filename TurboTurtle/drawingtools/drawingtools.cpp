/**

 @author Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Creerio, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "drawingtools.h"
#include "ui_drawingtools.h"
#include <QDebug>
#include "lib_dialog/userbrushdialog.h"

namespace lib_drawing_tools {

    DrawingTools::DrawingTools(QWidget *parent)
        : QGroupBox(parent)
        , m_ui(new Ui::DrawingTools)
        , m_pencil_brush(lib_drawing_tools::Brush(QImage(":/brushes/pencil"), 10))
        , m_paint_brush(lib_drawing_tools::Brush(QImage(":/brushes/realBrush7"), 5))
        , m_marker_brush(lib_drawing_tools::Brush(QImage(":/brushes/realBrush5"), 1))
        , m_user_brush(lib_drawing_tools::Brush(QImage(":/brushes/airBrush"), 1))
        , m_selected_button(nullptr)
    {
        m_ui->setupUi(this);
        m_slider_thickness = m_ui->hSlideThickness;
        m_spin_box_thickness = m_ui->spinBoxThickness;
        // Set slider range
        m_slider_thickness->setMinimum(1);
        m_slider_thickness->setMaximum(100);

        // Connect spinbox signal to update slider
        connect(m_spin_box_thickness,
                QOverload<int>::of(&QSpinBox::valueChanged),
                this, &DrawingTools::updateSliderFromSpinBox);

        // Connect slider signal to update spinbox
        connect(m_slider_thickness, &QSlider::valueChanged,
                this, &DrawingTools::updateSpinBoxFromSlider);

        //stop selection
        connect(m_ui->butEraser, SIGNAL(clicked()), this, SLOT(button_Clicked()));
        connect(m_ui->butPencil, SIGNAL(clicked()), this, SLOT(button_Clicked()));
        connect(m_ui->butBucket, SIGNAL(clicked()), this, SLOT(button_Clicked()));
        connect(m_ui->butColorPicker, SIGNAL(clicked()), this, SLOT(button_Clicked()));
        connect(m_ui->butPaintbrush, SIGNAL(clicked()), this, SLOT(button_Clicked()));
        connect(m_ui->butMarker, SIGNAL(clicked()), this, SLOT(button_Clicked()));
        connect(m_ui->butUserbrush, SIGNAL(clicked()), this, SLOT(button_Clicked()));
        connect(m_ui->butPolyShape, SIGNAL(clicked()), this, SLOT(button_Clicked()));
        connect(m_ui->butRectShape, SIGNAL(clicked()), this, SLOT(button_Clicked()));

        //Get the right color for the border
        QColor tmp_color = this->palette().color(QPalette::Highlight);
        std::stringstream ss;
        ss << "border: 1px solid rgb(" << tmp_color.red() << "," << tmp_color.green() << ","
           << tmp_color.blue() << ");";

        m_ui->butPaintbrush->setStyleSheet(QString::fromStdString(ss.str()));

        m_spin_box_thickness->setValue(20);

    }

    DrawingTools::~DrawingTools()
    {
        qDebug() << "trying to delete DrawingTools";
        delete m_ui;
        qDebug() << "DrawingTools deleted correctly";
    }

    void DrawingTools::updateSliderFromSpinBox(const size_t value)
    {
        m_slider_thickness->setValue(value);
        emit thicknessChange(value);
    }

    void DrawingTools::updateSpinBoxFromSlider(const size_t value)
    {
        m_spin_box_thickness->setValue(value);
        emit thicknessChange(value);
    }

    void DrawingTools::on_butPaintbrush_clicked()
    {
        emit brushChange(m_paint_brush);
    }

    void DrawingTools::on_butMarker_clicked()
    {
        emit brushChange(m_marker_brush);
    }

    void DrawingTools::on_butPencil_clicked()
    {
        emit brushChange(m_pencil_brush);
    }

    void DrawingTools::on_butEraser_clicked()
    {
        emit toolChange(lib_drawing_tools::ERASER);
    }

    void DrawingTools::on_butColorPicker_clicked()
    {
        emit toolChange(lib_drawing_tools::COLORPICKER);
    }

    void DrawingTools::on_butBucket_clicked()
    {
        emit toolChange(lib_drawing_tools::FILLBUCKET);
    }

    void DrawingTools::on_butUndo_clicked()
    {
        emit undo();
    }

    void DrawingTools::on_butRedo_clicked()
    {
        emit redo();
    }

    void DrawingTools::increase_thickness(unsigned inc)
    {
        m_spin_box_thickness->setValue(m_spin_box_thickness->value() + inc);
        emit thicknessChange(m_spin_box_thickness->value());
    }

    void DrawingTools::decrease_thickness(unsigned dec)
    {
        m_spin_box_thickness->setValue(m_spin_box_thickness->value() - dec);
        emit thicknessChange(m_spin_box_thickness->value());
    }

    std::stringstream DrawingTools::get_clicked_stylesheet()
    {
        QColor tmp_color = this->palette().color(QPalette::Highlight);
        std::stringstream ss;
        ss << "border: 1px solid rgb(" << tmp_color.red() << "," << tmp_color.green() << ","
           << tmp_color.blue() << ");";

        return ss;
    }

    void DrawingTools::button_Clicked()
    {
        //for the start of the sofware
        if (m_selected_button == nullptr)
        {
            m_ui->butPaintbrush->setStyleSheet("");
        }

        QPushButton *clicked_button = qobject_cast<QPushButton*>(sender());

        if (m_selected_button != nullptr)
        {
           m_selected_button->setStyleSheet(""); // reset styleSheet
        }

        if (m_selected_button == m_ui->butUserbrush)
        {
            m_selected_button->setText("Other");
            m_selected_button->setIcon(QIcon());
        }

        //Get the right color for the border
        std::stringstream ss = get_clicked_stylesheet();

        // set border
        clicked_button->setStyleSheet(QString::fromStdString(ss.str()));

        //set "already" selected button
        m_selected_button = clicked_button;
    }


    void DrawingTools::on_butUserbrush_clicked()
    {
        userBrushDialog brushDialog;

        connect(&brushDialog, SIGNAL(user_brush_selected(QString)), this, SLOT(selectUserBrush(QString)));

        brushDialog.exec();
    }

    void DrawingTools::selectUserBrush(QString p)
    {
        m_user_brush = lib_drawing_tools::Brush(QImage(p), 5);
        m_ui->butUserbrush->setIcon(QIcon(p));
        m_ui->butUserbrush->setText("");
        emit brushChange(m_user_brush);
    }

} // namespace lib_drawing_tools
