/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Katarina

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef THICKNESSSLIDER_H
#define THICKNESSSLIDER_H

#include <QObject>
#include <QSlider>
#include "qevent.h"

namespace lib_drawing_tools {

/**
* @brief A slider for selecting the thickness of a drawing tool.
* This class inherits from QSlider and overrides its mousePressEvent() method to allow for custom behavior.
*/
class ThicknessSlider : public QSlider
{
    Q_OBJECT
public:
    /**
    * @brief Constructs a ThicknessSlider object.
    * @param parent Parent widget of the slider.
    */
    explicit ThicknessSlider(QWidget *parent = 0);

    /**
    *@brief Constructs a ThicknessSlider object with a specified orientation.
    *@param orientation Orientation of the slider (Qt::Orientation type).
    *@param parent Parent widget of the slider.
    */
    explicit ThicknessSlider(const Qt::Orientation &orientation, QWidget *parent = 0);

protected:
    /**
    *@brief Handles the mouse press event.
    *This method is called when the user presses the mouse button over the slider. It calculates the position
    *of the mouse relative to the slider's range and sets the value of the slider accordingly.
    *@param event The mouse press event.
    */
    void mousePressEvent(QMouseEvent *event) override;
};
} // namespace lib_drawing_tools
#endif // THICKNESSSLIDER_H
