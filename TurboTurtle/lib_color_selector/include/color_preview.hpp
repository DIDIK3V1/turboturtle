/**

@author Mattia Basaglia

@section License

    Copyright (C) 2013-2014 Mattia Basaglia
    Copyright (C) 2014 Calle Laakkonen

    This software is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This software is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Color Widgets.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef COLOR_PREVIEW_HPP
#define COLOR_PREVIEW_HPP


#include <QWidget>

namespace lib_color_selector{

/**
    @brief Simple widget that shows a preview of a color
*/
class ColorPreview : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QColor color READ color WRITE set_color NOTIFY color_changed DESIGNABLE true)
    Q_PROPERTY(QColor comparison_color READ comparison_color WRITE set_comparison_color DESIGNABLE true)
    Q_PROPERTY(DisplayMode display_mode READ display_mode WRITE set_display_mode DESIGNABLE true)
    Q_PROPERTY(QBrush background READ background WRITE set_background DESIGNABLE true)
    Q_ENUMS(DisplayMode)

public:
    /**
     * @brief The DisplayMode enum
     */
    enum DisplayMode
    {
        NoAlpha,    ///< Show current color with no transparency
        AllAlpha,   ///< show current color with transparency
        SplitAlpha, ///< Show both solid and transparent side by side
        SplitColor  ///< Show current and comparison colors side by side
    };

    explicit ColorPreview(QWidget *parent = 0);
    ~ColorPreview();


    /**
     * @brief background : Get the background visible under transparent colors
     * @return
     */
    QBrush background() const;

    /**
     * @brief set_background : Change the background visible under transparent colors
     * @param bk
     */
    void set_background(const QBrush &bk);

    /**
     * @brief display_mode : Get color display mode
     * @return
     */
    DisplayMode display_mode() const;

    /**
     * @brief set_display_mode : Set how transparent colors are handled
     * @param dm
     */
    void set_display_mode(const DisplayMode &dm);

    /**
     * @brief color : Get current color
     * @return
     */
    QColor color() const;


    /**
     * @brief comparison_color :Get the comparison color
     * @return
     */
    QColor comparison_color() const;

    /**
     * @brief size_hint
     * @return
     */
    QSize size_hint () const;

    /**
     * @brief paint
     * @param painter
     * @param rect
     */
    void paint(QPainter &painter, QRect rect) const;

public slots:
    /**
     * @brief set_color : Set current color
     * @param c
     */
    void set_color(const QColor &c);

    ///
    /**
     * @brief set_comparison_color : Set the comparison color
     * @param c
     */
    void set_comparison_color(const QColor &c);


signals:
    /**
     * @brief clicked : Emitted when the user clicks on the widget
     */
    void clicked();

    /**
     * @brief color_changed : Emitted on setColor
     */
    void color_changed(QColor);

protected:
    /**
     * @brief paintEvent
     */
    void paintEvent(QPaintEvent *);
    /**
     * @brief resizeEvent
     */
    void resizeEvent(QResizeEvent *);
    /**
     * @brief mouseReleaseEvent
     * @param ev
     */
    void mouseReleaseEvent(QMouseEvent *ev);
    /**
     * @brief mouseMoveEvent
     * @param ev
     */
    void mouseMoveEvent(QMouseEvent *ev);

private:
    class Private;
    Private *const m_p;
};


}
#endif // COLOR_PREVIEW_HPP
