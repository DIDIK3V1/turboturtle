/**
 @author Creerio

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "framebutton.h"
#include "lib_timeline/widgets/timelinelayout.h"
#include "framemenu.h"

namespace lib_timeline {

    FrameButton::FrameButton(lib_frame::Frame *item, const QString &mime_type, const int pos, QWidget * parent) :
    TemplatedImageButton(item, mime_type, pos, parent),
    layout(std::bit_cast<TimelineLayout *>(parent))
    {
        QObject::connect(this,
                        &FrameButton::on_right_click,
                        this,
                        [this] (const QPointF mouse_pos, [[maybe_unused]] const int elem_pos) {
                            FrameMenu menu(this);
                            layout->clear_selection();
                            layout->select(this->get_pos());

                            // Load menu, action is covered by menu
                            menu.exec(mouse_pos.toPoint());
                        });
    }

}// namespace lib_timeline
