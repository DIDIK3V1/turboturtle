/**
 @author Creerio

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef HLAYOUT_H
#define HLAYOUT_H

#include <QWidget>
#include <mutex>

namespace Ui { class widHLayout; }
namespace lib_widgets {

    /**
     * @brief The HLayout class
     * UI element containing objects in a horizontal QScrollArea
     */
    class HLayout : public QWidget
    {
        Q_OBJECT

    public:
        /**
         * @brief Default constructor of a HLayout object
         * @param parent Parent QWidget, usually the main window
         */
        explicit HLayout(QWidget *parent = nullptr);
        ~HLayout();

        /**
         * @brief Gives pointer to currently loaded item
         * @return Loaded item
         */
        const QWidget* get_loaded();

        /**
         * @brief finished_loading Call this function to indicate that the QWidget has been loaded. If no security is needed, call this when receiving a signal
         * @param item Widget loaded
         */
        void finished_loading(QWidget *item);

    protected:
        /**
         * @brief User interface, from the corresponding .ui file
         */
        Ui::widHLayout *m_ui;

        /**
         * @brief Loaded item
         */
        QWidget *loaded_item = nullptr;

        /**
         * @brief update_min_height Used to update the minimum size on the scroll area for the items
         * @param height Height to use
         */
        void update_min_height(const size_t &height);

    private:
        /**
         * @brief loading_item Boolean indicating if a QWidget is currently loading on some other widget
         */
        bool loading_item = false;

        /**
         * @brief item_loading_block Mutex used to lock access to the read/write of the loading_item boolean
         */
        std::mutex item_loading_block;

    signals:
        /**
         * @brief load_item Gives the QWidget to load. DO NOT LINK THIS SIGNAL TO AVOID ISSUES, link with start_load_item.
         * @param item QWidget to load
         */
        void load_item(QWidget *item);

        /**
         * @brief start_load_item Signal to used when the current QWidget is saved, use this
         * @param item QWidget loaded
         */
        void start_load_item(QWidget *item);

        /**
         * @brief update_counter Signal used to update the counter
         * @param current_loaded number of the currently loaded element
         * @param nb_elements Number of elements
         */
        void update_counter(const size_t &current_loaded, const size_t &nb_elements);

    protected slots:
        /**
         * @brief Signal used to store currently loaded widget
         * @param item QWidget loaded
         */
        virtual void save_loaded_item(QWidget *item);
    };
}

#endif // HLAYOUT_H
