/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Aurélien LAPLAUD, Katarina

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ANIMATION_H
#define ANIMATION_H

#include "lib_canvas/frame.h"
#include <vector>
#include <QStandardPaths>

namespace lib_animation {

/**
 * @brief Class representing a sequence of frames
 * Inherits from std::vector<Frame*>
 * Provides operators to multiply, add or subtract frames, creating a new Animation
 */
class Animation : public std::vector<lib_frame::Frame*>
{
public:
    /**
     * @brief Backup location for every animation
     */
    static const QString BACKUP_LOCATION;

    /**
     * @brief Indicates if one frame of the animation has been modified
     */
    bool is_modified = false;


    Animation();

    /**
     * @brief destructor for the frames
     */
    ~Animation();

    Animation(Animation &anim);

    /**
     * @brief Used to create a new animation
     * @return A pointer to a new animation with default settings
     */
    static Animation* new_animation();

    /**
     * @brief Used to create a new animation
     * @return A pointer to a new animation with custom provided color
     */
    static Animation* new_animation(Qt::GlobalColor color);

    /**
     * @brief copy_memory Used to copy an animation and keep the frames in memory
     * @param anim Animation to copy
     * @return Copy of the animation created
     */
    static Animation* copy_memory(const Animation &anim);

    /**
     * @brief Creates an animation from a gif file
     * @param image_file gif file targeted
     * @return A new animation using the provided gif file. If it doesn't exist, a default animation will be given
     */
    static Animation* import_from_gif(const QString &image_file);

    /**
     * @brief Creates an animation from multiple png files
     * @param import_folder folder targeted for the import
     * @param backup_recover indicates if this import is from a backup recovering or not to avoid sending unnecessary messages to the user
     * @return A new animation using the provided folder. If it doesn't exist or if there were any issues with the import process, nullptr will be returned
     */
    static Animation* import_from_png(const QString &import_folder, const bool &backup_recover = false);

    /**
     * @brief Gives a thumbnail for the current animation
     * @return A QImage for a thumbnail
     */
    QImage get_thumbnail() const;

    /**
     * @brief Gives the generated name of the animation
     * @return A std::string containing the name of the animation
     */
    std::string get_name() const;

    /**
     * @brief is_loaded Indicates if the animation is loaded in memory or not
     * @return True if loaded, false if not
     */
    bool is_loaded() const;

    /**
     * @brief unload Unloads the animation if possible
     * @return True if successful, false if it cannot unload the animation
     */
    bool unload();

    /**
     * @brief set_used_in_timeline Updates the indicator for the frame use in the timeline
     * @param is_used If the animation is used or not
     */
    void set_used_in_timeline(const bool &is_used);

    /**
     * @brief set_used_in_composing_bench Updates the indicator for the frame use in the composing bench
     * @param is_used If the animation is used or not
     */
    void set_used_in_composing_bench(const bool &is_used);

    /**
     * @brief Moves a frame from a start index to an end index.
     * @param start_index Index of the frame to move
     * @param end_index Index to move the frame to
     */
    void move_frame(const size_t &start_index, const size_t &end_index);

    /**
    * @brief save the animation on the computer
    * @param path the path where the animation will be saved
    */
    void save_to_png(const QString &path, const bool &log = true);

    /**
    * @brief save the animation on the computer as a gif file
    * Warning : This saves data using other threads, it isn't required to use one to call this function
    * @param file_name the path where the animation will be saved
    * @param framerate framerate of the gif file
    */
    void save_to_gif(const QString &file_name, const double &framerate = 10.0);

    /**
    * @brief save the animation on the computer as a mp4 file
    * Warning : This saves data using other threads, it isn't required to use one to call this function
    * @param file_name the path where the animation will be saved
    * @param framerate framerate of the mp4 file
    */
    void save_to_mp4(const QString &file_name, const double &framerate = 10.0);

    /**
     * @brief saves the animation in png form inside of the backup directory
     */
    void backup();

    /**
     * @brief saves a single frame as png inside of the backup directory
     */
    void backup_frame(const int &pos);

    /**
     * @brief reload_from_backup Reloads the animation using it's backup path
     * @return true if successful, false if not
     */
    bool reload_from_backup();

private :
    /**
     * @brief ANIM_COUNTER Counter used when creating an animation, allows for ""unique"" animation names
     */
    static size_t ANIM_COUNTER;

    /**
     * @brief m_name Name for the animation, used when saving it or with backups
     */
    std::string m_name;

    /**
     * @brief m_in_timeline Indicates if the current animation is used in the timeline
     */
    bool m_in_timeline = false;

    /**
     * @brief m_in_composing_bench Indicates if the current animation is in the composing bench, this avoids unloading it
     */
    bool m_in_composing_bench = false;
};

} // namespace lib_animation

#endif // ANIMATION_H
