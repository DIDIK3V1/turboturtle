/**
 @authors Creerio, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "templatedhlayouttools.h"

namespace lib_widgets {
    template<class Stored, class ToGive>
    TemplatedHLayoutTools<Stored, ToGive>::TemplatedHLayoutTools(QWidget *parent) : HLayoutTools(parent) {}

    template<class Stored, class ToGive>
    void TemplatedHLayoutTools<Stored, ToGive>::set_layout(TemplatedHLayout<Stored, ToGive> *layout) {
        // If m_layout already given, do not any allow modification of it
        if (m_layout == nullptr) {
            m_layout = layout;
            connect(m_layout, &HLayout::update_counter, this, &HLayoutTools::update_counter);
        }
    }

    template<class Stored, class ToGive>
    const int TemplatedHLayoutTools<Stored, ToGive>::duplicate() {
        size_t pos = m_layout->get_current_pos() + 1;
        std::vector<ToGive*> to_dupl= m_layout->get_current_copy();
        if(!to_dupl.empty()){
            for(ToGive *dupl : to_dupl){
                m_layout->insert(dupl, pos);
                ++pos;
            }
            return int(pos);
        }
        else
            return -1;
    }

    template<class Stored, class ToGive>
    void TemplatedHLayoutTools<Stored, ToGive>::remove() {
        m_layout->remove_selection();
    }

    template<class Stored, class ToGive>
    void TemplatedHLayoutTools<Stored, ToGive>::on_btnAdd_clicked() {
        if (m_layout == nullptr)
            return;

        add();
    }

    template<class Stored, class ToGive>
    void TemplatedHLayoutTools<Stored, ToGive>::on_btnDelete_clicked() {
        if (m_layout == nullptr)
            return;

        remove();
    }

    template<class Stored, class ToGive>
    void TemplatedHLayoutTools<Stored, ToGive>::on_btnDuplicate_clicked() {
        if (m_layout == nullptr)
            return;

        duplicate();
    }

}
