/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "mainwindow.h"
#include <QBrush>
#include <QGraphicsEllipseItem>
#include <QKeyEvent>
#include <QLabel>
#include <QMessageBox>
#include "lib_dialog/aboutdialog.h"
#include "lib_dialog/cameradialog.h"
#include "lib_dialog/onionskinningdialog.h"
#include "lib_library/widgets/animationlayout.h"
#include "ui_mainwindow.h"
#include <QStyle>
#include <QDir>
#include <QFileDialog>
#include <QDirIterator>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_ui(new Ui::MainWindow)
    , m_ui_is_lock(false)
    , m_is_camera_horizontal_mirror(false)
    , m_is_camera_vertical_mirror(false)
{
    this->setWindowIcon(QIcon("image/logo_turboturtle.ico"));
    m_ui->setupUi(this);
    m_timer = new QTimer(this);
    m_pos_anim = 0;

    m_canvas = m_ui->canvas;
    m_timeline = m_ui->widTimeline;
    m_library = m_ui->widLibrary;

    m_is_living = false;
    m_is_camera_on = false;

    m_ui->themesGroup->setExclusive(true);


    //Connects :
    QObject::connect(this, &MainWindow::updateCameraClicked, this, &MainWindow::set_text_ButCamera);
    QObject::connect(this, &MainWindow::updateCameraClicked, m_ui->canvas, &lib_canvas::Canvas::set_camera_state);
    QObject::connect(this, &MainWindow::updateCameraClicked, m_timeline, &lib_timeline::FrameTimeline::set_cam_started);
    QObject::connect(this, &MainWindow::undo_signal, m_ui->canvas, &lib_canvas::Canvas::on_undo);
    QObject::connect(this, &MainWindow::redo_signal, m_ui->canvas, &lib_canvas::Canvas::on_redo);
    QObject::connect(this, &MainWindow::signal_update_camera, this, &MainWindow::start_update_camera);
    QObject::connect(this, &MainWindow::send_frame_live_drawing, m_ui->composingbench, &lib_composing_bench::ComposingBench::get_frame_live_drawing);
    QObject::connect(m_ui->but_snap, &lib_widgets::SnapButton::snap, this, &MainWindow::snap);
    QObject::connect(m_ui->but_snap, &lib_widgets::SnapButton::snap_and_new_frame, this, &MainWindow::snap_and_new_frame);

    QObject::connect(m_ui->widColorWheel, &lib_color_selector::ColorSelector::color_changed, m_ui->canvas, &lib_canvas::Canvas::color_changed);

    QObject::connect(m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::thicknessChange, m_ui->canvas, &lib_canvas::Canvas::thickness_change);
    QObject::connect(this, &MainWindow::live_drawing, m_ui->composingbench, &lib_composing_bench::ComposingBench::start_stop_live_drawing);
    QObject::connect(m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::brushChange, m_ui->canvas, &lib_canvas::Canvas::brush_changed);
    QObject::connect(m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::toolChange, m_ui->canvas, &lib_canvas::Canvas::tool_changed);
    QObject::connect(m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::undo, m_ui->canvas, &lib_canvas::Canvas::on_undo);
    QObject::connect(m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::redo, m_ui->canvas, &lib_canvas::Canvas::on_redo);

    // Timeline related signals/slots
    QObject::connect(m_library->get_layout(), &lib_library::AnimationLayout::start_load_item, m_timeline, &lib_timeline::FrameTimeline::load_animation);
    QObject::connect(m_timeline, &lib_timeline::FrameTimeline::animation_loaded, m_library->get_layout(), &lib_library::AnimationLayout::finished_loading);
    QObject::connect(m_timeline, &lib_timeline::FrameTimeline::load_into_canvas, m_canvas, &lib_canvas::Canvas::set_frame);
    QObject::connect(m_ui->canvas, &lib_canvas::Canvas::update_frame_button, m_timeline, &lib_timeline::FrameTimeline::update_frame_thumbnail);
    QObject::connect(m_ui->canvas, &lib_canvas::Canvas::update_color_select, m_ui->widColorWheel, &lib_color_selector::ColorSelector::on_color_picked);

    //QObject::connect(m_ui->newAnimationFrame, &lib_drawing_tools::NewAnimationFrame::add_frame, this, &MainWindow::add_frame);

    // Composing bench
    QObject::connect(m_ui->composingbench, &lib_composing_bench::ComposingBench::get_canvas_frame, this, &MainWindow::get_canvas_frame);
    QObject::connect(m_ui->composingbench, &lib_composing_bench::ComposingBench::stop_live_drawing, this , &MainWindow::on_butStartStopLive_clicked);
    QObject::connect(m_ui->composingbench, &lib_composing_bench::ComposingBench::get_anim, m_library->get_layout(), &lib_library::AnimationLayout::send_animation_to_cb);
    QObject::connect(m_library->get_layout(), &lib_library::AnimationLayout::animation_to_cb, m_ui->composingbench, &lib_composing_bench::ComposingBench::get_animation);
    QObject::connect(m_library->get_layout(), &lib_library::AnimationLayout::animation_removed_to_cb, m_ui->composingbench, &lib_composing_bench::ComposingBench::remove_animation);
    QObject::connect(m_library->get_layout(), &lib_library::AnimationLayout::animation_new_id_to_cb, m_ui->composingbench, &lib_composing_bench::ComposingBench::update_animation_id);

    // Shortcut signals connection
    QObject::connect(this, &MainWindow::shortcut_l, m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::on_butColorPicker_clicked);
    QObject::connect(this, &MainWindow::shortcut_b, m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::on_butPaintbrush_clicked);
    QObject::connect(this, &MainWindow::shortcut_n, m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::on_butPencil_clicked);
    QObject::connect(this, &MainWindow::shortcut_p, m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::on_butMarker_clicked);

    QObject::connect(this, &MainWindow::shortcut_g, m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::on_butBucket_clicked);
    QObject::connect(this, &MainWindow::shortcut_e, m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::on_butEraser_clicked);
    QObject::connect(this, &MainWindow::shortcut_3, m_ui->composingbench, &lib_composing_bench::ComposingBench::on_butBroadcast_clicked);
    //!!QObject::connect(this, &MainWindow::shortcut_5, m_ui->groupBoxDrawingTools, &MainWindow::on_butStartStopLive_clicked);
    //QObject::connect(this, &MainWindow::shortcut_insert, m_ui->newAnimationFrame, &lib_drawing_tools::NewAnimationFrame::NewAnimationFrame::on_butAddFrame_clicked);
    QObject::connect(this, &MainWindow::shortcut_comma, m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::decrease_thickness);
    QObject::connect(this, &MainWindow::shortcut_semicolon, m_ui->groupBoxDrawingTools, &lib_drawing_tools::DrawingTools::increase_thickness);
    QObject::connect(this, &MainWindow::shortcut_less, m_ui->widColorWheel, &lib_color_selector::ColorSelector::decrease_alpha);
    QObject::connect(this, &MainWindow::shortcut_greater, m_ui->widColorWheel, &lib_color_selector::ColorSelector::increase_alpha);

    QObject::connect(this, &MainWindow::snap_camera, m_ui->canvas, &lib_canvas::Canvas::get_snaped);
    set_text_ButCamera(false);
    m_is_live_broadcast = false;
    m_is_live_drawing = false;
    m_is_looping = false;

    lib_frame::Frame *to_delete = m_canvas->get_frame();
    delete to_delete;
    //m_timeline->send_to_library();

    //create image based on canva size
    m_blackscreen = QImage(m_canvas->geometry().width(), m_canvas->geometry().height(), QImage::Format_RGB888);
    m_blackscreen.fill(Qt::black);
    m_device_id_camera = 0;

    // Load first animation
    m_library->addAnimation();
}

MainWindow::~MainWindow()
{
    qDebug() << "trying to delete MainWindow";
    delete m_timer;
    //delete m_live_timer;
    if (m_thread_cam.joinable())
    {
        m_thread_cam.join();
    }
    //delete m_ndi;
    delete m_ui;
    qDebug() << "MainWindow deleted correctly";
}

void MainWindow::closeEvent(QCloseEvent *event) // show prompt when user wants to close app
{
    event->ignore();
    if (QMessageBox::Yes
            == QMessageBox::question(this,
                                     "Close Confirmation",
                                     "Are you sur you want to quit ?",
                                     QMessageBox::Yes | QMessageBox::No)) {
        event->accept();
    }
}

void MainWindow::set_first_picture()
{
    cv::Mat m_frame(300, 300, CV_8UC3);

    cvtColor(m_frame, m_frame, cv::COLOR_BGR2RGBA);

    QImage image = QImage(m_frame.data, m_frame.cols, m_frame.rows, QImage::Format_RGBA8888)
            .scaled(m_ui->canvas->width(), m_ui->canvas->height());
    m_canvas->set_image(image);
    m_canvas->clear_image();
}

void MainWindow::update_window()
{
    m_cap >> m_frame;

    cvtColor(m_frame, m_frame, cv::COLOR_BGR2RGBA);

    try {
        m_frame = m_frame(cv::Range(65, 656), cv::Range(115, 1165));
        cv::resize(m_frame, m_frame, cv::Size(m_ui->canvas->width(), m_ui->canvas->height()));
    } catch (cv::Exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }

    QImage image = QImage(m_frame.data, m_frame.cols, m_frame.rows, QImage::Format_RGBA8888)
            .scaled(m_ui->canvas->width(), m_ui->canvas->height());


    // code to reverse the camera
    if(m_is_camera_vertical_mirror || m_is_camera_horizontal_mirror)
        image.mirror(m_is_camera_horizontal_mirror, m_is_camera_vertical_mirror);

    m_canvas->set_camera_background(image);
    m_canvas->set_camera_image(image);

}

void MainWindow::get_canvas_frame()
{
    emit send_frame_live_drawing(*m_ui->canvas->get_frame());
}


void MainWindow::start_update_camera()
{
    if (!m_cap.isOpened()) // Check if we succeeded
    {
        qDebug() << "camera is not open";
        emit updateCameraClicked(false);

    } else {
        qDebug() << "camera is open";
        connect(m_timer, SIGNAL(timeout()), this, SLOT(update_window()));
        m_timer->stop();
        m_timer->start(20);
        m_is_camera = true;
    }
}

void MainWindow::on_butRectShape_clicked()
{
    m_canvas->tool_changed(lib_drawing_tools::RECTANGLE_SELECTOR);
}

void MainWindow::on_butPolyShape_clicked()
{
    m_canvas->tool_changed(lib_drawing_tools::POLYGONAL_SELECTOR);
}

void MainWindow::snap()
{
    emit snap_camera();
    qDebug()<< "Took a picture";
}

void MainWindow::snap_and_new_frame()
{
    snap();
    m_timeline->add_new_frame();
}

void MainWindow::on_actionSnap_triggered()
{
    snap();
}


void MainWindow::on_actionSnap_and_New_Frame_triggered()
{
    snap_and_new_frame();
}


void MainWindow::on_actionCamera_triggered()
{
    on_but_startCamera_clicked();
}

void MainWindow::on_actionUndo_triggered()
{
    emit undo_signal();
}


void MainWindow::on_actionRedo_triggered()
{
    emit redo_signal();
}


void MainWindow::on_actionCut_triggered()
{
    if(m_canvas->get_is_rectangle_area_selected())
        m_canvas->cut_rectangle_selection();
    else if (m_canvas->get_is_polygon_area_selected())
        m_canvas->cut_polygonal_selection();
}


void MainWindow::on_actionCopy_triggered()
{
    if(m_canvas->get_is_rectangle_area_selected())
    {
        m_canvas->copy_rectangle_selection();

    } else if (m_canvas->get_is_polygon_area_selected())
    {
        m_canvas->copy_polygonal_selection();
    }
}


void MainWindow::on_actionPaste_triggered()
{
    m_canvas->paste_clipboard();
}

void MainWindow::on_but_startCamera_clicked()
{
    if (m_thread_cam.joinable())
    {
        m_thread_cam.join();
    }

    if (!m_cap.isOpened())
    {
        lib_dialog::CameraDialog camdial;
        QObject::connect(&camdial,
                         &lib_dialog::CameraDialog::camera_chosen,
                         this,
                         &MainWindow::camera_chosen);
        int res = camdial.exec();
        // Rejected
        if (res == 0)
            m_ui->actionCamera->setChecked(false);

    }
    else
    {
        m_thread_cam = std::thread(&MainWindow::close_camera_thread, this);
        disconnect(m_timer, SIGNAL(timeout()), this, SLOT(update_window()));
        m_is_camera = false;
        emit updateCameraClicked(false);
        //m_canvas->clear_image();
        //m_timeline->select_frame(m_timeline->get_nb_frames());

        qDebug() << "camera is closed";

        emit send_to_timeline_camera_state(false);
        m_canvas->set_camera_state(false);
        //m_timeline->reverse_camera(false);

    }
}

void MainWindow::camera_chosen(int val)
{
    emit updateCameraClicked(true);
    m_device_id_camera = val;
    m_thread_cam = std::thread(&MainWindow::open_camera_thread, this);

    emit send_to_timeline_camera_state(true);
}

void MainWindow::open_camera_thread()
{
    short deviceID = m_device_id_camera;
    short apiID = cv::CAP_ANY;

    try {
        m_cap.open(deviceID, apiID);
    } catch (cv::Exception &e) {
        QMessageBox::warning(
                    this,
                    "Camera Error",
                    "Could not open camera. Please make sure it is connected and try again.");
        m_ui->actionCamera->setChecked(false);
        return;
    }

    m_cap.set(cv::CAP_PROP_FRAME_WIDTH, m_ui->canvas->width());
    m_cap.set(cv::CAP_PROP_FRAME_HEIGHT, m_ui->canvas->height());

    emit signal_update_camera();
}

void MainWindow::close_camera_thread()
{
    m_cap.release();
}

void MainWindow::keyPressEvent(QKeyEvent *event)

{
    if(event->key() == Qt::Key_Shift) {
        m_canvas->resize_keep_aspect_ratio(true);
    }
    if (event->key() == Qt::Key_Delete) {
        if(m_ui->canvas->get_is_rectangle_area_selected())
            m_ui->canvas->delete_selection();
        else
            emit delete_frame();
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if(!this->m_ui_is_lock){
        switch(event->key()){
        case Qt::Key_Shift:
            m_canvas->resize_keep_aspect_ratio(false);
            break;
        case Qt::Key_Escape:
            m_canvas->deselect_selection();
            break;

        case int(Qt::Key_L) :
            emit shortcut_l();
            break;

        case Qt::Key_B :
            emit shortcut_b();
            break;

        case Qt::Key_N :
            emit shortcut_n();
            break;

        case Qt::Key_P :
            emit shortcut_p();
            break;

        case Qt::Key_G :
            emit shortcut_g();
            break;



        case Qt::Key_E :
            if (event->modifiers() == Qt::ControlModifier) {
                m_canvas->clear_image();
            }
            else{
                emit shortcut_e();
            }
            break;

        case Qt::Key_T :
            if (event->modifiers() == Qt::ControlModifier) {
                m_timeline->get_layout()->clear();
            }
            break;

        case Qt::Key_0 :
            if(!m_is_looping){
                emit play_animation();
            }
            break;
        /*
        case Qt::Key_3 :
            qDebug() << "NUM 3 TEST" << m_is_live_drawing;
            m_is_live_drawing = !m_is_live_drawing;
            emit shortcut_3();
            break;

        case Qt::Key_5 :
            if(!m_is_live_broadcast){
                emit shortcut_5();
            }
            break;
        */

        case Qt::Key_7 :
            emit shortcut_7();
            break;

        case Qt::Key_9 :
            emit shortcut_9();
            break;

        case Qt::Key_F:
            emit shortcut_insert();
            break;

        case Qt::Key_Comma :
            emit shortcut_comma(2);
            break;

        case Qt::Key_Semicolon :
            emit shortcut_semicolon(2);
            break;

        case Qt::Key_Less :
            emit shortcut_less(2);
            break;

        case Qt::Key_Greater :
            emit shortcut_greater(2);
            break;

        default :
            break;
        }
    }
}

void MainWindow::on_actionImport_folder_triggered()
{
    qDebug() << "action import triggered";
    m_ui->widLibrary->import_from_png();
}

void MainWindow::lock_ui(const bool state){
    this->setEnabled(!state);
    this->m_ui_is_lock = state;
}

void MainWindow::on_action_png_triggered()
{
    m_ui->widLibrary->save_all_png();
}

void MainWindow::on_actionImport_GIF_triggered()
{
    m_library->import_from_gif();
}


void MainWindow::on_actionSave_triggered()
{
    if (m_save_path.size() == 0)
        on_actionSave_As_triggered();
    else
        m_library->save_session(m_save_path);
}

void MainWindow::on_actionSave_As_triggered() {
    // Get where the zip should be saved
    QString zip_path = QFileDialog::getSaveFileName(this, "Select where the session will be saved", QDir::homePath() + "/Session_" + QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss") + ".ttle", "Session File (*.ttle);;");
    if (zip_path.isEmpty()) {
        qDebug() << "Main window" << "Session export : no folder given";
        return;
    }

    if (!zip_path.endsWith(".ttle", Qt::CaseInsensitive))
        zip_path.append(".ttle");

    m_save_path = zip_path;

    m_library->save_session(zip_path);
}

void MainWindow::on_actionOpen_Session_triggered() {
    // Get the zip file
    QString zip_path = QFileDialog::getOpenFileName(this, "Select the session to import", QDir::homePath(), "Session File (*.ttle);;");
    if (zip_path.isEmpty()) {
        qDebug() << "Main window" << "Session import : no file given";
        return;
    }

    if(QFileInfo file_info(zip_path); file_info.suffix() != "ttle"){
        qDebug() << "Main window" << "Session import : wrong file type given from " << zip_path;
        QMessageBox::information(this, "TurboTurtle : Session import", "Wrong file type, expected TTLE.");
        return;
    }

    m_save_path = zip_path;

    m_library->load_session(zip_path);
}

void MainWindow::on_action_gif_triggered()
{
    m_ui->widLibrary->save_all_gif();
}

void MainWindow::on_actionQuit_triggered()
{
    this->close();
}

void MainWindow::on_actionAbout_triggered()
{
    lib_dialog::AboutDialog about(this);
    about.exec();
}

void MainWindow::on_actionAbout_Qt_triggered()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::on_actionOnion_Skinning_triggered()
{
    // Create onion skinning dialog, link to value change
    lib_dialog::OnionSkinningDialog onion_skinning_settings;
    onion_skinning_settings.setValues(m_timeline->get_frames_before(), m_timeline->get_frames_after());
    QObject::connect(&onion_skinning_settings, &lib_dialog::OnionSkinningDialog::value_changed, m_timeline, &lib_timeline::FrameTimeline::update_skins);
    onion_skinning_settings.exec();
}

void MainWindow::on_actionNDI_triggered()
{

    m_ui->composingbench->send_ndi_state(m_ui->actionNDI->isChecked());
}

void MainWindow::on_actionReverse_triggered()
{
    m_is_camera_vertical_mirror = m_ui->actionReverse->isChecked();
    qDebug() << "Reversed camera";
}

void MainWindow::on_actionMirror_triggered()
{
    m_is_camera_horizontal_mirror = m_ui->actionMirror->isChecked();
    qDebug() << "Camera inverted";
}

void MainWindow::on_actionDocumentation_triggered()
{
    QString file_path = QDir::currentPath() + "/documentation.pdf";
    file_path = "file:///" + file_path;
    qDebug() << file_path << "       " << QDir::currentPath();
    QDesktopServices::openUrl(QUrl(file_path , QUrl::TolerantMode));
}
void MainWindow::on_actionDocumentation_en_ligne_triggered()
{
    QDesktopServices::openUrl(QUrl("https://gitlab.com/turboturtle/turboturtle/-/wikis/home"));
}

void MainWindow::on_actionTh_me_clair_triggered()
{
#ifdef Q_OS_WIN64
    QPalette brightPalette;
    brightPalette.setColor(QPalette::Window, QColor(240, 240, 240));
    brightPalette.setColor(QPalette::WindowText, QColor(17, 17, 17));
    brightPalette.setColor(QPalette::Base, QColor(250, 250, 250));
    brightPalette.setColor(QPalette::AlternateBase, QColor(240, 240, 240));
    brightPalette.setColor(QPalette::ToolTipBase, QColor(17, 17, 17));
    brightPalette.setColor(QPalette::ToolTipText, QColor(255, 255, 255));
    brightPalette.setColor(QPalette::Text, Qt::black);
    brightPalette.setColor(QPalette::Button, QColor(249, 249, 249));
    brightPalette.setColor(QPalette::ButtonText, Qt::black);
    brightPalette.setColor(QPalette::BrightText, Qt::red);
    brightPalette.setColor(QPalette::Link, QColor(37, 137, 164));
    brightPalette.setColor(QPalette::Highlight, QColor(137, 207, 84));
    brightPalette.setColor(QPalette::HighlightedText, Qt::black);
    qApp->setPalette(brightPalette);
#else
    m_ui->actionSystem_theme_2->setChecked(true);
    QMessageBox::information(this,"Unsupported","Changing themes is not supported on your system");
#endif
}

void MainWindow::on_actionTh_me_sombre_triggered()
{
#ifdef Q_OS_WIN64
    QPalette darkPalette;
    darkPalette.setColor(QPalette::Window, QColor(49, 49, 49));
    darkPalette.setColor(QPalette::WindowText, Qt::white);
    darkPalette.setColor(QPalette::Base, QColor(34, 34, 34));
    darkPalette.setColor(QPalette::AlternateBase, QColor(42, 42, 42));
    darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
    darkPalette.setColor(QPalette::ToolTipText, Qt::white);
    darkPalette.setColor(QPalette::Text, Qt::white);
    darkPalette.setColor(QPalette::Button, QColor(48, 48, 48));
    darkPalette.setColor(QPalette::ButtonText, Qt::white);
    darkPalette.setColor(QPalette::BrightText, Qt::red);
    darkPalette.setColor(QPalette::Link, QColor(47, 163, 198));
    darkPalette.setColor(QPalette::Highlight, QColor(150, 219, 89));
    darkPalette.setColor(QPalette::HighlightedText, Qt::black);
    darkPalette.setColor(QPalette::PlaceholderText, Qt::darkGray);
    qApp->setPalette(darkPalette);
#else
    m_ui->actionSystem_theme_2->setChecked(true);
    QMessageBox::information(this,"Unsupported","Changing themes is not supported on your system");
#endif
}

void MainWindow::on_actionSystem_theme_2_triggered()
{
#ifdef Q_OS_WIN64
    qApp->setPalette(this->style()->standardPalette());
#else
    m_ui->actionSystem_theme_2->setChecked(true);
    QMessageBox::information(this,"Unsupported","Changing themes is not supported on your system");
#endif
}

void MainWindow::activate_desactivate_live_drawing_button()
    {

        if(m_ui->butStartStopLive->isEnabled())
    {
            m_ui->butStartStopLive->setDisabled(true);
        }
        else{
            m_ui->butStartStopLive->setEnabled(true);
        }
    }

void MainWindow::on_butStartStopLive_clicked()
{
    if(m_is_living)
        {
            m_ui->butStartStopLive->setText("Live Drawing");
            m_is_living = false;
            emit live_drawing(false);
            qDebug("live_drawing false send");
            m_ui->but_startCamera->setEnabled(true);
            m_ui->but_startCamera->setToolTip("[u]");

        }
    else
        {
            m_ui->butStartStopLive->setText("Stop Live");
            m_is_living = true;
            emit live_drawing(true);
            qDebug("live_drawing true send");
            m_ui->but_startCamera->setEnabled(false);
            m_ui->but_startCamera->setToolTip("Disabled during the live");
        }
}

void MainWindow::set_text_ButCamera(bool b)
    {
        m_is_camera_on = b;
        if(m_is_camera_on)
        {
            m_ui->but_startCamera->setText("Disable camera");
            m_ui->actionCamera->setChecked(true);
            m_ui->butStartStopLive->setEnabled(false);
            m_ui->butStartStopLive->setToolTip("Disabled when camera is on");
            m_ui->but_snap->setEnabled(true);
            m_ui->actionSnap->setEnabled(true);
            m_ui->actionSnap_and_New_Frame->setEnabled(true);
        }
        else
        {
            m_ui->but_startCamera->setText("Enable camera");
            m_ui->actionCamera->setChecked(false);
            m_ui->butStartStopLive->setEnabled(true);
            m_ui->butStartStopLive->setToolTip("[5]");
            m_ui->but_snap->setEnabled(false);
            m_ui->actionSnap->setEnabled(false);
            m_ui->actionSnap_and_New_Frame->setEnabled(false);
        }
}
