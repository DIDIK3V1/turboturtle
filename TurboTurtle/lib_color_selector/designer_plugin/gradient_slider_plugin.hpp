/**
@author Julien BLANCO, Samuel GOUBEAU, Kévin DIDIER, Idaïa METZ, Lucas PLANCHET

@section License

    Copyright (C) 2023 Julien BLANCO, Samuel GOUBEAU, Kévin DIDIER, Idaïa METZ, Lucas PLANCHET

    This file is part of TurboTurtle.

    TurboTurtle is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TurboTurtle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TurboTurtle.  If not, see http://www.gnu.org/licenses/.
*/
#ifndef GRADIENT_SLIDER_PLUGIN_HPP
#define GRADIENT_SLIDER_PLUGIN_HPP

#include <QtUiPlugin/QDesignerCustomWidgetInterface>
class Gradient_Slider_Plugin : public QObject, public QDesignerCustomWidgetInterface
{
    Q_OBJECT
    Q_INTERFACES(QDesignerCustomWidgetInterface)

public:
    Gradient_Slider_Plugin(QObject *parent = 0);

    void initialize(QDesignerFormEditorInterface *core);
    bool isInitialized() const;

    QWidget *createWidget(QWidget *parent);

    QString name() const;
    QString group() const;
    QIcon icon() const;
    QString toolTip() const;
    QString whatsThis() const;
    bool isContainer() const;

    QString domXml() const;

    QString includeFile() const;

private:
    bool initialized;
};


#endif // GRADIENT_SLIDER_PLUGIN_HPP
