/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef UNDOREDO_H
#define UNDOREDO_H

#include <QImage>
#include <vector>
#include <iostream>

#define MAX_UNDO 15
namespace lib_frame {
/**
 * @brief The UndoRedo class used to store and perform undo/redo actions
 */
class UndoRedo
{
public:

    /**
     * @brief Constructor of UndoRedo
     */
    UndoRedo() = default;
    /**
     * @brief destuctor of the class
     */
    ~UndoRedo() = default;

    QImage undo_top();

    /**
     * @brief used to perform an undo action
     * @return the last image of undo
     */
    QImage undo();

    /**
     * @brief used to perform a redo action
     * @return the last image of redo
     */
    QImage redo();

    /**
     * @brief add image to undo
     * @param image
     */
    void add(const QImage &image);

    /**
     * @brief Resize the undo and redo frames
     * @param target size
    */
    void resize(const QSize &new_size);

    /**
     * @brief check if undo is empty
     * @return true if undo is empty
     */
    bool undo_is_empty();

    /**
     * @brief check if redo is empty
     * @return true if redo is empty
     */
    bool redo_is_empty();

    /**
     * @brief clear undo / redo
     */
    void clear();

    UndoRedo operator=(const UndoRedo&);

private:
    /**
     * @brief vector of undo frames
     */
    std::vector<QImage> m_undo_frames;

    /**
     * @brief vector of redo frames
     */
    std::vector<QImage> m_redo_frames;

    /**
     * @brief clear undo frames
     */
    void clear_undo();

    /**
     * @brief clear redo frames
     */
    void clear_redo();

};
} // namespace lib_frame
#endif // UNDOREDO_H
