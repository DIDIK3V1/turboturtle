/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Katarina

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef NDIMANAGER_H
#define NDIMANAGER_H

#include <QThread>
#include <QImage>
#include <QMutex>
#include <Processing.NDI.Lib.h>
#include "lib_canvas/frame.h"
#include "lib_canvas/canvas.h"
namespace lib_ndi {
/**
 * @brief class used to handle the ndi protocol to send an animation via ndi
 */
class ndimanager : public QThread
{
public:

    //static QMutex m_mutexImg;
    /**
     * @brief Default constructor
     */
    ndimanager();

    /**
     * @brief Default destructor
     */
    ~ndimanager();

    /**
     * @brief Set the current image send by NDI protocole
     * @param new image to be set
     */
    void set_ndi_img(lib_frame::Frame *frame);

protected:
    /**
     * @brief Overwrited method of QThread. Launch when start() is called
     */
    void run();

private:
    /**
     * @brief Current image send by NDI protocole
     */
    QImage m_img;

    /**
     * @brief NDI sender
     */
    NDIlib_send_instance_t m_NDI_send;

    /**
     * @brief pointer to the current frame to broadcast
     */
    lib_frame::Frame *m_frame;

    /**
     * @brief Send an image by NDI protocole
     */
    void send_qimage();
};
} // namespace lib_ndi
#endif // NDIMANAGER_H
