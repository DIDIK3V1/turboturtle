﻿/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Katarina, Creerio

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ndimanager.h"
#include <QDebug>
#include <opencv2/opencv.hpp>
namespace lib_ndi {
ndimanager::ndimanager() : QThread(0), m_frame(nullptr)
{
    m_img = QImage(1280, 720, QImage::Format_RGBA8888);
    m_img.fill(Qt::black);

    NDIlib_send_create_t sendDesc = {0};
    sendDesc.p_groups = NULL;
    sendDesc.clock_video = true;
    sendDesc.clock_audio = false;

    m_NDI_send = NDIlib_send_create();
}

ndimanager::~ndimanager()
{
    qDebug() << "trying to delete ndimanager";
    NDIlib_send_destroy(m_NDI_send);
    qDebug() << "ndimanager deleted successfully";
}

void ndimanager::set_ndi_img(lib_frame::Frame *frame){
    m_frame = frame;
}

void ndimanager::run()
{

    while(true) {
        // Frame clocking is done automatically
        send_qimage();
    }
}

void ndimanager::send_qimage()
{
    if(m_frame != nullptr) {
        NDIlib_video_frame_v2_t videoFrame = {0};

        // lock the shared mutex before making a copy of the image
        // using a copy avoids working with the original image
        // while the canvas may be drawing on it concurrently
        lib_canvas::Canvas::m_mutexImg.lock();
        QImage img = m_frame->image.copy();
        lib_canvas::Canvas::m_mutexImg.unlock();

        cv::Mat mat(
            img.height(),
            img.width(),
            CV_8UC4,const_cast<uchar*>(img.bits()),
            static_cast<size_t>(img.bytesPerLine())
            );


        videoFrame.xres = mat.cols;
        videoFrame.yres = mat.rows;
        videoFrame.p_data = mat.data;
        videoFrame.line_stride_in_bytes = mat.step;
        videoFrame.FourCC = NDIlib_FourCC_video_type_RGBA;

        //NDI framerate set to 30 FPS
        videoFrame.frame_rate_N = 1000;
        videoFrame.frame_rate_D = 1;

        NDIlib_send_send_video_async_v2(m_NDI_send, &videoFrame);
        NDIlib_send_send_video_async_v2(m_NDI_send, NULL);
    }
}
} // namespace lib_ndi
