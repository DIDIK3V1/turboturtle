/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "broadcastbutton.h"
#include <QAction>
#include <QDrag>
#include <QMenu>
#include <QMouseEvent>
#include "qmimedata.h"
#include <iostream>
namespace lib_composing_bench {

BroadcastButton::BroadcastButton(lib_animation::Animation *anim, size_t id, QWidget *Parent)
    : QPushButton(Parent)
    , m_anim(anim)
{
    m_anim_id = id;
    set_thumbnail(anim->get_thumbnail());
}

void BroadcastButton::set_id(size_t id)
{
    m_anim_id = id;
}

lib_animation::Animation *BroadcastButton::get_animation()
{
    return m_anim;
}

void BroadcastButton::set_thumbnail(const QImage &img)
{
    int w = 128; // à changer
    int h = 67;

    m_image = img;
    QPixmap pixmap = QPixmap::fromImage(m_image).scaled(w-5,h-5,Qt::KeepAspectRatio);
    this->setIconSize(QSize(w,h));
    this->setIcon(QIcon(pixmap));
    this->setFixedSize(w,h);
    this->setFlat(false);
}

void BroadcastButton::set_border(bool b)
{
    if(b){
        //Get the right color for the border
        QColor tmpColor=this->palette().color(QPalette::Highlight);
        std::stringstream ss;
        ss << "border: 2px solid rgb(" << tmpColor.red() << "," << tmpColor.green() << "," << tmpColor.blue() << ");";

        this->setStyleSheet(QString::fromStdString(ss.str()));
    }
    else{
        this->setStyleSheet("border: 0px");
    }
}

void BroadcastButton::mousePressEvent(QMouseEvent *e)
{
    e->ignore();
    /* if(e->button() == Qt::LeftButton){
        emit on_left_clicked(m_anim_id);

    }
   */
    /* else if(e->button()== Qt::RightButton ) {
        QMenu menu(this);
        QAction *action1 = menu.addAction("Save");
        QAction *action2 = menu.addAction("Delete");

        QAction *selectedAction = menu.exec(e->globalPos());

        if (selectedAction == action1) {
            emit save_anim(m_anim_id);
        } else if (selectedAction == action2) {
            emit delete_anim(m_anim_id);
        }
    }*/
}

void BroadcastButton::mouseMoveEvent(QMouseEvent *e)
{
    if (e->buttons() &Qt::LeftButton) {
        QDrag drag(this);
        QMimeData *mimeData = new QMimeData;

        QByteArray animData;
        QDataStream dataStream(&animData, QIODevice::WriteOnly);
        dataStream << (int) m_anim_id;
        mimeData->setData("application/x-broadcast-data", animData);

        drag.setMimeData(mimeData);
        drag.setPixmap(QWidget::grab(this->rect()));
        drag.exec(Qt::CopyAction);
    }
}


} // namespace lib_composing_bench
