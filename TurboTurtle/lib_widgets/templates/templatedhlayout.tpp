/**
 @authors Creerio, Katarina, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include <utility>

#include "templatedhlayout.h"
#include "ui_hlayout.h"

namespace lib_widgets {
    template<class Stored, class Given>
    TemplatedHLayout<Stored, Given>::TemplatedHLayout(QString mime_type, QWidget *parent) :
        HLayout(parent),
        m_mimeType(std::move(mime_type))
    {
        // Stored elements should have position indicators on both sides, so one is needed
        lib_widgets::posindicator *first = get_pos_indicator(0);
        m_item_separators.push_back(first);
        m_ui->HBoxItemContainer->insertWidget(0, first);
    }

    template<class Stored, class Given>
    TemplatedHLayout<Stored, Given>::~TemplatedHLayout()
    {
        // Stored pointers
        for (Stored *item : m_items)
        {
            delete item->get();
            delete item;
        }
        for (lib_widgets::posindicator *separator : m_item_separators)
        {
            delete separator;
        }
    }

    template<class Stored, class Given>
    const bool TemplatedHLayout<Stored, Given>::add(Given *item) {
        // Position is always times 2 + 1
        size_t pos = m_items.size();
        size_t insertPos = pos * 2 + 1;

        // Add item
        Stored* to_add = get_stored(item, pos);
        m_items.push_back(to_add);
        m_ui->HBoxItemContainer->insertWidget(-1, to_add);
        qDebug() << QString::fromStdString(get_name()) << "added item " << pos << " at pos " << insertPos;


        // Add separator
        lib_widgets::posindicator *separator_to_add = get_pos_indicator(m_item_separators.size());
        m_item_separators.push_back(separator_to_add);
        m_ui->HBoxItemContainer->insertWidget(-1, separator_to_add);
        qDebug() << QString::fromStdString(get_name()) << "added separator " << pos << " at pos " << insertPos + 1;
        refresh_order();

        // Update scroll area size accordingly
        update_min_height(to_add->minimumHeight());

        // Select current item
        clear_selection();
        select(pos);

        return true;
    }


    template<class Stored, class Given>
    const bool TemplatedHLayout<Stored, Given>::insert(Given *item, const size_t pos) {
        if (pos > m_items.size()) {
            qDebug() << QString::fromStdString(get_name()) << "failed to insert item at pos " << pos << " : Out of range";
            return false;
        }

        // At the end, use add instead
        if (pos == m_items.size()) {
            return add(item);
        }
        size_t insertPos = pos * 2 + 1;

        // Add item
        Stored* to_add = get_stored(item, pos);
        m_items.insert(m_items.begin() + pos, to_add);
        m_ui->HBoxItemContainer->insertWidget(insertPos, to_add);
        qDebug() << QString::fromStdString(get_name()) << "inserted item " << pos << " at pos " << insertPos;


        // Add separator, positioned after in the list to have a correct order
        lib_widgets::posindicator *separator_to_add = get_pos_indicator(0);
        m_item_separators.insert(m_item_separators.begin() + pos + 1, separator_to_add);
        m_ui->HBoxItemContainer->insertWidget(int(insertPos) + 1, separator_to_add);
        qDebug() << QString::fromStdString(get_name()) << "inserted separator " << pos << " at pos " << insertPos + 1;
        refresh_order();

        // Update scroll area size accordingly
        update_min_height(to_add->minimumHeight());

        // Select current item
        clear_selection();
        select(pos);

        return true;
    }

    template<class Stored, class Given>
    const bool TemplatedHLayout<Stored, Given>::remove(const int &pos) {
        // Do not remove elements if there is only one element (or none)
        if (is_empty() || m_items.size() <= 1)
            return false;

        if (pos < 0 || pos >= m_items.size()) {
            qDebug() << QString::fromStdString(get_name()) << "did not remove element at position " << pos << " : Out of range";
            return false;
        }

        const size_t loaded_pos = std::bit_cast<Stored*>(loaded_item)->get_pos();

        // Remove element from vectors
        unselect(pos);
        Stored* to_remove = m_items[pos];
        lib_widgets::posindicator* indicator_to_remove = m_item_separators[pos + 1];
        m_items.erase(m_items.begin() + pos);
        m_item_separators.erase(m_item_separators.begin() + pos + 1);
        m_ui->HBoxItemContainer->layout()->removeWidget(to_remove);
        m_ui->HBoxItemContainer->layout()->removeWidget(indicator_to_remove);
        qDebug() << QString::fromStdString(get_name()) << "removed element " << pos;
        // Items are owned by QT
        to_remove->hide();
        indicator_to_remove->hide();

        refresh_order();

        // Update scroll area size accordingly
        update_min_height(m_items[0]->minimumHeight());

        if (m_items.empty())
            m_current.clear();
        else {
            // Not deleting current item, no need to load new one
            if (loaded_pos != pos) {
                clear_selection();
                select(loaded_pos);
                return true;
            }

            clear_selection();
            if(pos == 0 || pos < loaded_pos){
                select(pos);
                emit load_item(m_items[pos]);
            } else {
                select(pos-1);
                emit load_item(m_items[pos-1]);
            }


        }

        return true;
    }

    template<class Stored, class Given>
    const bool TemplatedHLayout<Stored, Given>::remove(const Stored *elem) {
        if (is_empty())
            return false;

        size_t pos = -1;
        for (size_t i = 0; i < m_items.size(); ++i) {
            if (m_items[i] == elem) {
                pos = i;
                break;
            }
        }
        if (pos == -1)
            return false;

        return remove(pos);
    }

    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::remove_selection() {
        if (is_empty() || m_items.size() <= 1)
            return;

        // Create copy of selected elements
        std::vector<Stored*> current;
        for(Stored* ind: m_current){
            current.push_back(ind);
        }

        for(Stored* ind: current){
            this->remove(ind->get_pos());
        }

    }

    template<class Stored, class Given>
    const bool TemplatedHLayout<Stored, Given>::move_item(const size_t &from, size_t to) {
        if (is_empty() || from == to)
            return false;

        if (const size_t size = m_items.size(); from > size || to > size) {
            qDebug() << QString::fromStdString(get_name()) << "did not move item from position " << from << " to " << to << " : Out of range";
            return false;
        }
        // Fix "to" position. Movement to the left is valid, movement by one to the right is also valid, movement to right by more is invalid
        to = from + 1 >= to ? to : to - 1;

        // Insert position in the widget, also test if out of limits in every container
        size_t insertPos = to * 2 + 1;
        bool out_of_limit = (m_items.size() + m_item_separators.size()) <= insertPos;

        // Move item & separator in UI
        m_ui->HBoxItemContainer->layout()->removeWidget(m_items[from]);
        m_ui->HBoxItemContainer->layout()->removeWidget(m_item_separators[from + 1]);
        if (out_of_limit) {
            m_ui->HBoxItemContainer->insertWidget(-1, m_items[from]);
            m_ui->HBoxItemContainer->insertWidget(-1, m_item_separators[from + 1]);
        }
        else {
            m_ui->HBoxItemContainer->insertWidget(insertPos, m_items[from]);
            m_ui->HBoxItemContainer->insertWidget(int(insertPos) + 1, m_item_separators[from + 1]);
        }

        // Move item & separator in vector
        Stored *to_move = m_items[from];
        lib_widgets::posindicator *indicator_to_move = m_item_separators[from + 1];
        m_items.erase(m_items.begin() + from);
        m_item_separators.erase(m_item_separators.begin() + from + 1);

        if (out_of_limit) {
            m_items.push_back(to_move);
            m_item_separators.push_back(indicator_to_move);
        }
        else {
            m_items.insert(m_items.begin() + to, to_move);
            m_item_separators.insert(m_item_separators.begin() + to + 1, indicator_to_move);
        }

        refresh_order();
        qDebug() << QString::fromStdString(get_name()) << "moved item from position " << from << " to " << to;

        // Select current item
        click_select(out_of_limit ? m_items.size() - 1 : to);

        return true;
    }

    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::clear(const bool is_delete) {
        if (is_empty())
            return;

        // Empty container, no widget stored
        for (lib_widgets::posindicator *separator : m_item_separators) {
            m_ui->HBoxItemContainer->layout()->removeWidget(separator);
            separator->hide();
        }
        for (Stored *item : m_items) {
            m_ui->HBoxItemContainer->layout()->removeWidget(item);
            // Delete Frame/Animation and button
            delete item->get();
            item->hide();
        }

        m_item_separators.clear();
        m_items.clear();
        // No item selected, there are none
        m_current.clear();

        // Not closing the application, force a default position indicator
        if (!is_delete) {
            lib_widgets::posindicator *first = get_pos_indicator(0);
            m_item_separators.push_back(first);
            m_ui->HBoxItemContainer->insertWidget(0, first);
        }
    }

    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::clear_view()
    {
        if (is_empty())
            return;

        // Empty container, no widget stored
        for (lib_widgets::posindicator *separator : m_item_separators) {
            m_ui->HBoxItemContainer->layout()->removeWidget(separator);
            separator->hide();
        }
        for (Stored *items : m_items) {
            m_ui->HBoxItemContainer->layout()->removeWidget(items);
            items->hide();
        }

        // Widgets are owned by QT
        m_item_separators.clear();

        lib_widgets::posindicator *first = get_pos_indicator(0);
        m_item_separators.push_back(first);
        m_ui->HBoxItemContainer->insertWidget(0, first);
        qDebug() << QString::fromStdString(get_name()) << "cleared view";

        // No item selected, there are none
        m_current.clear();
    }

    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::empty() {
        if (is_empty())
            return;

        clear_view();
        m_items.clear();
    }

    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::refresh() {
        if (is_empty())
            return;

        // Since QT owns the items, emptying is required.. so a copy is needed
        std::vector<Given*> cp;
        for (Stored *item : m_items) {
            cp.push_back(item->get());
        }

        empty();

        // Add back the buttons
        for (Given *item : cp) {
            add(item);
        }
        qDebug() << QString::fromStdString(get_name()) << "refreshed view";

        // Select first item
        clear_selection();
        select(0);
        emit load_item(m_items[0]);
    }

    template<class Stored, class Given>
    const std::vector<const Stored*> TemplatedHLayout<Stored, Given>::get_current() const {
        std::vector<const Stored*> list;
        for(Stored* item : m_current){
            list.push_back(item);
        }
        return list;
    }

    template<class Stored, class Given>
    std::vector<Given*> TemplatedHLayout<Stored, Given>::get_current_copy() const {
        std::vector<Given*> copy;
        for(Stored* item : m_current){
            copy.push_back(new Given(*item->get()));
        }
        return copy;
    }

    template<class Stored, class Given>
    const int TemplatedHLayout<Stored, Given>::get_current_pos() const {
        int pos = -1;
        for(Stored* item : m_current){
            if(pos < item->get_pos()){
                pos = item->get_pos();
            }
        }
        return pos;
    }

    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::select(const int pos) {
        if (is_empty())
            return;

        if (const size_t size = m_items.size(); pos < 0 || pos >= size) {
            qDebug() << QString::fromStdString(get_name()) << "no element selected : Out of range";
            return;
        }

        // Block inserting if already in
        for (Stored *item : m_current) {
            if (item->get_pos() == pos)
                return;
        }

        m_current.push_back(m_items[pos]);
        m_items[pos]->set_border(true);

        if (m_current.size() == 1) {
            emit update_counter(pos + 1, m_items.size());
        }

        qDebug() << QString::fromStdString(get_name()) << "selected element at pos " << pos;
    }


    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::click_select(const int pos) {
        if (is_empty() || is_only_selected(pos))
            return;

        clear_selection();
        select(pos);
    }

    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::ctrl_select(const int pos) {
        if (is_empty())
            return;

        if (const size_t size = m_items.size(); pos < 0 || pos >= size) {
            qDebug() << QString::fromStdString(get_name()) << "no element selected : Out of range";
            return;
        }
        bool is_selected = false;
        for(Stored* item : m_current){
            if(item->get_pos() == pos) {
                is_selected = true;
                break;
            }
        }

        if(m_current.size() > 1 && is_selected){
            unselect(pos);
            qDebug() << QString::fromStdString(get_name()) << "un-selected element at pos " << pos;
        }

        if(!is_selected) {
            select(pos);
            qDebug() << QString::fromStdString(get_name()) << "selected element at pos " << pos;
        }
    }

    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::unselect(const int pos) {
        if (is_empty())
            return;

        if (const size_t size = m_items.size(); pos < 0 || pos >= size) {
            qDebug() << QString::fromStdString(get_name()) << "no element unselected : Out of range";
            return;
        }

        m_items[pos]->set_border(false);
        for (size_t i = 0; i < m_current.size(); ++i) {
            if (m_current[i]->get_pos() == pos) {
                m_current.erase(m_current.begin() + i);
                break;
            }
        }
        qDebug() << QString::fromStdString(get_name()) << "unselected element at pos " << pos;
    }

    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::clear_selection(){
        // Create copy of selected elements
        std::vector<Stored*> current;
        for(Stored* ind: m_current){
            current.push_back(ind);
        }

        for(Stored* item : current){
            unselect(item->get_pos());
        }
        qDebug() << QString::fromStdString(get_name()) << "Cleared selected elements";
    }

    template<class Stored, class Given>
    bool TemplatedHLayout<Stored, Given>::is_only_selected(const int pos) {
        if (is_empty() || m_current.size() > 1)
            return false;

        for (size_t i = 0; i < m_current.size(); ++i) {
            if (m_items[pos] == m_current[i]) {
                return true;
            }
        }

        return false;
    }

    template<class Stored, class Given>
    bool TemplatedHLayout<Stored, Given>::is_empty() const {
        return m_items.empty();
    }

    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::refresh_order() const {
        for (size_t i = 0; i < m_items.size(); ++i)
        {
            m_items[i]->set_pos(i);
            m_item_separators[i]->set_number(i);
        }
        m_item_separators[m_item_separators.size() - 1]->set_number(m_item_separators.size() - 1);
        qDebug() << QString::fromStdString(get_name()) << "updated item positions";
    }

    template<class Stored, class Given>
    Stored* TemplatedHLayout<Stored, Given>::get_stored(Given *item, const size_t &pos) {
        Stored* to_add = new Stored(item, m_mimeType, pos, this);
        to_add->set_thumbnail(item->get_thumbnail());

        // Give functioning aspects to item storage (click)
        QObject::connect(to_add,
                        &ImageButton::on_left_clicked,
                        this,
                        [this](const int item_pos){
                            // Do not reselect already selected elements
                            if (is_only_selected(item_pos))
                                return;

                            this->click_select(item_pos);
                            emit this->load_item(m_items[item_pos]);
        });

        // Allow to select multiple elements
        QObject::connect(to_add,
                        &ImageButton::on_ctrl_left_clicked,
                        this,
                        [this](const int btn_pos){
                            this->ctrl_select(btn_pos);
                        });

        return to_add;
    }

    template<class Stored, class Given>
    lib_widgets::posindicator* TemplatedHLayout<Stored, Given>::get_pos_indicator(const size_t &pos) {
        lib_widgets::posindicator *pos_indicator = new lib_widgets::posindicator(m_mimeType, pos, this);
        QObject::connect(pos_indicator,
                        &posindicator::itemDropped,
                        this,
                        &TemplatedHLayout<Stored, Given>::move_item);

        return pos_indicator;
    }

    template<class Stored, class Given>
    void TemplatedHLayout<Stored, Given>::save_loaded_item(QWidget *item) {
        HLayout::save_loaded_item(item);

        emit update_counter(std::bit_cast<Stored*>(item)->get_pos() + 1, m_items.size());
    }
}
