/**
 @authors Creerio, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "qpainter.h"
#include "frametimeline.h"
#include "lib_library/widgets/animationbutton.h"
#include "ui_frametimeline.h"
#include "lib_canvas/canvas.h"

namespace lib_timeline {

    FrameTimeline::FrameTimeline(QWidget *parent) :
            QWidget(parent),
            m_ui(new Ui::FrameTimeline) {
        m_ui->setupUi(this);

        // Link buttons to m_layout
        m_ui->widButtons->set_layout(m_ui->widTimeline);

        QObject::connect(m_ui->widTimeline, &lib_timeline::TimelineLayout::start_load_item, this, &lib_timeline::FrameTimeline::get_frame_to_canvas);

        // Connect TimelineTools and TimelineLayout to allow to play the animation
        connect(m_ui->widButtons, &lib_timeline::TimelineTools::launch_animation, m_ui->widTimeline, &lib_timeline::TimelineLayout::play_animation);
        connect(m_ui->widTimeline, &TimelineLayout::stop_animation, m_ui->widButtons, &lib_timeline::TimelineTools::end_animation);
        connect(m_ui->widButtons, &TimelineTools::stop_animation, m_ui->widTimeline, &lib_timeline::TimelineLayout::end_animation);

        // Connect the signal that indicate the beginning or the end of the animation playing to update m_anim_started
        connect(m_ui->widButtons, &lib_timeline::TimelineTools::launch_animation, this, [this]([[maybe_unused]]const bool val){ set_anim_started(true);} );
        connect(m_ui->widButtons, &TimelineTools::stop_animation, this, [this](){ set_anim_started(false);} );
        connect(m_ui->widTimeline, &TimelineLayout::stop_animation, this, [this](){ set_anim_started(false);});
    }

    FrameTimeline::~FrameTimeline() {
        m_ui->widTimeline->clear(true);
        delete m_ui;
    }

    lib_timeline::TimelineLayout *FrameTimeline::get_layout() {
        return m_ui->widTimeline;
    }

    const int FrameTimeline::get_frames_before() const {
        return m_nb_skins_before;
    }

    const int FrameTimeline::get_frames_after() const {
        return m_nb_skins_after;
    }

    void FrameTimeline::set_anim_started(bool is_started) {
        m_anim_started = is_started;
    }

    void FrameTimeline::set_cam_started(bool is_started) {
        m_cam_started = is_started;
        m_ui->widButtons->enable_disable_play(is_started);
    }

    lib_frame::Frame* FrameTimeline::get_onion_skinned_frame(int pos)
    {
        // No animation set. Should not happen, but in case it may...
        if (m_ui->widTimeline->m_animation == nullptr) {
            return new lib_frame::Frame();
        }

        // Before any operation, check the size of the images. If invalid it MUST be fixed for anything to work as intended
        for (lib_frame::Frame *frame : (*m_ui->widTimeline->m_animation)) {
            if (frame->image.size() != lib_canvas::Canvas::s_canvas_size)
                frame->resize(lib_canvas::Canvas::s_canvas_size);
        }

        // Animation or camera started, or out of range
        if(m_anim_started || m_cam_started || pos < 0 || m_ui->widTimeline->m_animation->size() <= pos){
            return new lib_frame::Frame((*m_ui->widTimeline->m_animation)[pos]->image.copy());
        }

        //number of skins before
        int nb_skins_b = std::min(m_nb_skins_before, pos);
        //index of the last skin to process
        int nb_skins_a = std::min((int)m_ui->widTimeline->m_animation->size(), pos + m_nb_skins_after + 1);

        QImage background = (*m_ui->widTimeline->m_animation)[pos]->background.copy();
        QImage img = (*m_ui->widTimeline->m_animation)[pos]->background.copy();

        //number of frames to process
        int divisor = nb_skins_b + (nb_skins_a - pos);

        int width = img.width();
        int height = img.height();
        std::vector<QRgb*> lines;

        //first composition
        //determines the average colour of each pixel of the frame based on the onion skinning frames
        for(int h = 0; h < height; h++) {
            QRgb *background_line = std::bit_cast<QRgb *>(img.scanLine(h));
            const QRgb *img_line = std::bit_cast<QRgb *>((*m_ui->widTimeline->m_animation)[pos]->image.scanLine(h));
            lines.clear();
            for (int index = pos - nb_skins_b; index < nb_skins_a; index++) {
                lines.push_back(std::bit_cast<QRgb *>((*m_ui->widTimeline->m_animation)[index]->image.scanLine(h)));
            }
            for (int w = 0; w < width; w++) {
                // Determine the average colour of each pixel of the frame based on the onion skinning frames
                double r = 0.f, g = 0.f, b = 0.f, a = 0.f;
                //sum of all colour channels for the pixel [w,h]
                for (QRgb const *line: lines) {
                    r += qRed(line[w]);
                    g += qGreen(line[w]);
                    b += qBlue(line[w]);
                    a += qAlpha(line[w]);
                }
                //average value for each colour channel
                r /= divisor;
                g /= divisor;
                b /= divisor;
                a /= divisor;

                // Merge the onion skinning into the displayed frame using the average of the two
                r += qRed(img_line[w]);
                g += qGreen(img_line[w]);
                b += qBlue(img_line[w]);
                a += qAlpha(img_line[w]);
                r /= 2;
                g /= 2;
                b /= 2;
                a /= 2;

                background_line[w] = qRgba(int(r), int(g), int(b), int(a));
            }
        }

        lib_frame::Frame *frame = new lib_frame::Frame(img, background);
        return frame;
    }

    void FrameTimeline::load_animation(QWidget *anim) {
        lib_animation::Animation *animation = std::bit_cast<lib_library::AnimationButton*>(anim)->get();
        if(animation != m_ui->widTimeline->m_animation){
            // Mark animations as unloaded/loaded
            if (m_ui->widTimeline->m_animation != nullptr)
                m_ui->widTimeline->m_animation->set_used_in_timeline(false);
            animation->set_used_in_timeline(true);

            if (!animation->is_loaded())
                animation->reload_from_backup();

            m_ui->widTimeline->m_animation = animation;
            m_ui->widTimeline->empty();
            for(lib_frame::Frame *frame : *m_ui->widTimeline->m_animation) {
                m_ui->widTimeline->add(frame);
            }
            m_ui->widTimeline->clear_selection();
            m_ui->widTimeline->load_first_frame();
        }
        // Mark animation as "loaded", even if nothing was loaded to avoid blocking animation loading
        emit animation_loaded(anim);
    }

    void FrameTimeline::update_skins(const size_t before, const size_t after) {
        m_nb_skins_before = int(before);
        m_nb_skins_after = int(after);

        // Reload frame to actualize onion skinning on the canvas
        // This also allows for a preview while modifying the values
        get_layout()->reload_frame();
    }

    void FrameTimeline::get_frame_to_canvas(QWidget *btn) {
        // Inform the layout that the loading is finished, here the frame is in memory so no issue should happen
        get_layout()->finished_loading(btn);
        lib_timeline::FrameButton const *frame_btn = std::bit_cast<lib_timeline::FrameButton*>(btn);

        emit load_into_canvas(frame_btn->get(), get_onion_skinned_frame(frame_btn->get_pos()));
    }

    void FrameTimeline::update_frame_thumbnail(lib_frame::Frame const *frame, const bool reloadFrame) const {
        m_ui->widTimeline->update_frame_thumbnail(frame);

        if (reloadFrame)
            m_ui->widTimeline->reload_frame();
    }

    void FrameTimeline::add_new_frame() {
        int new_frame_pos = std::bit_cast<const FrameButton*>(m_ui->widTimeline->get_loaded())->get_pos() + 1;
        m_ui->widTimeline->insert(new lib_frame::Frame(Qt::white), new_frame_pos);
        m_ui->widTimeline->load_frame(new_frame_pos);
    }


}// namespace lib_timeline
