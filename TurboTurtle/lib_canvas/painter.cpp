/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Creerio, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "painter.h"
#include "qpainter.h"
#include "qpainterpath.h"
#include <iostream>
#include <QStack>
namespace lib_canvas {
Painter::Painter(lib_frame::Frame  *frame_hidden, lib_frame::Frame  *frame_displayed)
{
    m_frame_hidden = frame_hidden;
    m_frame_displayed = frame_displayed;
}

void Painter::tablet_draw_line_to(const QTabletEvent *event,
                                  lib_drawing_tools::Brush &custom_brush,
                                  QPoint &last_point)
{
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    painter_hidden.setPen(Qt::NoPen);
    painter_hidden.setBrush(Qt::NoBrush);
    painter_displayed.setPen(Qt::NoPen);
    painter_displayed.setBrush(Qt::NoBrush);
    custom_brush.tablet_scale(event,m_pen_width);

    emit updateCursor();

    QColor temp_pen_color = m_pen_color;
    temp_pen_color.setAlphaF(std::min(float(m_pen_color.alphaF()*event->pressure()),m_pen_color.alphaF()));
    custom_brush.tablet_change_color_and_rotate(temp_pen_color,event);
    QPainterPath path;
    path.moveTo(last_point);
    path.lineTo(event->position().toPoint());
    qreal length = path.length();
    qreal pos = 0;
    // Calculate the size of the rotated image
    const QRect bbox = custom_brush.rect();
    const QSize size = bbox.size();

    while (pos < length) {
        qreal percent = path.percentAtLength(pos);
        QPoint tst(path.pointAtPercent(percent).x() - (size.width() / 2), path.pointAtPercent(percent).y() - (size.height() / 2));
        QImage brush =  custom_brush.get_brush();
        painter_displayed.drawImage(tst, brush);
        painter_hidden.drawImage(tst, brush);
        pos += 1;

    }
    last_point = event->position().toPoint();
    emit frameModified();
}

void Painter::tablet_draw_line_to_selection(const QTabletEvent *event,
                                            lib_drawing_tools::Brush &custom_brush,
                                            QPoint &last_point,
                                            const QRect &clipping_rect)
{
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    painter_hidden.setPen(Qt::NoPen);
    painter_hidden.setBrush(Qt::NoBrush);
    painter_displayed.setPen(Qt::NoPen);
    painter_displayed.setBrush(Qt::NoBrush);
    custom_brush.tablet_scale(event,m_pen_width);

    // add clipping rectangle to the painters
    painter_hidden.setClipRect(clipping_rect);
    painter_displayed.setClipRect(clipping_rect);

    emit updateCursor();

    QColor temp_pen_color = m_pen_color;
    temp_pen_color.setAlphaF(std::min(float(m_pen_color.alphaF()*event->pressure()),m_pen_color.alphaF()));
    custom_brush.tablet_change_color_and_rotate(temp_pen_color,event);
    QPainterPath path;
    path.moveTo(last_point);
    path.lineTo(event->position().toPoint());
    qreal length = path.length();
    qreal pos = 0;
    // Calculate the size of the rotated image
    const QRect bbox = custom_brush.rect();
    const QSize size = bbox.size();

    while (pos < length) {
        qreal percent = path.percentAtLength(pos);
        QPoint tst(path.pointAtPercent(percent).x() - (size.width() / 2), path.pointAtPercent(percent).y() - (size.height() / 2));
        QImage brush =  custom_brush.get_brush();
        painter_displayed.drawImage(tst, brush);
        painter_hidden.drawImage(tst, brush);
        pos += 1;

    }
    last_point = event->position().toPoint();
    emit frameModified();
}

void Painter::tablet_draw_line_to_selection(const QTabletEvent *event,
                                            lib_drawing_tools::Brush &custom_brush,
                                            QPoint &last_point,
                                            const QPolygon &clipping_poly)
{
    QRegion clipped_polygon(clipping_poly);
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    painter_hidden.setPen(Qt::NoPen);
    painter_hidden.setBrush(Qt::NoBrush);
    painter_displayed.setPen(Qt::NoPen);
    painter_displayed.setBrush(Qt::NoBrush);
    custom_brush.tablet_scale(event,m_pen_width);

    // add clipping rectangle to the painters
    painter_hidden.setClipRegion(clipped_polygon);
    painter_displayed.setClipRegion(clipped_polygon);

    emit updateCursor();

    QColor temp_pen_color = m_pen_color;
    temp_pen_color.setAlphaF(std::min(float(m_pen_color.alphaF()*event->pressure()),m_pen_color.alphaF()));
    custom_brush.tablet_change_color_and_rotate(temp_pen_color,event);
    QPainterPath path;
    path.moveTo(last_point);
    path.lineTo(event->position().toPoint());
    qreal length = path.length();
    qreal pos = 0;
    // Calculate the size of the rotated image
    const QRect bbox = custom_brush.rect();
    const QSize size = bbox.size();

    while (pos < length) {
        qreal percent = path.percentAtLength(pos);
        QPoint tst(path.pointAtPercent(percent).x() - (size.width() / 2), path.pointAtPercent(percent).y() - (size.height() / 2));
        QImage brush =  custom_brush.get_brush();
        painter_displayed.drawImage(tst, brush);
        painter_hidden.drawImage(tst, brush);
        pos += 1;

    }
    last_point = event->position().toPoint();
    emit frameModified();
}

void Painter::print_pasted_image(const QImage &img, const QRect &rect) {
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    painter_hidden.drawImage(rect, img);
    painter_displayed.drawImage(rect, img);
    emit frameModified();
}

void Painter::set_frame(lib_frame::Frame *hidden, lib_frame::Frame *displayed ){
    m_frame_hidden = hidden;
    m_frame_displayed = displayed;
}

void Painter::draw_line_to(const QPoint &end_point,
                           lib_drawing_tools::Brush &custom_brush,
                           QPoint &last_point)
{
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    painter_hidden.setPen(Qt::NoPen);
    painter_hidden.setBrush(Qt::NoBrush);
    painter_displayed.setPen(Qt::NoPen);
    painter_displayed.setBrush(Qt::NoBrush);

    custom_brush.change_color(m_pen_color);
    custom_brush.change_width(m_pen_width);

    QPainterPath path;
    path.moveTo(last_point);
    path.lineTo(end_point);
    qreal length = path.length();
    qreal pos = 0;

    const QRect bbox = custom_brush.rect();
    const QSize size = bbox.size();

    while (pos < length) {
        qreal percent = path.percentAtLength(pos);
        QPoint point(path.pointAtPercent(percent).x() - (size.width() / 2), path.pointAtPercent(percent).y() - (size.height() / 2));
        QImage brush =  custom_brush.get_brush();
        painter_hidden.drawImage(point, brush);
        painter_displayed.drawImage(point, brush);
        ++pos;
    }
    last_point = end_point;

    emit frameModified();
}

void Painter::draw_line_to_selection(const QPoint &end_point,
                           lib_drawing_tools::Brush &custom_brush,
                           QPoint &last_point,
                           const QRect &clipping_rect)
{
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    painter_hidden.setPen(Qt::NoPen);
    painter_hidden.setBrush(Qt::NoBrush);
    painter_displayed.setPen(Qt::NoPen);
    painter_displayed.setBrush(Qt::NoBrush);
    // set the clipping rectangle of the painter
    painter_hidden.setClipRect(clipping_rect);
    painter_displayed.setClipRect(clipping_rect);

    custom_brush.change_color(m_pen_color);
    custom_brush.change_width(m_pen_width);

    QPainterPath path;
    path.moveTo(last_point);
    path.lineTo(end_point);
    qreal length = path.length();
    qreal pos = 0;

    const QRect bbox = custom_brush.rect();
    const QSize size = bbox.size();

    while (pos < length) {
        qreal percent = path.percentAtLength(pos);
        QPoint point(path.pointAtPercent(percent).x() - (size.width() / 2), path.pointAtPercent(percent).y() - (size.height() / 2));
        QImage brush =  custom_brush.get_brush();
        painter_hidden.drawImage(point, brush);
        painter_displayed.drawImage(point, brush);
        ++pos;
    }
    last_point = end_point;

    emit frameModified();
}

void Painter::draw_line_to_selection(const QPoint &end_point,
                                     lib_drawing_tools::Brush &custom_brush,
                                     QPoint &last_point,
                                     const QPolygon &clipping_poly)
{
    // QRegion representing the polygonal area
    QRegion clipped_polygon(clipping_poly);
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    painter_hidden.setPen(Qt::NoPen);
    painter_hidden.setBrush(Qt::NoBrush);
    painter_displayed.setPen(Qt::NoPen);
    painter_displayed.setBrush(Qt::NoBrush);
    // set the clipping region of the painter
    painter_hidden.setClipRegion(clipped_polygon);
    painter_displayed.setClipRegion(clipped_polygon);

    custom_brush.change_color(m_pen_color);
    custom_brush.change_width(m_pen_width);

    QPainterPath path;
    path.moveTo(last_point);
    path.lineTo(end_point);
    qreal length = path.length();
    qreal pos = 0;

    const QRect bbox = custom_brush.rect();
    const QSize size = bbox.size();

    while (pos < length) {
        qreal percent = path.percentAtLength(pos);
        QPoint point(path.pointAtPercent(percent).x() - (size.width() / 2), path.pointAtPercent(percent).y() - (size.height() / 2));
        QImage brush =  custom_brush.get_brush();
        painter_hidden.drawImage(point, brush);
        painter_displayed.drawImage(point, brush);
        ++pos;
    }
    last_point = end_point;

    emit frameModified();
}

void Painter::draw_point_to(const QPoint &end_point, lib_drawing_tools::Brush &custom_brush)
{
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    painter_hidden.setPen(Qt::NoPen);
    painter_hidden.setBrush(Qt::NoBrush);
    painter_displayed.setPen(Qt::NoPen);
    painter_displayed.setBrush(Qt::NoBrush);

    custom_brush.change_color(m_pen_color);
    custom_brush.change_width(m_pen_width);

    const QRect bbox = custom_brush.rect();
    const QSize size = bbox.size();

    QPoint point(end_point.x() - (size.width() / 2), end_point.y() - (size.height() / 2));
    QImage brush =  custom_brush.get_brush();
    painter_hidden.drawImage(point, brush);
    painter_displayed.drawImage(point, brush);


    emit frameModified();
}

void Painter::draw_point_to_selection(const QPoint &end_point,
                                      lib_drawing_tools::Brush &custom_brush,
                                      const QRect &clipping_rect)
{
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    painter_hidden.setPen(Qt::NoPen);
    painter_hidden.setBrush(Qt::NoBrush);
    painter_hidden.setClipRect(clipping_rect);
    painter_displayed.setPen(Qt::NoPen);
    painter_displayed.setBrush(Qt::NoBrush);
    painter_displayed.setClipRect(clipping_rect);

    custom_brush.change_color(m_pen_color);
    custom_brush.change_width(m_pen_width);

    const QRect bbox = custom_brush.rect();
    const QSize size = bbox.size();

    QPoint point(end_point.x() - (size.width() / 2), end_point.y() - (size.height() / 2));
    QImage brush =  custom_brush.get_brush();
    painter_hidden.drawImage(point, brush);
    painter_displayed.drawImage(point, brush);


    emit frameModified();
}

void Painter::draw_point_to_selection(const QPoint &end_point,
                                      lib_drawing_tools::Brush &custom_brush,
                                      const QPolygon &clipping_poly)
{
    QRegion clip_region(clipping_poly);
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    painter_hidden.setPen(Qt::NoPen);
    painter_hidden.setBrush(Qt::NoBrush);
    painter_hidden.setClipRegion(clip_region);
    painter_displayed.setPen(Qt::NoPen);
    painter_displayed.setBrush(Qt::NoBrush);
    painter_displayed.setClipRegion(clip_region);

    custom_brush.change_color(m_pen_color);
    custom_brush.change_width(m_pen_width);

    const QRect bbox = custom_brush.rect();
    const QSize size = bbox.size();

    QPoint point(end_point.x() - (size.width() / 2), end_point.y() - (size.height() / 2));
    QImage brush =  custom_brush.get_brush();
    painter_hidden.drawImage(point, brush);
    painter_displayed.drawImage(point, brush);


    emit frameModified();
}

QPair<QImage, QImage> Painter::calculate_erased(const QPoint &end_point,int pen_width)
{
    QRect rect(end_point.x(), end_point.y(), pen_width, pen_width);

    QImage cropped_displayed = m_frame_displayed->background.copy(rect);
    QImage cropped_hidden = m_frame_hidden->background.copy(rect);

    return {cropped_hidden, cropped_displayed};
}

void Painter::erase_foreground(const QPoint &end_point, QPoint &last_point)
{
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    QPainterPath path;
    path.moveTo(last_point);
    path.lineTo(end_point);
    qreal length = path.length();
    qreal pos = -1.f;

    while (pos < length) {
        qreal percent = path.percentAtLength(pos);
        QPoint point(path.pointAtPercent(percent).x() - (m_pen_width / 2), path.pointAtPercent(percent).y() - (m_pen_width / 2));
        QPair<QImage, QImage> erased = calculate_erased(point,m_pen_width);
        painter_hidden.drawImage(point, erased.first);
        painter_displayed.drawImage(point, erased.second);
        pos++;
    }
    last_point = end_point;

    emit frameModified();
}

void Painter::erase_foreground_selection(const QPoint &end_point, QPoint &last_point, const QRect &clipping_rect)
{
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    // set the clipping rectangle of the painter
    painter_hidden.setClipRect(clipping_rect);
    painter_displayed.setClipRect(clipping_rect);
    QPainterPath path;
    path.moveTo(last_point);
    path.lineTo(end_point);
    qreal length = path.length();
    qreal pos = -1.f;

    while (pos < length) {
        qreal percent = path.percentAtLength(pos);
        QPoint point(path.pointAtPercent(percent).x() - (m_pen_width / 2), path.pointAtPercent(percent).y() - (m_pen_width / 2));
        QPair<QImage, QImage> erased = calculate_erased(point,m_pen_width);
        painter_hidden.drawImage(point, erased.first);
        painter_displayed.drawImage(point, erased.second);
        pos++;
    }
    last_point = end_point;

    emit frameModified();
}

void Painter::erase_foreground_selection(const QPoint &end_point, QPoint &last_point, const QPolygon &clipping_poly)
{
    // QRegion representing the polygonal area
    QRegion clipped_polygon(clipping_poly);
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    // set the clipping region of the painter
    painter_hidden.setClipRegion(clipped_polygon);
    painter_displayed.setClipRegion(clipped_polygon);
    QPainterPath path;
    path.moveTo(last_point);
    path.lineTo(end_point);
    qreal length = path.length();
    qreal pos = -1.f;

    while (pos < length) {
        qreal percent = path.percentAtLength(pos);
        QPoint point(path.pointAtPercent(percent).x() - (m_pen_width / 2), path.pointAtPercent(percent).y() - (m_pen_width / 2));
        QPair<QImage, QImage> erased = calculate_erased(point,m_pen_width);
        painter_hidden.drawImage(point, erased.first);
        painter_displayed.drawImage(point, erased.second);
        pos++;
    }
    last_point = end_point;

    emit frameModified();
}

void Painter::erase_selection(const QRect &area_to_delete)
{
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    QImage cropped_displayed = m_frame_displayed->background.copy(area_to_delete);
    QImage cropped_hidden = m_frame_hidden->background.copy(area_to_delete);

    painter_hidden.drawImage(area_to_delete, cropped_hidden);
    painter_displayed.drawImage(area_to_delete, cropped_displayed);

    emit frameModified();
}

void Painter::erase_selection(const QPolygon &polygon_to_delete)
{
    QPainter painter_hidden(&m_frame_hidden->image);
    QPainter painter_displayed(&m_frame_displayed->image);
    QRegion clipping_polygon(polygon_to_delete);
    painter_hidden.setClipRegion(clipping_polygon);
    painter_displayed.setClipRegion(clipping_polygon);

    QImage background_displayed = m_frame_displayed->background.copy();
    QImage background_hidden = m_frame_hidden->background.copy();

    painter_hidden.drawImage(m_frame_hidden->image.rect(), background_hidden);
    painter_displayed.drawImage(m_frame_displayed->image.rect(), background_displayed);
}

void Painter::fill_shape(const QPoint &point, const QColor &base_color, const QColor &pen_color) {
    QStack<QPoint> points;
    points.push(point);

    if(pen_color.name() == base_color.name()){
        return;
    }

    while (!points.empty()) {
        QPoint current_point = points.top();
        points.pop();

        int x = current_point.x();
        int y = current_point.y();
        if (m_frame_hidden->image.pixelColor(x, y) == base_color) {
            m_frame_hidden->image.setPixelColor(current_point, pen_color);

            if (x - 1 >= 0){
                points.push(QPoint(x - 1, y));
            }

            if (x + 1 < m_frame_hidden->image.width()){
                points.push(QPoint(x + 1, y));
            }

            if (y - 1 >= 0 ){
                    points.push(QPoint(x, y - 1));
                }

            if (y + 1 < m_frame_hidden->image.height()){
                points.push(QPoint(x, y + 1));
            }
        }
    }

    emit frameModified();
}

void Painter::fill_shape(const QPoint &point, const QColor &base_color, const QColor &pen_color, const QRect &selection_rectangle) {
    QStack<QPoint> points;
    points.push(point);

    if(pen_color.name() == base_color.name()){
        return;
    }

    while (!points.empty()) {
        QPoint current_point = points.top();
        points.pop();

        int x = current_point.x();
        int y = current_point.y();
        if (m_frame_hidden->image.pixelColor(x, y) == base_color) {
            m_frame_hidden->image.setPixelColor(current_point, pen_color);

            if (x - 1 >= 0 && selection_rectangle.contains(x - 1, y)){
                points.push(QPoint(x - 1, y));
            }

            if (x + 1 < m_frame_hidden->image.width() && selection_rectangle.contains(x + 1, y)){
                points.push(QPoint(x + 1, y));
            }

            if (y - 1 >= 0 && selection_rectangle.contains(x, y - 1)){
                points.push(QPoint(x, y - 1));
            }

            if (y + 1 < m_frame_hidden->image.height() && selection_rectangle.contains(x, y + 1)){
                points.push(QPoint(x, y + 1));
            }
        }
    }

    emit frameModified();
}

void Painter::fill_shape(const QPoint &point, const QColor &base_color, const QColor &pen_color, const QPolygon &selection_polygon) {
    QStack<QPoint> points;
    points.push(point);

    if(pen_color.name() == base_color.name()){
        return;
    }

    while (!points.empty()) {
        QPoint current_point = points.top();
        points.pop();

        int x = current_point.x();
        int y = current_point.y();
        if (m_frame_hidden->image.pixelColor(x, y) == base_color) {
            m_frame_hidden->image.setPixelColor(current_point, pen_color);

            if (x - 1 >= 0 && selection_polygon.containsPoint(QPoint(x - 1, y), Qt::OddEvenFill)){
                points.push(QPoint(x - 1, y));
            }

            if (x + 1 < m_frame_hidden->image.width(), selection_polygon.containsPoint(QPoint(x + 1, y), Qt::OddEvenFill)){
                points.push(QPoint(x + 1, y));
            }

            if (y - 1 >= 0, selection_polygon.containsPoint(QPoint(x, y - 1), Qt::OddEvenFill)){
                points.push(QPoint(x, y - 1));
            }

            if (y + 1 < m_frame_hidden->image.height(), selection_polygon.containsPoint(QPoint(x, y + 1), Qt::OddEvenFill)){
                points.push(QPoint(x, y + 1));
            }
        }
    }

    emit frameModified();
}

void Painter::set_cursor_color(lib_frame::Frame *frame,
                               QPoint point,
                               lib_drawing_tools::Brush &brush)
{
    if (point.x() > 0 && point.x() < frame->image.width() && point.y() > 0
        && point.y() < frame->image.height()) {
        brush.extract_border(frame->image.pixelColor(point));
    }
}
} // namespace lib_canvas
