/**
 @author Creerio

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "templatedimagebutton.h"
#include "templatedhlayout.h"

namespace lib_widgets {
    template<class ToStore>
    TemplatedImageButton<ToStore>::TemplatedImageButton(ToStore *item, const QString &mime_type, const int pos, QWidget *parent) : ImageButton(mime_type, parent, pos), m_stored(item) {}

    template<class ToStock>
    ToStock* TemplatedImageButton<ToStock>::get() const {
        return m_stored;
    }
}
