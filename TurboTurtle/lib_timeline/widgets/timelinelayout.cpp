/**
 @authors Creerio, Katarina, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "timelinelayout.h"
#include "qtimer.h"
#include <QScrollBar>

namespace lib_timeline {


    TimelineLayout::TimelineLayout(QWidget *parent) :
            TemplatedHLayout("application/x-frame-data", parent)
            , m_is_playing(false)
            , m_is_looping(false)
            , m_animation(nullptr)
    {
        m_timer = new QTimer(this);

        // Bind shortcuts
        m_shortcuts.push(std::make_unique<QShortcut>(QKeySequence::MoveToPreviousChar, this, SLOT(load_previous())));
        m_shortcuts.push(std::make_unique<QShortcut>(QKeySequence("1"), this, SLOT(load_previous())));
        m_shortcuts.push(std::make_unique<QShortcut>(QKeySequence::MoveToNextChar, this, SLOT(load_next())));
        m_shortcuts.push(std::make_unique<QShortcut>(QKeySequence("2"), this, SLOT(load_next())));
    }

    void TimelineLayout::load_frame(int pos) {
        if (-1 < pos && pos < m_items.size()) {
            select(pos);
            emit load_item(m_items[pos]);
        }
    }

    void TimelineLayout::load_first_frame()
    {
        select(0);
        emit load_item(m_items[0]);
    }

    void TimelineLayout::reload_frame()
    {
        const int pos = std::bit_cast<const FrameButton*>(get_loaded())->get_pos();
        select(pos);
        emit load_item(m_items[pos]);
    }

    void TimelineLayout::load_last_frame()
    {
        select(m_items.size() - 1);
        emit load_item(m_items[m_items.size() - 1]);
    }

    const bool TimelineLayout::add(lib_frame::Frame *frame)
    {
        // Add failed, stop there
        if (!TemplatedHLayout::add(frame))
            return false;

        // Security : m_animation must be updated with the new frames... however we DO NOT want to add a frame already inside the animation (since add is called when loading an animation)
        if (std::ranges::find(m_animation->begin(), m_animation->end(), frame) == m_animation->end())
            m_animation->push_back(frame);
        m_animation->is_modified = true;

        return true;
    }

    const bool TimelineLayout::insert(lib_frame::Frame *frame, const size_t pos)
    {
        // Insertion failed, stop there
        if (!TemplatedHLayout::insert(frame, pos))
            return false;

        // Security : m_animation must be updated with the new frames... however we DO NOT want to add a frame already inside the animation
        if (std::ranges::find(m_animation->begin(), m_animation->end(), frame) == m_animation->end())
            m_animation->insert(m_animation->begin() + pos, frame);
        m_animation->is_modified = true;

        // Invalidate every animation from pos to the end, the numbering has changed
        for (size_t i = pos; i < m_items.size(); i++)
            m_items[i]->get()->is_modified = true;

        return true;
    }

    const bool TimelineLayout::remove(const int &pos)
    {
        if (m_items.size() <= 1) {
            return false;
        }

        if (TemplatedHLayout::remove(pos)) {
            lib_frame::Frame *to_delete = (*m_animation)[pos];
            m_animation->erase(m_animation->begin() + pos);
            delete to_delete;
            m_animation->is_modified = true;

            return true;
        }
        else
            return false;

    }

    const bool TimelineLayout::move_item(const size_t &from, size_t to)
    {
        if (TemplatedHLayout::move_item(from, to)) {
            lib_frame::Frame *to_move = (*m_animation)[from];
            m_animation->erase(m_animation->begin() + from);

            // Fix "to" position. Movement to the left is valid, movement by one to the right is also valid, movement to right by more is invalid
            to = from + 1 >= to ? to : to - 1;

            if (m_animation->size() <= to)
                m_animation->push_back(to_move);
            else
                m_animation->insert(m_animation->begin() + to, to_move);
            m_animation->is_modified = true;
            return true;
        }
        return false;
    }

    void TimelineLayout::select(const int pos)
    {
        TemplatedHLayout::select(pos);
        size_t nb_items = m_items.size() == 0 ? 1 : m_items.size();
        int scroll_pos = (pos * m_ui->scrollArea->horizontalScrollBar()->maximum())/nb_items;

        m_ui->scrollArea->horizontalScrollBar()->setValue(scroll_pos);
    }

    void TimelineLayout::clear(const bool is_delete)
    {
        // Clear the frames buttons & timeline
        TemplatedHLayout::clear(is_delete);

        // Also empty the animation from its pointers
        m_animation->clear();

        // Don't put back a frame when deleting the object
        if (is_delete)
            return;

        // Put default frame back
        lib_frame::Frame *frame = new lib_frame::Frame(Qt::white);
        add(frame);
        select(0);
        load_item(m_items[0]);
        m_animation->is_modified = true;
    }


    void TimelineLayout::update_frame_thumbnail(lib_frame::Frame const *frame) const
    {
        for (lib_timeline::FrameButton *btn : m_items) {
            // Find the only frame that is identical
            if (btn->get() == frame) {
                btn->set_thumbnail(frame->get_thumbnail());
                // Also mark as modified so the backup system can save it
                btn->get()->is_modified = true;
                m_animation->is_modified = true;
                break;
            }
        }
    }

    const bool TimelineLayout::is_playing() const
    {
        return m_is_playing;
    }

    void TimelineLayout::play_animation(bool loop)
    {
        m_is_looping = loop;
        m_is_playing = true;
        const FrameButton *fr = std::bit_cast<const FrameButton*>(get_loaded());
        m_frame_displayed = fr->get_pos();
        if(loop){
            connect(m_timer, &QTimer::timeout, this, &lib_timeline::TimelineLayout::update_loop);
            m_timer->start(84);
        } else {
            connect(m_timer, &QTimer::timeout, this, &lib_timeline::TimelineLayout::update_once);
            m_timer->start(84);
        }
    }

    void TimelineLayout::update_loop()
    {
        if(m_is_playing) {
            if (m_frame_displayed == m_items.size() - 1) {
                m_frame_displayed = 0;
            } else {
                ++m_frame_displayed;
            }
            clear_selection();
            select(m_frame_displayed);
            emit load_item(m_items[m_frame_displayed]);
        }
    }

    void TimelineLayout::update_once()
    {
        if(m_is_playing) {
            if (m_frame_displayed == m_items.size() - 1) {
                end_animation();
                emit stop_animation();
            } else {
                ++m_frame_displayed;
            }
            clear_selection();
            select(m_frame_displayed);
            emit load_item(m_items[m_frame_displayed]);
        }
    }

    void TimelineLayout::end_animation()
    {
        m_is_playing = false;
        if(m_is_looping){
            disconnect(m_timer, &QTimer::timeout, this, &lib_timeline::TimelineLayout::update_loop);
        }else {
            disconnect(m_timer, &QTimer::timeout, this, &lib_timeline::TimelineLayout::update_once);
        }
        m_timer->stop();
        emit stop_animation();
    }

    void TimelineLayout::load_previous() {
        const int select_pos = std::bit_cast<const FrameButton*>(get_loaded())->get_pos() - 1;
        if (-1 < select_pos && select_pos < m_items.size()) {
            clear_selection();
            select(select_pos);
            load_frame(select_pos);
        }
        else if (select_pos == -1) {
            const int pos = int(m_items.size()) - 1;
            clear_selection();
            select(pos);
            load_frame(pos);
        }
    }

    void TimelineLayout::load_next() {
        const int select_pos = std::bit_cast<const FrameButton*>(get_loaded())->get_pos() + 1;
        if (-1 < select_pos && select_pos < m_items.size()) {
            clear_selection();
            select(select_pos);
            load_frame(select_pos);
        }
        else if (select_pos == m_items.size()) {
            const int pos = 0;
            clear_selection();
            select(pos);
            load_frame(pos);
        }
    }

    void TimelineLayout::set_is_playing(const bool playing){
        m_is_playing = playing;
    }



}// namespace lib_timeline
