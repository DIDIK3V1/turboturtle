/**
 @author Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "imagepaster.h"
#include <QPainter>
#include <QStyleOptionRubberBand>
#include <QStylePainter>
#include <QClipboard>
#include <QMimeData>
#include <QApplication>

namespace lib_canvas {
ImagePaster::ImagePaster(QRubberBand::Shape s, QImage &i, QWidget *p) :
    QRubberBand(s,p)
    , m_img(i)
    , m_begin_offset(0)
    , m_end_offset(0)
{
    //this->resize(m_img.width() + 3, m_img.height() + 3);

}

ImagePaster::ImagePaster(QRubberBand::Shape s, QWidget *p) :
    QRubberBand(s,p)
    , m_img(QImage())
    , m_begin_offset(0)
    , m_end_offset(0)
{

}

void ImagePaster::paintEvent(QPaintEvent *event)
{

    int border_size = 1;
    m_begin_offset = 3*border_size;
    m_end_offset = 0-3*border_size;
    QPainter painter(this);
    //dark border
    painter.setPen(QPen(Qt::black,border_size, Qt::DotLine));
    QRect rect(QPoint(2*border_size , 2*border_size), QPoint(this->width() - 3*border_size, this->height() - 3*border_size));
    painter.drawRect(rect);

    //white border
    QRect contrast_rect(QPoint(border_size , border_size), QPoint(this->width() - 2*border_size, this->height() - 2*border_size));
    painter.setPen(QPen(Qt::gray,border_size, Qt::DotLine));
    painter.drawRect(contrast_rect);

    QRect img_size(QPoint(m_begin_offset ,m_begin_offset), QPoint(this->width() + m_end_offset, this->height() + m_end_offset));
    painter.drawImage(img_size, m_img);
    painter.end();


}

QPair<QPoint, QPoint> ImagePaster::get_offsets() {
    return {{m_begin_offset, m_begin_offset}, {m_end_offset, m_end_offset}};
}


}
