/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Creerio, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef FRAME_H
#define FRAME_H
#include <QImage>
#include "lib_canvas/undoredo.h"

#include <QDebug>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <QDir>
#include <QMessageBox>

namespace lib_frame {
/**
 * @brief The Frame class the storing unit of a frame for TurboTurtle
 */
struct Frame
{
    /**
     * @brief Constructor without argument : Creates a black frame by default
     */
    Frame()
        : image(QImage(1280, 720, QImage::Format_RGBA8888))
        , background(QImage(1280, 720, QImage::Format_RGBA8888))
    {
        image.fill(Qt::black);
        background.fill(Qt::black);
        undoredo = new UndoRedo;
        undoredo->add(image.copy());
        is_modified=true;
        is_modified_composing_bench = false;
    }

    /**
     * @brief Constructor of the class
     * @param color color of the frame
    */
    Frame(Qt::GlobalColor color)
        : image(QImage(1280, 720, QImage::Format_RGBA8888))
        , background(QImage(1280, 720, QImage::Format_RGBA8888))
    {
        image.fill(color);
        background.fill(Qt::white);
        undoredo = new UndoRedo;
        undoredo->add(image.copy());
        is_modified=true;
        is_modified_composing_bench = false;
    }

    /**
     * @brief Constructor of the class
     * @param this is the image of the frame, for this constructor the background is the same as the image
    */
    Frame(QImage img)
        : image(img)
        , background(img)
    {
        image = image.convertToFormat(QImage::Format_RGBA8888);
        background = background.convertToFormat(QImage::Format_RGBA8888);
        undoredo = new UndoRedo;
        undoredo->add(image.copy());
        is_modified=true;
        is_modified_composing_bench = false;
    }

    /**
     * @brief Copy constructor of the class
     * @param this is the frame that will be copied
    */
    Frame(const Frame &frame)
    {
        image = frame.image.copy();
        image = image.convertToFormat(QImage::Format_RGBA8888);
        background = frame.background.copy();
        background = background.convertToFormat(QImage::Format_RGBA8888);
        undoredo = new UndoRedo();
        undoredo->add(image.copy());
        is_modified = true;
        is_modified_composing_bench = false;
    }

    /**
     * @brief Constructor of the class
     * @param this is the image of the frame
     * @param this is the background of the frame
    */
    Frame(QImage front, QImage back)
        : image(front)
        , background(back)
    {
        undoredo = new UndoRedo();
        undoredo->add(image.copy());
        is_modified = true;
        is_modified_composing_bench = false;
    }

    /**
     * @brief Destructor of the class
    */
    ~Frame(){
        delete undoredo;
    }

    /**
     * @brief Gives the thumbnail of the current item
     * @return The image used by the frame.. for now
     */
    const QImage get_thumbnail() const {
        return image;
    }

    /**
     * @brief resize the foreground and background images and their undoredo objects
     * @param target size
    */
    void resize(const QSize &new_size) {
        image = image.scaled(new_size, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
        background = background.scaled(new_size, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
        undoredo->resize(new_size);
    }

    /**
     * @brief Defining the operator = for the class
     * @param frame to set
    */
    Frame operator=(const Frame &frame)
    {
        if (&frame != this){
            this->image = frame.image.copy();
            this->background = frame.background.copy();
            this->undoredo = frame.undoredo;
            is_modified = true;
            is_modified_composing_bench = false;
        }
        return *this;
    }

    /**
     * @brief Defining the operator == for the class
     * @param frame to compare with
    */
    bool operator==(const Frame &frame) const
    {
        return this->is_modified == frame.is_modified
            && this->is_modified_composing_bench == frame.is_modified_composing_bench
            && this->image == frame.image
            && this->background == frame.background
            && this->undoredo == frame.undoredo;
    }

    /**
     * @brief used to convert a qImage to a cv::Mat
     * @return the frame converted
     */
    cv::Mat convertToMat() const
    {
        cv::Mat mat(image.height(),
                image.width(),
                CV_8UC3,
                const_cast<uchar *>(image.bits()),
                static_cast<size_t>(image.bytesPerLine()));
        cv::cvtColor(mat, mat, cv::COLOR_BGR2RGBA);
        return mat;
    }

    /**
     * @brief additive filter between two frames
     * @param  frame to add to
     * @return frame added
     */
    Frame operator+(const Frame &frm)
    {
        cv::Mat frame1 = this->convertToMat();
        cv::Mat frame2 = frm.convertToMat();
        cv::Mat frame3 = frame1 + frame2;
        QImage image(frame3.data,
                frame3.cols,
                frame3.rows,
                static_cast<int>(frame3.step),
                QImage::Format_ARGB32);
        return image;
    }
    /**
     * @brief multiplicative filter between two frames
     * @param frame to multiply to
     * @return frame multiplied to
     */
    Frame operator*(const Frame &frm)
    {
        cv::Mat frame1 = frm.convertToMat();
        cv::Mat frame2 = this->convertToMat();
        cv::Mat frame3 = frame1 * frame2;
        QImage image(frame3.data,
                    frame3.cols,
                    frame3.rows,
                    static_cast<int>(frame3.step),
                    QImage::Format_ARGB32);
        return image;
    }
    /**
     * @brief substractive filter between two frames
     * @param frame to substract to
     * @return frame substracted
     */
    Frame operator-(const Frame &frm)
    {
        cv::Mat frame1 = frm.convertToMat();
        cv::Mat frame2 = this->convertToMat();
        cv::Mat frame3 = frame1 - frame2;
        QImage image(frame3.data,
                frame3.cols,
                frame3.rows,
                static_cast<int>(frame3.step),
                QImage::Format_ARGB32);
        return image;
    }

    /**
     * @brief Saves the frame with an image & background
     * @param path Path to use as a base to save
     * @param id id of the frame
     */
    void save(const QString &path, const size_t &id) const {
        QDir destination_images_dir(path + "/images");
        QDir destination_backgrounds_dir(path + "/backgrounds");

        if(!destination_images_dir.mkpath(".")){
            QMessageBox::warning(nullptr, "Frame saving", "Unable to save the frame to that location. Please chose another.");
        }

        if(!destination_backgrounds_dir.mkpath(".")) {
            QMessageBox::warning(nullptr, "Frame saving", "Unable to save the frame to that location. Please chose another.");
        }

        QString dest_img = destination_images_dir.absolutePath();
        QString dest_bck = destination_backgrounds_dir.absolutePath();

        QString dest_img_curr(dest_img + "/image" + QString::number(id) + ".png");
        QString dest_bck_curr(dest_bck + "/background" + QString::number(id) + ".png");
        image.save(dest_img_curr, "PNG");
        background.save(dest_bck_curr, "PNG");
    }

    /**
     * @brief Saves the frame with an image & background.
     * WARNING : Does not verify if the destination folder exists ! Use this only if you are certain that the folder exists !
     * @param path Path to use as a base to save
     * @param id id of the frame
     */
    void unsafe_save(const QString &path, const size_t &id) const {
        QDir destination_images_dir(path + "/images");
        QDir destination_backgrounds_dir(path + "/backgrounds");

        QString dest_img = destination_images_dir.absolutePath();
        QString dest_bck = destination_backgrounds_dir.absolutePath();

        QString dest_img_curr(dest_img + "/image" + QString::number(id) + ".png");
        QString dest_bck_curr(dest_bck + "/background" + QString::number(id) + ".png");
        image.save(dest_img_curr, "PNG");
        background.save(dest_bck_curr, "PNG");
    }

    /**
     * @brief Front image of the frame
     *
    */
    QImage image;

    /**
     * @brief Background image of the frame
    */
    QImage background;

    /**
     * @brief UndoRedo object of the frame
    */
    UndoRedo * undoredo;

    /**
     * @brief used to know if the frame is modified (to store it)
     */
    bool is_modified;

    /**
     * @brief used to know if the frame is modified (to broadcast it)
     */
    bool is_modified_composing_bench;

    };
} // namespace lib_frame

#endif // FRAME_H
