/**

@author Mattia Basaglia,Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU

@section License

    Copyright (C) 2013-2014 Mattia Basaglia

    This software is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This software is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Color Widgets.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef COLOR_WHEEL_HPP
#define COLOR_WHEEL_HPP

#include <QWidget>
namespace lib_color_selector{
/**
 * \brief Display an analog widget that allows the selection of a HSV color
 *
 * It has an outer wheel to select the Hue and an intenal square to select
 * Saturation and Lightness.
 */
class ColorWheel : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QColor color READ color WRITE set_color NOTIFY color_changed DESIGNABLE true STORED false )
    Q_PROPERTY(qreal hue READ hue WRITE set_hue DESIGNABLE false )
    Q_PROPERTY(qreal saturation READ saturation WRITE set_saturation DESIGNABLE false)
    Q_PROPERTY(qreal value READ value WRITE set_value DESIGNABLE false)
    Q_PROPERTY(unsigned wheel_width READ wheel_width WRITE set_wheel_width DESIGNABLE true)

public:
    explicit ColorWheel(QWidget *parent = 0);
    ~ColorWheel();

    /// Get current color
    QColor color() const;

    QSize size_hint() const;

    /// Get current hue in the range [0-1]
    qreal hue() const;

    /// Get current saturation in the range [0-1]
    qreal saturation() const;

    /// Get current value in the range [0-1]
    qreal value() const;

    /// Get the width in pixels of the outer wheel
    unsigned int wheel_width() const;

    /// Set the width in pixels of the outer wheel
    void set_wheel_width(unsigned int w);

public slots:

    /// Set current color
    void set_color(const QColor &c);

    /**
     * @param h Hue [0-1]
    */
    void set_hue(qreal h);

    /**
     * @param s Saturation [0-1]
    */
    void set_saturation(qreal s);

    /**
     * @param v Value [0-1]
    */
    void set_value(qreal v);

signals:
    /**
     * Emitted when the user selects a color or setColor is called
     */
    void color_changed(QColor);

    /**
     * Emitted when the user selects a color
     */
    void color_selected(QColor);

    /**
     * Emitted when the user drag click over the wheel
     */
    void on_drag();

    /**
     * Emitted when the user release the mouse on the wheel
    */
    void on_click_release();

protected:
    void paintEvent(QPaintEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void resizeEvent(QResizeEvent *);

private:
    class Private;
    Private * const m_p;
};
}
#endif // COLOR_WHEEL_HPP


