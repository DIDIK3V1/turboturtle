/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "canvascursor.h"
#include <QPixmap>
namespace lib_canvas {
CanvasCursor::CanvasCursor(const QCursor &cursor)
{
    m_cursor = cursor;
}

CanvasCursor::CanvasCursor() : m_cursor(QPixmap())
{
}

void CanvasCursor::set_cursor(const QCursor &cursor){
    m_cursor = cursor;
}

QCursor CanvasCursor::get_cursor() const
{
    return m_cursor;
}

void CanvasCursor::update_cursor(const lib_drawing_tools::DrawingTool &tool)
{
    m_tool = tool;
    switch(tool){
    case lib_drawing_tools::ERASER:
        eraser_cursor();
        break;
    case lib_drawing_tools::FILLBUCKET:
        bucket_cursor();
        break;
    case lib_drawing_tools::COLORPICKER:
        picker_cursor();
        break;
    case lib_drawing_tools::RECTANGLE_SELECTOR:
    case lib_drawing_tools::POLYGONAL_SELECTOR:
        rectangle_selector_cursor();
        break;
    case lib_drawing_tools::HAND:
        hand_cursor();
        break;
    default:
        break;
    }
}

void CanvasCursor::update_thickness(int thickness){
    m_thickness = thickness;
    update_cursor(m_tool);
}

void CanvasCursor::eraser_cursor(){
    QImage cursor = QImage(m_thickness , m_thickness , QImage::Format_ARGB32);
    cursor.fill(Qt::transparent);

    QColor black = Qt::black;

    for (int i = 0; i < m_thickness; ++i) {
        cursor.setPixel(i , 0 , black.rgb());
        cursor.setPixel(i , m_thickness - 1 , black.rgb());
        cursor.setPixel(0 , i , black.rgb());
        cursor.setPixel(m_thickness -1 , i , black.rgb());
    }

    set_cursor(QCursor(QPixmap::fromImage(cursor)));
}

void CanvasCursor::picker_cursor(){
    QImage cursor = QImage(":/icon/pipette");

    cursor = cursor.scaled(32 , 32);
    set_cursor(QCursor(QPixmap::fromImage(cursor) , 0 , 32));
}

void CanvasCursor::bucket_cursor(){
    QImage cursor = QImage(":/icon/paintpot");

    cursor = cursor.scaled(32 , 32);
    set_cursor(QCursor(QPixmap::fromImage(cursor) , 30 , 26));
}

void CanvasCursor::rectangle_selector_cursor()
{
    QImage cursor = QImage(":/icon/selection_cross");

    cursor = cursor.scaled(24,24);
    set_cursor(QCursor(QPixmap::fromImage(cursor) , 12 , 12));
}

void CanvasCursor::hand_cursor()
{
    QImage cursor = QImage(":/icon/selection_cross");

    cursor = cursor.scaled(24,24);
    set_cursor(QCursor(QPixmap::fromImage(cursor) , 12 , 12));
}


} // namespace lib_canvas
