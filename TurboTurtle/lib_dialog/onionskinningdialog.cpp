/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "onionskinningdialog.h"
#include "ui_onionskinningdialog.h"
#include <QDebug>

namespace lib_dialog {

OnionSkinningDialog::OnionSkinningDialog(QWidget *parent)
    : QDialog(parent),
    m_ui(new Ui::OnionSkinningDialog)
{
    m_ui->setupUi(this);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    this->setWindowTitle("Onion Skinning Settings");

    m_ui->slider_frame_before->setRange(m_min_value, m_max_value);
    m_ui->slider_frame_before->setSliderPosition(m_frame_before);

    m_ui->spinbox_frame_before->setRange(m_min_value, m_max_value);
    m_ui->spinbox_frame_before->setValue(m_frame_before);

    m_ui->slider_frame_after->setRange(m_min_value, m_max_value);
    m_ui->slider_frame_after->setSliderPosition(m_frame_after);

    m_ui->spinbox_frame_after->setRange(m_min_value, m_max_value);
    m_ui->spinbox_frame_after->setValue(m_frame_after);

    // Connect spinbox to sliders
    connect(m_ui->spinbox_frame_before, &QSpinBox::valueChanged, m_ui->slider_frame_before, [this](const int val){
        if (m_ui->slider_frame_before->value() != val)
            m_ui->slider_frame_before->setValue(val);
    });
    connect(m_ui->spinbox_frame_after, &QSpinBox::valueChanged, m_ui->slider_frame_after, [this](const int val){
        if (m_ui->slider_frame_after->value() != val)
            m_ui->slider_frame_after->setValue(val);
    });

    // If value is changed, update immediately the timeline
    connect(m_ui->spinbox_frame_before, &QSpinBox::valueChanged, this, [this]([[maybe_unused]]const int val){
        updateValues();
    });
    connect(m_ui->spinbox_frame_after, &QSpinBox::valueChanged, this, [this]([[maybe_unused]]const int val){
        updateValues();
    });
}

OnionSkinningDialog::~OnionSkinningDialog()
{
    delete m_ui;
}

void OnionSkinningDialog::setValues(const int before, const int after) {
    if (before != -1 && after != -1) {
        m_frame_before = before;
        m_frame_after = after;
    }

    m_ui->spinbox_frame_before->setValue(int(m_frame_before));
    m_ui->spinbox_frame_after->setValue(int(m_frame_after));
}

void OnionSkinningDialog::updateValues() {
    emit value_changed(m_ui->spinbox_frame_before->value(), m_ui->spinbox_frame_after->value());
}

void OnionSkinningDialog::on_buttonBox_rejected()
{
    // Rejected, reset values to what they were before
    emit value_changed(m_frame_before, m_frame_after);
}


} // namespace lib_dialog
