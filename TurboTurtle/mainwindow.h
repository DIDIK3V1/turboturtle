/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QWidget>
#include <thread>
#include "lib_canvas/canvas.h"

#include "lib_library/animationlibrary.h"
#include "lib_timeline/frametimeline.h"
#include <opencv2/opencv.hpp>
#include <QDesktopServices>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Constructor of the main window, called by the main.cpp
     * @param parent parent of the class
     */
    MainWindow(QWidget *parent = nullptr);

    /**
     * @brief destructor of the class
     */
    ~MainWindow();

    /**
     * @brief function that sets the first picture of the software
     */
    void set_first_picture();

    /**
         * @brief enable or disable the live drawing button
         */
    void activate_desactivate_live_drawing_button();



private slots:

    /**
     * @brief Slot called when the camera button is clicked
     */
    void on_but_startCamera_clicked();

    /**
     * @brief update the widow UI
     */
    void update_window();

    /**
     * @brief get the actual frame of the canvas and send it
     */
    void get_canvas_frame();

    /**
     * @brief use to update ui after the open of the camera
     */
    void start_update_camera();

    /**
     * @brief Slot telling the library to call its methods to load an animation
     */
    void on_actionImport_folder_triggered();

    /**
     * @brief lock_ui lock all unlock all the UI components so the user can't use them (during loading for example)
     * @param state new state of the ui
     */
    void lock_ui(const bool state);

    /**
     * @brief on_action_png_triggered call the library to save animations in png
     */
    void on_action_png_triggered();

    /**
     * @brief on_action_gif_triggered call the library to import a gif
     */
    void on_actionImport_GIF_triggered();

    /**
     * @brief on_action_png_triggered call the library to save animations in gif
     */
    void on_action_gif_triggered();

    /**
     * @brief on_actionSave_triggered Tries to save current session to a set file, or calls on_actionSave_As_triggered if no file set
     */
    void on_actionSave_triggered();

    /**
     * @brief on_actionSave_As_triggered call the library to save the current session
     */
    void on_actionSave_As_triggered();

    /**
     * @brief on_actionOpen_Session_triggered Call the library to load a session
     */
    void on_actionOpen_Session_triggered();

    /**
     * @brief MainWindow::on_actionQuit_triggered
     */
    void on_actionQuit_triggered();

    /**
     * @brief on_actionAbout_triggered
     */
    void on_actionAbout_triggered();

    /**
     * @brief on_actionAboutQt_triggered
     */
    void on_actionAbout_Qt_triggered();

    /**
     * @brief on_actionOnion_Skinning_triggered : triggered the onion skinning settings window
     */

    void on_actionReverse_triggered();

    void on_actionMirror_triggered();

    void on_actionOnion_Skinning_triggered();

    void on_actionNDI_triggered();

    void on_actionDocumentation_triggered();

    void on_actionDocumentation_en_ligne_triggered();

    void on_actionTh_me_clair_triggered();

    void on_actionTh_me_sombre_triggered();

    void on_actionSystem_theme_2_triggered();

    void on_butRectShape_clicked();

    void on_butPolyShape_clicked();

    /**
     * @brief snap Informs the canvas to store the picture in the frame
     */
    void snap();

    /**
     * @brief snap Informs the canvas to store the picture in the frame & create a new frame
     */
    void snap_and_new_frame();

    void on_actionSnap_triggered();

    void on_actionSnap_and_New_Frame_triggered();

    void on_actionCamera_triggered();

    void on_actionUndo_triggered();

    void on_actionRedo_triggered();

    void on_actionCut_triggered();

    void on_actionCopy_triggered();

    void on_actionPaste_triggered();

public slots :

    /**
     * @brief set the id of the camera choosen
     * @param val value of the camera
     */
    void camera_chosen(int val);

    /**
         * @brief change the text of the button to start or stop
         */
    void on_butStartStopLive_clicked();

    /**
         * @brief change the text of the button depending on the state of the camera
         * @param state state of the camera
         */
    void set_text_ButCamera(bool state);

protected :
    /**
     * @brief keyReleaseEvent Called when the user presses a key
     * @param event event
     */
    void keyPressEvent(QKeyEvent *event);
    /**
     * @brief keyReleaseEvent Called when the user releases a key
     * @param event event
     */
    void keyReleaseEvent(QKeyEvent *event);
    /**
     * @brief closeEvent Called when the user tries to exit the software
     * @param event event
     */
    void closeEvent(QCloseEvent *event);


private:

    /**
     * @brief m_ui ui of the application
    */
    Ui::MainWindow *m_ui;

    /**
     * @brief m_is_camera boolean to know if the camera is on
    */
    bool m_is_camera;

    /**
     * @brief m_is_live_broadcast boolean to know if the live broadcast is on
    */
    bool m_is_live_broadcast;

    /**
     * @brief m_is_live_drawing boolean to know if the live drawing is on
    */
    bool m_is_live_drawing;

    /**
     * @brief m_is_looping boolean to know if the animation is looping
    */
    bool m_is_looping;

    /**
     * @brief m_ui_is_lock boolean to know if the ui is lock (when an animation is loading = progressBar)
    */
    bool m_ui_is_lock;

    /**
     * @brief m_is_living
     */
    bool m_is_living;

    /**
     * @brief m_is_camera_on
     */
    bool m_is_camera_on;

    /**
     * @brief m_timer timer to update the camera
     */
    QTimer *m_timer;

    /**
     * @brief m_frame frame of the camera
     */
    cv::Mat m_frame;

    /**
     * @brief m_cap capture of the camera
     */
    cv::VideoCapture m_cap;

    /**
     * @brief m_canvas canvas of the application
     */
    lib_canvas::Canvas *m_canvas;

    /**
     * @brief timeline of the application
     */
    lib_timeline::FrameTimeline *m_timeline;

    /**
     * @brief m_live_timer timer to update the live broadcast
     */
    QTimer *m_live_timer;

    /**
     * @brief m_pos_anim position of the frame in the animation, use to play the animation in the broadcast
     */
    size_t m_pos_anim;

    /**
     * @brief m_animation vector of the frames of the animation, use to play the animation in the broadcast
     */
    std::vector<QImage> m_animation;

    /**
     * @brief m_device_id_camera id of the camera
     */
    short m_device_id_camera;

    /**
     * @brief m_blackscreen blackscreen to display when the camera is not open
     */
    QImage m_blackscreen;

    /**
     * @brief m_library library of the application
     */
    lib_library::AnimationLibrary *m_library;

    /**
     * @brief m_thread_cam thread the open and close of the camera
     */
    std::thread m_thread_cam;

    /**
     * @brief save_path Path to the currently saved session
     */
    QString m_save_path = QString();

    /**
     * @brief open the camera
     */
    void open_camera_thread();

    /**
     * @brief close the camera
     */
    void close_camera_thread();

    bool m_is_camera_horizontal_mirror;

    bool m_is_camera_vertical_mirror;

signals:

    /**
     * @brief signal to undo the last action on the canvas (CTRL+Z)
    */
    void undo_signal();

    /**
     * @brief signal to redo the last action on the canvas (CTRL+Y)
    */
    void redo_signal();

    /**
     * @brief signal to copy the frame(s) in the timeline
    */
    void ctrl_c();

    /**
     * @brief signal to know the camera state in canvas and timeline
     * @param state actual state of the camera
    */
    void updateCameraClicked(bool state);

    /**
     * @brief signal to send the actual frame of the canvas
     * @param frame frame to be sent
     */
    void send_frame_live_drawing(lib_frame::Frame &frame);

    /**
     * @brief signal to request start_update_camera() method in the main thread
     */
    void signal_update_camera();

    /**
     * @brief signal to paste the frame(s) in the timeline
     */
    void ctrl_v();

    /**
     * @brief signal to activate the color picker
     */
    void shortcut_l();

    /**
     * @brief signal to activate the paint brush
     */
    void shortcut_b();

    /**
     * @brief signal to activate the pencil
     */
    void shortcut_n();

    /**
     * @brief signal to activate the marker
     */
    void shortcut_p();

    /**
     * @brief signal to activate the bucket
     */
    void shortcut_g();

    /**
     * @brief signal to activate the eraser
     */
    void shortcut_e();

    /**
     * @brief signal to play the sequence
     */
    void play_animation();

    /**
     * @brief signal to start broadcast
     */
    void shortcut_3();

    /**
     * @brief signal to start live drawing
     */
    void shortcut_5();

    /**
     * @brief signal to duplicate selected frames
     */
    void shortcut_7();
    /**
     * @brief signal to delete selected frames in the timeline
     */
    void shortcut_9();

    /**
     * @brief signal to add a new frame
     */
    void shortcut_insert();

    /**
     * @brief signal to decrease the thickness
     * @param dec value to decrement the slider value
     */
    void shortcut_comma(unsigned dec);

    /**
     * @brief signal to increase the thickness
     * @param inc value to increment the slider value
     */
    void shortcut_semicolon(unsigned inc);

    /**
     * @brief signal to decrease the alpha
     * @param dec value to decrement the slider value
     */
    void shortcut_less(unsigned dec);

    /**
     * @brief signal to increase the alpha
     * @param inc value to increment the slider value
     */
    void shortcut_greater(unsigned inc);

    /**
     * @brief signal to delete the current frame (delete key)
     */
    void delete_frame();

    /**
     * @brief signal to go to the previous frame in the timeline (left arrow)
    */
    void prev_frame_in_timeline();

    /**
     * @brief signal to go to the next frame in the timeline (right arrow)
    */
    void next_frame_in_timeline();

    /**
     * @brief  signal to send the actual state of the camera to the timeline
     * @param  state actual state of the camera
     */
    void send_to_timeline_camera_state(bool state);

    /**
     * @brief send the pos of the new animation
     */
    void new_animation_pos();

    /**
         *
         * @brief signal to emit when a brush is changed, send the brush texture
        */
    void live_drawing(bool);

    void snap_camera();


};
#endif // MAINWINDOW_H
