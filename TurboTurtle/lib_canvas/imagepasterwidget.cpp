/**
 @author Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "imagepasterwidget.h"
#include <QApplication>

namespace lib_canvas {
ImagePasterWidget::ImagePasterWidget(const QImage &i, QWidget *parent)
    : QWidget{parent}
    , m_rubberband(nullptr)
    , m_img(i)
    , m_original_position(QPoint())
    , m_aspect_ratio((float)m_img.width() / (float)m_img.height())
    , m_keep_aspect_ratio(false)
    , m_layout(new QGridLayout(this))
{
    m_original_position = this->pos();
    //tell QSizeGrip to resize this widget instead of top-level window
    setMouseTracking(true);
    setWindowFlags(Qt::SubWindow);
    m_layout->setContentsMargins(0, 0, 0, 0);

    //QSizeGrips to resize the widget
    QSizeGrip* grip_1 = new QSizeGrip(this);
    QSizeGrip* grip_2 = new QSizeGrip(this);
    QSizeGrip* grip_3 = new QSizeGrip(this);
    QSizeGrip* grip_4 = new QSizeGrip(this);
    m_layout->addWidget(grip_1, 1, 0, Qt::AlignLeft | Qt::AlignBottom);
    m_layout->addWidget(grip_2, 1, 1, Qt::AlignRight | Qt::AlignBottom);
    m_layout->addWidget(grip_3, 0, 0, Qt::AlignLeft | Qt::AlignTop);
    m_layout->addWidget(grip_4, 0, 1, Qt::AlignRight | Qt::AlignTop);
    m_rubberband = new ImagePaster(QRubberBand::Rectangle, m_img, this);
    m_rubberband->move(0, 0);
    m_rubberband->show();
    show();
}

ImagePasterWidget::~ImagePasterWidget() {
    if(m_rubberband)
        delete m_rubberband;
    if(m_layout)
        delete m_layout;
}

void ImagePasterWidget::resizeEvent(QResizeEvent *e) {
    m_rubberband->resize(size());
    if(m_keep_aspect_ratio) {
        resize(size().width(), size().width() / m_aspect_ratio);
        m_rubberband->resize(size());
        this->move(m_original_position.x(), m_original_position.y());
    }

}

void ImagePasterWidget::enterEvent(QEnterEvent *) {
    //reset the cursor up to the default cursor
    while(qApp->overrideCursor())
        qApp->restoreOverrideCursor();
    m_original_position = this->pos();
}

void ImagePasterWidget::keep_aspect_ratio(bool keep_aspect) {
    m_keep_aspect_ratio = keep_aspect;
}

void ImagePasterWidget::mouseReleaseEvent(QMouseEvent *e) {
    m_original_position = this->pos();
}

QImage ImagePasterWidget::get_image() {
    return m_img;
}

QPair<QPoint, QPoint> ImagePasterWidget::get_image_offsets() {
    return m_rubberband->get_offsets();
}


}

