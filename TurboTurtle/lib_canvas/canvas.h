/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Creerio, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CANVAS_H
#define CANVAS_H

#include <QMenu>
#include <QPainter>
#include <QWidget>
#include <QMutex>
#include "drawingtools/brush.h"
#include "lib_canvas/canvascursor.h"
#include "lib_canvas/frame.h"
#include "lib_canvas/painter.h"
#include "qframe.h"
#include <QRubberBand>
#include "lib_canvas/imagepasterwidget.h"
#include <QPolygon>

namespace Ui {
class Canvas;
}
namespace lib_canvas {

/**
 * @brief The Canvas class, used to draw with brushes, also contains camera pictures when needed (Heart of the software)
 */

class Canvas : public QFrame
{
    Q_OBJECT

public:

    /**
     * @brief mutex shared with ndimanager to manage frame access
     */
    static QMutex m_mutexImg;
    /**
     * @brief Constructor of the class
    */
    explicit Canvas(QWidget *parent = nullptr);

    /**
     * @brief Destructor of the class
    */
    ~Canvas();

    inline static QSize s_canvas_size;

    /**
     * @brief Pastes the content of the system clipboard
    */
    void paste_clipboard();

    /**
     * @brief Tells the ImagePaster to keep the aspect ratio while resizing
    */
    void resize_keep_aspect_ratio(bool);


    /**
     * @brief getter for the state of rectangle selection
     * @return the state of the selection
     */
    bool get_is_rectangle_area_selected();

    /**
     * @brief getter for the state of polygonal selection
     * @return the state of the selection
     */
    bool get_is_polygon_area_selected();

    /**
     * @brief erases the image within the selected rectangle
     */
    void delete_selection();

    /**
     * @brief set the image of the canvas
     * @param the image to set
     */
    void set_image(const QImage &img);

    /**
     * @brief set the camera frame image
     * @param the image to set
     */
    void set_camera_image(const QImage &img);

    /**
     * @brief set the background of the canvas
     * @param the image to set
    */
    void set_background(const QImage &img);

    /**
     * @brief set the camera frame background
     * @param the image to set
     */
    void set_camera_background(const QImage &img);

    /**
     * @brief get the image of the canvas
     * @return the image of the canvas
    */
    QImage get_image();

    /**
     * @brief get the frame of the canvas
     * @return the frame of the canvas
    */
    lib_frame::Frame *get_frame();

    /**
     * @brief set the frame of the canvas
     * @param the hidden frame to set :  used to compute onion skinning
     * @param the displayed frame to set : used to store onion skinning
    */
    void set_frame(lib_frame::Frame *hidden , lib_frame::Frame *displayed);

    /**
     * @brief deselects the current rectangle_selection and hide the selection rectangle
     * @param boolean indicating if the deselection is a consequence of a new frame being loaded
    */
    void deselect_selection(bool is_frame_change = false);

    /**
     * @brief copies the current rectangle selection as an image in the clipboard
    */
    void copy_rectangle_selection();

    /**
     * @brief copies the current polygonal selection as an image in the clipboard
    */
    void copy_polygonal_selection();

    /**
     * @brief copies the current selection in the clipboard and erases the selected area
    */
    void cut_rectangle_selection();

    /**
     * @brief copies the current polygonal selection in the clipboard and erases the selected area
    */
    void cut_polygonal_selection();


protected:

    void resizeEvent(QResizeEvent *event) override;
    /**
     * @brief mousePressEvent of the canvas, this is used to draw
    */
    void mousePressEvent(QMouseEvent *event) override;

    /**
     * @brief mouseMoveEvent of the canvas, this is used to draw
    */
    void mouseMoveEvent(QMouseEvent *event) override;

    /**
     * @brief mouseReleaseEvent of the canvas, this is used to draw
    */
    void mouseReleaseEvent(QMouseEvent *event) override;

    /**
     * @brief paintEvent of the canvas, this is used to draw
    */
    void paintEvent(QPaintEvent *event) override;

    /**
     * @brief enterEvent of the canvas, this is used to set the mouse cursor
    */
    void enterEvent(QEnterEvent *) override;

    /**
     * @brief leaveEvent of the canvas, this is used to set the mouse cursor
    */
    void leaveEvent(QEvent *) override;

    /**
     * @brief tabletEvent, tabletEvent of the canvas, this is used to draw
    */
    void tabletEvent(QTabletEvent *event) override;

private:

    /**
     * @brief Constant storing the minimum set size of a polygon
     */
    static const unsigned short K_MINIMUM_POLYGON_SIZE = 10;

    /**
     * @brief Computes the surface area of a given polygon
     * @param Polygon from which we want the area
     * @return Surface area of the polygon (absolute value)
     */
    double polygon_area(const QPolygon& polygon);

    /**
     * @brief Resizes the displayed and hidden frames stored in the canva
    */
    void resize_current_frame();

    /**
     * @brief QPoint storing the QRubberband offset while moving it around
    */
    QPoint m_rubberband_offset;

    /**
     * @brief QPoint storing the ImagePaster offset while moving it around
    */
    QPoint m_imagepaster_offset;

    /**
     * @brief ImagePasterWidget used to paste the image stored in the clipboard
    */
    ImagePasterWidget *m_imagepaster;

    /**
     * @brief bool if the QRubberband is being moved
    */
    bool m_is_move_rubberband;

    /**
     * @brief bool if the ImagePasterWidget is being moved
    */
    bool m_is_move_imagepaster;

    /**
     * @brief bool if the ImagePasterWidget is currently in use
    */
    bool m_is_pasting;

    /**
     * @brief Prints onto the canvas the image being pasted
    */
    void print_pasted_image();

    /**
     * @brief normalizes the selection rectangle coordinates so they fit the canvas
    */
    void normalize_rubberband_position();

    /**
     * @brief Pointer to the rectangle selector
    */
    QRubberBand *m_rubberband;

    /**
     * @brief List containing all points defining the polygonal selection
     */
    QPolygon m_polygon;

    /**
     * @brief m_is_polygon_selected
     */
    bool m_is_polygon_selected;

    /**
     * @brief Start coordinates of the selection rectangle
    */
    QPoint m_rectangle_selector_origin;

    /**
     * @brief End coordinates of the selection rectangle
    */
    QPoint m_rectangle_selector_end;

    /**
     * @brief Start coordinates of the image paster
    */
    QPoint m_imagepaster_origin;

    /**
     * @brief End coordinates of the image paster
    */
    QPoint m_imagepaster_end;

    /**
     * @brief Indicates whether an area of the canva is being selected
    */
    bool m_is_area_selected;

    /**
     * @brief Pointer to the ui of the canvas
    */
    Ui::Canvas *ui;

    /**
     * @brief Pointer to the hidden frame of the canvas
    */
    lib_frame::Frame *m_frame_hidden;

    /**
     * @brief Pointer to the displayed frame of the canvas
     */
    lib_frame::Frame *m_frame_displayed;

    /**
     * @brief Pointer to the hidden frame storing the camera capture
     */
    lib_frame::Frame *m_frame_camera_hidden;

    /**
     * @brief Pointer to the displayed frame storing the camera capture
     */
    lib_frame::Frame *m_frame_camera_displayed;

    /**
     * @brief Pointer to the painter of the canvas, the painter is used to draw
    */
    Painter *m_painter;

    /**
     * @brief Pointer to the cursor of the canvas
    */
    CanvasCursor m_cursor;

    /**
     * @brief QPoint of the last point of the drawing, used to draw the real frame
    */
    QPoint m_last_point_hidden;

    /**
     * @brief QPoint of the last point of the drawing, used to draw onion skinning
    */
    QPoint m_last_point_displayed;

    /**
     * @brief the current brush
    */
    lib_drawing_tools::Brush m_brush;

    /**
     * @brief the current drawing tool
    */
    lib_drawing_tools::DrawingTool m_tool = lib_drawing_tools::BRUSH;

    /**
     * @brief the tool used before pasting and image
     * used to restore the tool after pasting
     */
    lib_drawing_tools::DrawingTool m_tool_before_pasting;

    /**
     * @brief boolean to know if the mouse is pressed on the canvas
    */
    bool m_is_drawing = false;

    /**
     * @brief boolean to know if the tablet stylus is down
    */
    bool m_deviceDown = false;

    /**
     * @brief boolean to know if the tablet is used
    */
    bool m_is_tablet = false;

    /**
     * @brief boolean used to set the borderColor;
     */
    bool m_is_camera;

    /**
     * @brief boolean used to set the flip of the camera;
     */
    bool m_camera_reversed;

    /**
     * @brief boolean used to activate/deactivate the onion skinning
     */
    bool m_onion_skinning_on;

public slots:
    /**
     * @brief Clear the canvas after the cam is turned off
     */
    void clear_image();

    /**
     * @brief slot reacting to a change color
     * @param newColor new color chosen
     */
    void color_changed(const QColor &newColor);

    /**
     * @brief slot reacting to a change thickness
     * @param new width of the brush
     */
    void thickness_change(int newWidth);

    /**
     * @brief on_brush_changed, slot reacting to a change brush
     * @param brush to change
     */
    void brush_changed(const lib_drawing_tools::Brush &newBrush);

    /**
     * @brief on_tooled_changed, slot reacting to a change tool
     * @param tool to change
     */
    void tool_changed(const lib_drawing_tools::DrawingTool &newTool);

    /**
     * @brief slot reacting to a undo
    */
    void on_undo();

    /**
     * @brief slot reacting to a redo
    */
    void on_redo();

    /**
     * @brief update function is called when the canvas is updated
    */
    void update_canvas();

    /**
     * @brief update function is called when the cursor is updated
    */
    void update_cursor();

    /**
     * @brief update when camera is activated to show red border
     * @param state of the camrea
     */
    void set_camera_state(bool state);

    /**
     * @brief activate/deactivate onion skinning
     * @param state of the onion skinning
     */
    void onion_skinning_state(bool state);

    /**
     * @brief copy the camera image and background into the canva
     * non reversible action
     */
    void get_snaped();

signals:
    /**
     * @brief Signal send to timeline to send the new frame to the timeline, and to update the timeline
    */
    void update_frame_button(lib_frame::Frame *frame, const bool reloadFrame = false);

    /**
     * @brief Signal send to ColorSelector to update the color
    */
    void update_color_select(QColor);

    /**
     * @brief Signal send to Timeline when the frame is created, timeline will add the frame in the timeline
    */
    void send_to_timeline(lib_frame::Frame *);

    /**
     * @brief Signal send to Timeline when the camera is flipped
    */
    void camera_reversed(bool);
};
} // namespace lib_canvas
#endif // CANVAS_H
