/**
 @author Creerio

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "lib_timeline/widgets/framebutton.h"
#include <QMenu>

namespace lib_timeline {
    class FrameMenu : public QMenu
    {
    public:
        explicit FrameMenu(FrameButton *parent = nullptr);
        ~FrameMenu();

    private:
        /**
         * @brief FrameButton calling for the menu
         */
        FrameButton *m_btn;

        /**
         * @brief Layout associated
         */
        TimelineLayout *m_layout;

        /**
         * @brief Button selected in the m_layout
         * Used to reselect the button when this menu is closed
         */
        const FrameButton *m_loaded;

        /**
         * @brief Used to insert a new frame before the selected FrameButton
         */
        void insert_before();

        /**
         * @brief Used to insert a new frame after the selected FrameButton
         */
        void insert_after();

        /**
         * @brief Used to duplicate the selected frame
         */
        void duplicate();

        /**
         * @brief Used to remove the selected frame
         */
        void remove();

        /**
         * @brief Used to load the selected frame
         */
        void load();

        /**
         * @brief Used to save the selected frame as a png
         */
        void save_png();
    };
}
