/**

 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "newanimationframe.h"
#include "ui_newanimationframe.h"
namespace lib_drawing_tools {
NewAnimationFrame::NewAnimationFrame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NewAnimationFrame)
{
    ui->setupUi(this);
}

NewAnimationFrame::~NewAnimationFrame()
{
    delete ui;
}

void NewAnimationFrame::on_butAddFrame_clicked()
{
    emit add_frame();
}




void NewAnimationFrame::on_butNewAnimation_clicked()
{
    emit new_animation();
}
} // namespace lib_drawing_tools
