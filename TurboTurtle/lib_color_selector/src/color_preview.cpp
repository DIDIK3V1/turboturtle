/**

@author Mattia Basaglia

@section License

    Copyright (C) 2013-2014 Mattia Basaglia
    Copyright (C) 2014 Calle Laakkonen

    This file is part of TurboTurtle.

    TurboTurtle is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TurboTurtle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TurboTurtle.  If not, see http://www.gnu.org/licenses/.

*/

#include "../include/color_preview.hpp"

#include <QStylePainter>
#include <QStyleOptionFrame>
#include "../include/paint_border.hpp"
#include <QMouseEvent>
#include <QDrag>
#include <QMimeData>
namespace lib_color_selector{
class ColorPreview::Private
{
public:
    QColor col; ///< color to be viewed
    QColor comparison; ///< comparison color
    QBrush back;///< Background brush, visible on transaprent color
    DisplayMode display_mode; ///< How the color(s) are to be shown

    Private() : col(Qt::red), back(Qt::darkGray, Qt::DiagCrossPattern), display_mode(AllAlpha)
    {}
};

ColorPreview::ColorPreview(QWidget *parent) :
    QWidget(parent), m_p(new Private)
{
    m_p->back.setTexture(QPixmap(QLatin1String(":/alphaback.png")));
}

ColorPreview::~ColorPreview()
{
    delete m_p;
}

void ColorPreview::set_background(const QBrush &bk)
{
    m_p->back = bk;
    update();
}

QBrush ColorPreview::background() const
{
    return m_p->back;
}

ColorPreview::DisplayMode ColorPreview::display_mode() const
{
    return m_p->display_mode;
}

void ColorPreview::set_display_mode(const DisplayMode &m)
{
    m_p->display_mode = m;
    update();
}

QColor ColorPreview::color() const
{
    return m_p->col;
}

QColor ColorPreview::comparison_color() const
{
    return m_p->comparison;
}

QSize ColorPreview::size_hint() const
{
    return QSize(24,24);
}

void ColorPreview::paint(QPainter &painter, QRect rect) const
{
    QColor c1, c2;
    switch(m_p->display_mode) {
    case NoAlpha:
        c1 = c2 = m_p->col.rgb();
        break;
    case AllAlpha:
        c1 = c2 = m_p->col;
        break;
    case SplitAlpha:
        c1 = m_p->col.rgb();
        c2 = m_p->col;
        break;
    case SplitColor:
        c1 = m_p->comparison;
        c2 = m_p->col;
        break;
    }

    if(c1.alpha()<255 || c2.alpha()<255)
        painter.fillRect(1, 1, rect.width()-2, rect.height()-2, m_p->back);

    int w = (rect.width() - 2) / 2;
    int h = rect.height() - 2;
    painter.fillRect(1, 1, w, h, c1);
    painter.fillRect(1+w, 1, w, h, c2);

    paint_tl_border(painter,size(),palette().color(QPalette::Mid),0);
    paint_tl_border(painter,size(),palette().color(QPalette::Dark),1);

    paint_br_border(painter,size(),palette().color(QPalette::Midlight),1);
    paint_br_border(painter,size(),palette().color(QPalette::Button),0);
}

void ColorPreview::set_color(const QColor &c)
{
    m_p->col = c;
    update();
    emit color_changed(c);
}

void ColorPreview::set_comparison_color(const QColor &c)
{
    m_p->comparison = c;
    update();
}

void ColorPreview::paintEvent(QPaintEvent *)
{
    QStylePainter painter(this);
    paint(painter, geometry());
}

void ColorPreview::resizeEvent(QResizeEvent *)
{
    update();
}

void ColorPreview::mouseReleaseEvent(QMouseEvent * ev)
{
    if ( QRect(QPoint(0,0),size()).contains(ev->pos()) )
        emit clicked();
}

void ColorPreview::mouseMoveEvent(QMouseEvent *ev)
{

    if ( ev->buttons() &Qt::LeftButton && !QRect(QPoint(0,0),size()).contains(ev->pos()) )
    {
        QMimeData *data = new QMimeData;

        data->setColorData(m_p->col);

        QDrag* drag = new QDrag(this);
        drag->setMimeData(data);

        QPixmap preview(24,24);
        preview.fill(m_p->col);
        drag->setPixmap(preview);

        drag->exec();
    }
}


}
