/**
 @authors Creerio, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "animationbutton.h"
#include "lib_library/widgets/animationmenu.h"
#include <QMenu>

namespace lib_library {
    AnimationButton::AnimationButton(lib_animation::Animation *item, const QString &mime_type, const int pos, QWidget *parent) :
    TemplatedImageButton(item, mime_type, pos, parent),
    layout(std::bit_cast<AnimationLayout *>(parent))
    {
        QObject::connect(this,
                        &AnimationButton::on_right_click,
                        this,
                        [this] (const QPointF mouse_pos, [[maybe_unused]] const int elem_pos) {
                            AnimationMenu menu(this);
                            if (!layout->is_only_selected(this->get_pos()))
                                layout->click_select(this->get_pos());

                            // Load menu, action is covered by menu
                            menu.exec(mouse_pos.toPoint());
                        });
    }
}
