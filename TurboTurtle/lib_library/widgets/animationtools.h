/**
 @author Creerio

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "animationbutton.h"
#include "lib_widgets/templates/templatedhlayouttools.h"

namespace lib_library {
    /**
     * @brief Tools for animation m_layout, adds the add button functionality
     */
    class AnimationTools
            : public lib_widgets::TemplatedHLayoutTools<lib_library::AnimationButton, lib_animation::Animation> {
    public:
        /**
         * @brief Default constructor of the tools
         * @param parent Parent QWidget
         */
        explicit AnimationTools(QWidget *parent = nullptr);

        void add();
    };
}
