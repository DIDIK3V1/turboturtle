/**
 @authors Creerio, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "lib_widgets/templates/templatedimagebutton.h"
#include "lib_animation/animation.h"

namespace lib_library {

    // 'Virtual' class
    class AnimationLayout;

    /**
     * @brief Class representing a button containing an animation
     */
    class AnimationButton : public lib_widgets::TemplatedImageButton<lib_animation::Animation> {
    public:
        /**
         * @brief Default constructor for an AnimationButton
         * @param item Animation to store
         * @param pos Position of the button in the vector
         * @param parent Parent QWidget
         */
        explicit AnimationButton(lib_animation::Animation *item, const QString &mime_type = "", const int pos = -1, QWidget *parent = nullptr);

        const std::string get_name() const final {
            return "AnimationButton";
        }

        /**
         * @brief Animation m_layout containing the button calling for the menu
         * Used to insert/delete elements
         */
        AnimationLayout *layout;
    };
}
