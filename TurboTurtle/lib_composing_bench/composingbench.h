/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef COMPOSINGBENCH_H
#define COMPOSINGBENCH_H

#include <QWidget>
#include "lib_broadcast/broadcaster.h"
#include "lib_canvas/frame.h"
#include "lib_composing_bench/broadcastbutton.h"
#include <optional>

namespace lib_composing_bench {

namespace Ui {
class ComposingBench;
}

class ComposingBench : public QWidget
{
    Q_OBJECT

public:
    explicit ComposingBench(QWidget *parent = nullptr);
    ~ComposingBench();

    /**
     * @brief  used to color the indicator
     * @param painter  painter to color
     * @param rect area to paint
     */
    void paint(QPainter &painter, const QRect &rect) const;

    std::optional<std::vector<lib_animation::Animation*>> get_animation_in_bench();

protected:
    /**
    * @brief event called when painting the widget
    * @param e
    */
    void paintEvent(QPaintEvent *e) override;
    /**
    * @brief event called when mouse is entering and dragging an element
    * @param event
    */
    void dragEnterEvent(QDragEnterEvent *event) override;
    /**
    * @brief event called when mouse is leaving and dragging an element
    * @param event
    */
    void dragLeaveEvent(QDragLeaveEvent *event) override;
    /**
    * @brief dropEvent : event called when frame is dropped
    * @param event
    */
    void dropEvent(QDropEvent *event) override;

public slots:
    /**
     * @brief get the animation from library
     * @param anim animation to be displayed
     * @param anim_id to be used for operations
     */
    void get_animation(lib_animation::Animation *anim, int &anim_id);

    /**
     * @brief Removes an animation using it's id
     * @param anim_id Animation id to search for
     */
    void remove_animation(const int &anim_id);

    /**
     * @brief Called when an animation is updated
     * @param anim Animation to search for
     */
    void update_animation(const lib_animation::Animation *anim);

    /**
     * @brief Updates the animation id from a stored animation
     * @param old_id Old id to change
     * @param new_id New id to use
     */
    void update_animation_id(const size_t &old_id, const size_t &new_id);

    /**
     * @brief called when we want to start the broadcast
     */
    void on_butBroadcast_clicked();

    /**
     * @brief called when the broadcast ask for the animation
     */
    void get_asked_for_frames();

    /**
     * @brief called when the broadcast ask for the canvas frame
     */
    void ask_frame_live_drawing();

    /**
     * @brief called when the broadcast get the canvas frame
     * @param
     */
    void get_frame_live_drawing(lib_frame::Frame &frame);

    /**
     * @brief called when the button live drawing is pressed
     */
    void start_stop_live_drawing(bool);

    void send_ndi_state(const bool state);

signals:
    /**
    * @brief signal emitted when an animation is dropped
    * @param animNumber
    */
    void animDropped(int animNumber);

    /**
     * @brief emited to get the required animation from the library
     * @param  pos animation position from the library
     */
    void get_anim(int &pos);

    /**
     * @brief signal emited on start/stop broadast
     * @param pos animation position inside the composing bench
     */
    void start_broadcast(int pos);

    /**
     * @brief emitted when composing bench is asked for an animation
     * @param frames animation
     */
    void frames(std::optional<std::vector<lib_animation::Animation*>> frames);

    /**
     * @brief send_frame emited to send frame to the broadcaster
     */
    void send_frame(lib_frame::Frame &frame);

    /**
     * @brief emited to get the canvas frame
     */
    void get_canvas_frame();

    /**
     * @brief emited to stop live_drawing
     */
    void stop_live_drawing();

    /**
     * @brief emited to update current anim inside the broadcaster
     * @param animation to update
     */
    void update_current_anim(lib_animation::Animation animation);

private slots:
    /**
     * @brief called when button delete all is pressed
     */
    void on_butDeleteAll_clicked();

    /**
     * @brief called when radio button 12 fps is pressed
     */
    void on_radBut12fps_clicked();

    /**
     * @brief called when radio button 24 fps is pressed
     */
    void on_radBut24fps_clicked();

    void on_broadcast_mode_activated(int index);

    void on_composition_mode_activated(int index);

private:
    /**
     * @brief get_frames at index pos
     * @param pos animation position
     * @return std::vector<lib_frame::Frame *> animation
     */
    std::vector<lib_frame::Frame *> get_frames(size_t pos);
    /**
     * @brief m_ui ui
     */
    Ui::ComposingBench *m_ui;
    /**
     * @brief m_hovered set if bench is hovered with an anim button
     */
    bool m_hovered;
    /**
     * @brief m_animations
     */
    std::vector<BroadcastButton *> m_animations;
    /**
     * @brief m_is_live boolean to set if we're living
     */
    bool m_is_live;
    /**
     * @brief m_is_live_drawing boolean to set if we're live_drawing
     */
    bool m_is_live_drawing;
    /**
     * @brief m_broadcaster class used to broadcast
     */
    lib_broadcast::BroadCaster *m_broadcaster;
    /**
     * @brief m_fps current fps streaming
     */
    int m_fps;
    /**
     * @brief get index of of current animation of animId id
     * @param id asked current anim id
     * @return index if present else -1
     */
    int get_active_animation_number(size_t id);
};


} // namespace lib_composing_bench
#endif // COMPOSINGBENCH_H
