/**
 @author Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU

 @section License

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/color_button.hpp"
#include "qevent.h"
#include "qstylepainter.h"
#include <iostream>
#include <sstream>
namespace lib_color_selector{

ColorButton::ColorButton(QWidget *parent, const QColor &bg_color):
    QPushButton(parent), back(Qt::darkGray, Qt::DiagCrossPattern)
{
    this->setFlat(true);
    setAutoFillBackground(true);
    back.setTexture(QPixmap(QLatin1String(":/alphaback.png")));
    change_color(bg_color);
}

void ColorButton::change_color(const QColor &col){
    QPalette pal = this->palette();
    back.setColor(col);
    pal.setBrush(QPalette::Base,back);
    pal.setColor(QPalette::Button, col);
    this->setPalette(pal);
    this->update();
}

QColor ColorButton::get_color() const
{
    return this->palette().color(QPalette::Button);

}

void ColorButton::mousePressEvent(QMouseEvent *e)
{
    if(e->button()==Qt::RightButton){
        emit right_clicked();
    }
    else if(e->button()==Qt::LeftButton){
        emit left_clicked();
    }
}

void ColorButton::paint(QPainter &painter, QRect rect) const
{
    QColor c1, c2;
    c1 = c2 = get_color();
    if(c1.alpha()<255 || c2.alpha()<255)
        painter.fillRect(1, 1, rect.width()-2, rect.height()-2, back);
    int w = (rect.width() - 2) / 2;
    int h = rect.height() - 2;
    painter.fillRect(1, 1, w, h, c1);
    painter.fillRect(1+w, 1, w, h, c2);
}

void ColorButton::paintEvent(QPaintEvent *event){
    QStylePainter painter(this);
    paint(painter, geometry());
    QWidget::paintEvent(event);
}

}
