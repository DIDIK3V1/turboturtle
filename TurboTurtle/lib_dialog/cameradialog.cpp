/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Creerio

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "cameradialog.h"
#include "QMediaDevices"
#include "QCameraDevice"
#include "ui_cameradialog.h"
#include <QTextCursor>
#include <QColor>

namespace lib_dialog {

CameraDialog::CameraDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::CameraDialog)
{
    ui->setupUi(this);

    ui->textDialog->setText("<font color='red'>Caution the current frame will be overwritten, same if you move of frame with the camera on.<br>We advise you to use a white frame</font><br>Please choose a camera");

    const QList<QCameraDevice> cameras = QMediaDevices::videoInputs();

    for (const QCameraDevice &cameraInfo : cameras) {
        ui->comboBox->addItem(cameraInfo.description());
        qDebug() << cameraInfo.description();
    }
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

CameraDialog::~CameraDialog()
{
    delete ui;
}

void CameraDialog::on_buttonBox_accepted()
{
    emit camera_chosen(ui->comboBox->currentIndex());
}

} // namespace lib_dialog
