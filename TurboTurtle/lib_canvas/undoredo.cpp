/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "undoredo.h"
namespace lib_frame {

QImage UndoRedo::undo()
{
    try {
        if(m_undo_frames.size() > 1)
        {

            m_redo_frames.push_back(m_undo_frames.back());
            m_undo_frames.pop_back();

        }
        return m_undo_frames.back();
    } catch (const std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }
}

QImage UndoRedo::undo_top()
{
    if(m_undo_frames.size() > 0)
        return m_undo_frames[m_undo_frames.size()-1];
    else
        return QImage();
}

UndoRedo UndoRedo::operator=(const UndoRedo &other){
    if (this != &other) {
        this->clear_redo();
        this->clear_undo();

        for (size_t i = 0; i < other.m_undo_frames.size(); i++) {
            this->m_undo_frames.push_back(other.m_undo_frames[i].copy());
        }

        for (size_t i = 0; i < other.m_redo_frames.size(); i++) {
            this->m_redo_frames.push_back(other.m_redo_frames[i].copy());
        }
    }
    return *this;
}

QImage UndoRedo::redo()
{
    try {
        if(m_redo_frames.size() > 0)
        {
            m_undo_frames.push_back(m_redo_frames.back());
            m_redo_frames.pop_back();
        }
        return m_undo_frames.back();
    } catch (const std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }
}

void UndoRedo::add(const QImage &image)
{
    if(m_undo_frames.size() == MAX_UNDO)
    {
        m_undo_frames.erase(m_undo_frames.begin());
    }
    m_undo_frames.push_back(image);
    clear_redo();
}

void UndoRedo::clear_undo()
{
    m_undo_frames.clear();
}

void UndoRedo::clear_redo()
{
    m_redo_frames.clear();
}

void UndoRedo::clear()
{
    clear_redo();
    clear_undo();
}

bool UndoRedo::undo_is_empty()
{
    return m_undo_frames.size() == 0;
}

bool UndoRedo::redo_is_empty()
{
    return m_redo_frames.size() == 0;
}

void UndoRedo::resize(const QSize &new_size) {
    for(QImage& image: m_undo_frames)
        image = image.scaled(new_size, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    for(QImage& image: m_redo_frames)
        image = image.scaled(new_size, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
}
} // namespace lib_frame
