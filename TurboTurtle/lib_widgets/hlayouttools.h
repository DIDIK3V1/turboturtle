/**
 @author Creerio

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QShortcut>
#include <QWidget>
#include <stack>

namespace Ui { class HLayoutTools; }

namespace lib_widgets {
    /**
     * @brief Contains buttons and a widget used with a TemplatedHLayout
     */
    class HLayoutTools : public QWidget {
        Q_OBJECT

    public:
        /**
         * @brief Default constructor of the HLayoutTools
         * @param parent Parent QWidget
         */
        explicit HLayoutTools(QWidget *parent = nullptr);
        ~HLayoutTools();

        /**
         * @brief update_counter Updates the label with the number of the currently loaded items and the number of the currently loaded one
         * @param current_loaded number of the currently loaded element
         * @param nb_elements Number of elements
         */
        void update_counter(const size_t &current_loaded, const size_t &nb_elements);

    protected:
        /**
         * @brief addWidget Used to add addon frame
         * @param widget Widget to add
         */
        void addWidget(QWidget *widget);

        /**
         * @brief addWidgetShortcut Used to add a shortcut to the add button
         * @param shortcuts Shortcuts to connect
         * @param tooltip Tooltip set on the button
         */
        void addWidgetShortcut(const QList<QKeySequence> &shortcuts, const std::string &tooltip);

        /**
         * @brief addWidgetShortcut Used to add a shortcut to the duplicate button
         * @param shortcuts Shortcuts to connect
         * @param tooltip Tooltip set on the button
         */
        void duplicateWidgetShortcut(const QList<QKeySequence> &shortcuts, const std::string &tooltip);

        /**
         * @brief addWidgetShortcut Used to add a shortcut to the delete button
         * @param shortcuts Shortcuts to connect
         * @param tooltip Tooltip set on the button
         */
        void deleteWidgetShortcut(const QList<QKeySequence> &shortcuts, const std::string &tooltip);

        /**
         * @brief remove_delete Removes the delete button
         */
        void remove_delete();

        /**
         * @brief remove_frame Removes the addon frame
         */
        void remove_frame();

    private slots:
        /**
         * @brief Slot called when clicking on the Add button
         */
        virtual void on_btnAdd_clicked() = 0;

        /**
         * @brief Slot called when clicking on the Delete button
         */
        virtual void on_btnDelete_clicked() = 0;

        /**
         * @brief Slot called when clicking on the Duplicate button
         */
        virtual void on_btnDuplicate_clicked() = 0;

    private:
        /**
         * @brief User interface, from the corresponding .ui file
         */
        Ui::HLayoutTools *m_ui;

        /**
         * @brief Shortcuts used in the layout
         */
        std::stack<std::unique_ptr<QShortcut>> m_shortcuts;
    };
}
