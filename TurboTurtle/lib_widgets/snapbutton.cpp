#include "snapbutton.h"

namespace lib_widgets {

    SnapButton::SnapButton(QWidget *parent) : QToolButton(parent) {
        connect(this, &QToolButton::released, this, [this](){ emit snap(); });

        addAction("Snap And New Frame", this, [this](){
            emit snap_and_new_frame();
        });
    }
}