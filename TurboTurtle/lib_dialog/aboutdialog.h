/**
 @author Creerio

 @section License

 Copyright (C) 2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QDialog>

namespace lib_dialog {
    namespace Ui { class AboutDialog; }

    /**
     * @brief Dialog containing information about this software
     */
    class AboutDialog : public QDialog {
        Q_OBJECT

    public:
        /**
         * @brief Constructor for the dialog
         * @param parent Parent QWidget
         */
        explicit AboutDialog(QWidget *parent = nullptr);

        /**
         * @brief Destructor for the dialog
         */
        ~AboutDialog();

    private slots:
        /**
         * @brief Slot called when clicking on the close button
         */
        void on_btn_close_released();

    private:
        /**
         * @brief User interface
         */
        Ui::AboutDialog *m_ui;
    };
}
