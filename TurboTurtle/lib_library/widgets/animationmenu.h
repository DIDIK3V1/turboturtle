/**
 @authors Creerio, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QMenu>
#include "lib_library/widgets/animationbutton.h"
#include "lib_library/widgets/animationlayout.h"

namespace lib_library {
    /**
     * @brief Animation menu, called when right clicking on an AnimationButton
     */
    class AnimationMenu : public QMenu
    {
    public:
        /**
         * @brief Default constructor for an AnimationMenu
         * @param parent Parent QWidget
         */
        explicit AnimationMenu(AnimationButton *parent = nullptr);
        ~AnimationMenu();

    private:
        /**
         * @brief AnimationButton calling for the menu
         */
        AnimationButton *m_btn;

        /**
         * @brief Layout associated
         */
        AnimationLayout *m_layout;

        /**
         * @brief Button selected in the m_layout
         * Used to reselect the button when this menu is closed
         */
        const AnimationButton *m_loaded;

        /**
         * @brief Used to insert a new animation before the selected AnimationButton
         */
        void insert_before();

        /**
         * @brief Used to insert a new animation after the selected AnimationButton
         */
        void insert_after();

        /**
         * @brief Used to duplicate the selected animation
         */
        void duplicate();

        /**
         * @brief Used to remove the selected animation
         */
        void remove();

        /**
         * @brief Used to load the selected animation
         */
        void load();

        /**
         * @brief Used to save the selected animation as a png
         */
        void save_png();

        /**
         * @brief Used to save the selected animation as a gif
         */
        void save_gif();

        /**
         * @brief Used to save the selected animation as an MPEG-4 video file
         */
        void save_mp4();
    };
}
