/**
 @author Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef POSINDICATOR_H
#define POSINDICATOR_H

#include <QWidget>

namespace Ui {
class posindicator;
}


namespace lib_widgets {
class posindicator : public QWidget
{
    Q_OBJECT

public:
    explicit posindicator(const QString format, const size_t number = -1, QWidget *parent = nullptr);
    ~posindicator();

    void set_number(const size_t val);

protected:
    /**
    * @brief event called when painting
    * @param e paint event
    */
    void paintEvent(QPaintEvent *);

    /**
    * @brief event called when mouse is entering and dragging an element
    * @param event drag enter event
    */
    void dragEnterEvent(QDragEnterEvent *event);

    /**
    * @brief event called when mouse is leaving and dragging an element
    * @param event drag leave event
    */
    void dragLeaveEvent(QDragLeaveEvent *event);

    /**
    * @brief event called when item is dropped
    * @param event drop event
    */
    void dropEvent(QDropEvent *event) override;

    /**
     * @brief event called when clicking on this object
     * Prints current number
     * @param e click event
     */
    void mousePressEvent(QMouseEvent *e);

private:
    Ui::posindicator *ui;

    /**
     * @brief m_hovered bool to know if we're hovering over an animation
     */
    bool m_hovered;
    /**
     * @brief m_number number of the indicator to know where we dropped an animation
     */
    size_t m_number;

    const QString m_itemFormat;

    /**
     * @brief paint
     * @param painter
     * @param rect
     */
    void paint(QPainter &painter, const QRect &rect) const;


signals:
    /**
    * @brief signal called when a frame is dropped
    * @param frameNumber location of the frame
    * @param position position of the indicator
    */
    void itemDropped(int itemNumber, int position);

};
}
#endif // POSINDICATOR_H
