/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Alexis BOUE

 @section License

 Copyright (C) 2023-2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PAINTER_H
#define PAINTER_H

#include "drawingtools/brush.h"
#include "qevent.h"
#include "lib_canvas/frame.h"
#include <QPair>

namespace lib_canvas {
/**

@brief The Painter class allows to draw on a QImage.
*/
class Painter : public QObject
{
    Q_OBJECT
private:
    /**
     * @brief The hidden image (real frame) to draw on.
     */
    lib_frame::Frame *m_frame_hidden;

    /**
     * @brief The displayed image (frame with onion skinning) to draw on.
     */
    lib_frame::Frame *m_frame_displayed;

    /**
     * @brief witdth of the pen, default value = 1
     */
    int m_pen_width = 1;

    /**
     * @brief color of the pen default value = black
     */
    QColor m_pen_color = Qt::black;

public:
    /**
    * @brief Constructs a Painter object.
    * @param hidden Pointer to the image to draw on.
    * @param displayed Pointer to the image background to use.
    */
    Painter(lib_frame::Frame *hidden, lib_frame::Frame *displayed);

    /**
     * @brief Prints an image within a rectangle
     * @param img: image to print
     * @param rect: rectangle to print the image within
     */
    void print_pasted_image(const QImage &img, const QRect &rect);

    /**
    * @brief Draws a line to the given end point.
    * @param end_point The end point of the line.
    * @param custom_brush The brush to use.
    * @param last_point The last point drawn.
    */
    void draw_line_to(const QPoint &end_point,
                      lib_drawing_tools::Brush &custom_brush,
                      QPoint &last_point);

    /**
    * @brief Draws a line to the given end point within the clipped rectangle.
    * @param end_point The end point of the line.
    * @param custom_brush The brush to use.
    * @param last_point The last point drawn.
    * @param clipping_rect The clipped rectangle.
    */
    void draw_line_to_selection(const QPoint &end_point,
                      lib_drawing_tools::Brush &custom_brush,
                      QPoint &last_point,
                      const QRect &clipping_rect);

    /**
    * @brief Draws a line to the given end point within the clipped polygon.
    * @param end_point The end point of the line.
    * @param custom_brush The brush to use.
    * @param last_point The last point drawn.
    * @param clipping_poly The clipped polygon.
    */
    void draw_line_to_selection(const QPoint &end_point,
                                lib_drawing_tools::Brush &custom_brush,
                                QPoint &last_point,
                                const QPolygon &clipping_poly);

    /**
    * @brief Draws a point to the given end point.
    * @param end_point The point to draw.
    * @param custom_brush The brush to use.
    */
    void draw_point_to(const QPoint &end_point, lib_drawing_tools::Brush &custom_brush);

    /**
     * @brief Draws a point at the given coordinates within the selection rectangle
     * @param end_point: the coordinates to draw on
     * @param custom_brush: brush to use
     * @param clipping_rect: selection rectangle to draw in
     */
    void draw_point_to_selection(const QPoint &end_point,
                                 lib_drawing_tools::Brush &custom_brush,
                                 const QRect &clipping_rect);

    /**
     * @brief Draws a point at the given coordinates within the selection polygon
     * @param end_point: the coordinates to draw on
     * @param custom_brush: brush to use
     * @param clipping_poly: selection polygon to draw in
     */
    void draw_point_to_selection(const QPoint &end_point,
                                 lib_drawing_tools::Brush &custom_brush,
                                 const QPolygon &clipping_poly);

    /**
    * @brief Calculates the area to be erased and replaced with the background.
    * @param end_point The end point of the eraser.
    * @param pen_width The width of the eraser.
    * @return The calculated area of the background image for the two frames (hidden and displayed).
    */
    QPair<QImage, QImage> calculate_erased(const QPoint &end_point,
                            int pen_width);

    /**
    * @brief Erases the foreground of the frame.
    * @param end_point The end point of the line.
    * @param last_point The last point erased..
    */
    void erase_foreground(const QPoint &end_point, QPoint &last_point);

    /**
    * @brief Erases the foreground of the frame within the clipped rectangle.
    * @param end_point The end point of the line.
    * @param last_point The last point erased..
    * @param clipping_rect The clipped rectangle.
    */
    void erase_foreground_selection(const QPoint &end_point, QPoint &last_point, const QRect &clipping_rect);

    /**
    * @brief Erases the foreground of the frame within the clipped polygon.
    * @param end_point The end point of the line.
    * @param last_point The last point erased..
    * @param clipping_poly The clipped polygon.
    */
    void erase_foreground_selection(const QPoint &end_point, QPoint &last_point, const QPolygon &clipping_poly);

    /**
    * @brief Erases the selected area of the frame
    * @param area_to_delete Rectangle of the frame to erase
    */
    void erase_selection(const QRect &area_to_delete);

    /**
    * @brief Erases the selected area of the frame
    * @param area_to_delete Rectangle of the frame to erase
    */
    void erase_selection(const QPolygon &polygon_to_delete);

    /**
    * @brief Fills an area with the same color.
    * @param point The point to start filling from.
    * @param base_color The base color of the area.
    * @param pen_color The color of the pen to use.
    */
    void fill_shape(const QPoint &point, const QColor &base_color, const QColor &pen_color);

    /**
    * @brief Fills the selected area with the same color.
    * @param point The point to start filling from.
    * @param base_color The base color of the area.
    * @param pen_color The color of the pen to use.
    * @param selection_rectangle The selected rectangle to fill
    */
    void fill_shape(const QPoint &point, const QColor &base_color, const QColor &pen_color, const QRect &selection_rectangle);

    /**
    * @brief Fills the selected area with the same color.
    * @param point The point to start filling from.
    * @param base_color The base color of the area.
    * @param pen_color The color of the pen to use.
    * @param selection_polygon The selected polygon to fill
    */
    void fill_shape(const QPoint &point, const QColor &base_color, const QColor &pen_color, const QPolygon &selection_polygon);

    /**
    * @brief Draws a line to the given end point from a tablet event.
    * @param event The tablet event.
    * @param custom_brush The brush to use.
    * @param last_point The last point drawn.
    */
    void tablet_draw_line_to(const QTabletEvent *event,
                             lib_drawing_tools::Brush &custom_brush,
                             QPoint &last_point);

    void tablet_draw_line_to_selection(const QTabletEvent *event,
                                      lib_drawing_tools::Brush &custom_brush,
                                       QPoint &last_point,
                                       const QRect &clipping_rect);

    void tablet_draw_line_to_selection(const QTabletEvent *event,
                                      lib_drawing_tools::Brush &custom_brush,
                                       QPoint &last_point,
                                       const QPolygon &clipping_poly);

    /**
     * @brief set the color of the painter pen
     * @param color pen color to be set
     */
    inline void set_pen_color(const QColor &color) { m_pen_color = color; }

    /**
     * @brief set the witdth of the painter pen
     * @param width pen width to be set.
     */
    inline void set_pen_width(const int &width) { m_pen_width = width; }

    /**
     * @brief get the pen color
     * @return pen color.
     */
    inline QColor *pen_color() { return &m_pen_color; }

    /**
     * @brief get the pen width
     * @return width of the painter pen.
     */
    inline int *pen_width() { return &m_pen_width; }

    /**
     * @brief used to set the hidden and displayed frame
     * @param hidden the hidden frame (the real frame)
     * @param displayed the displayed frame (onion skinning)
     */
    void set_frame(lib_frame::Frame *hidden, lib_frame::Frame *displayed);

    /**
     * @brief change pen color to the opposite color of the frame to stay visible
     * @param frame current frame
     * @param point current cursor point
     * @param brush current brush
     */
    void set_cursor_color(lib_frame::Frame *frame, QPoint point, lib_drawing_tools::Brush &brush);

signals:
    /**
    * @brief This signal is emitted when a frame has been modified.
    */
    void frameModified();

    /**
    * @brief This signal is emitted when the cursor needs to be updated.
    */
    void updateCursor();
};
} // namespace lib_canvas
#endif
