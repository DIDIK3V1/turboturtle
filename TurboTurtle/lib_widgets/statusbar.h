/**

 @authors Creerio

 @section License

 Copyright (C) 2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QStatusBar>

namespace lib_widgets {

    /**
     * @brief Class used to access the status bar
     * WARNING : Normally this should be a singleton, however because QT won't use getInstance (UI file, MOC, all that), we have to keep a public constructor
     */
    class StatusBar : public QStatusBar {

    Q_OBJECT

    public:

        explicit StatusBar(QWidget *parent = nullptr);

        /**
         * @brief No copy
         * @param bar
         */
        StatusBar(StatusBar &bar) = delete;

        /**
         * @brief No assignment
         * @return
         */
        void operator=(const StatusBar &) = delete;

        /**
         * @brief Access to the status bar, while QT won't use that, we will
         * @return The ""only"" instance of the status bar
         */
        static StatusBar* getInstance();

    private:
        /**
         * @brief m_instance Instance of the status bar
         */
        static StatusBar * m_instance;

    signals:
        /**
         * @brief Signal to use when sending on another thread
         * @param message Message sent
         * @param timeout If the message should timeout or not, time in ms
         */
        void sendMessage(const QString &message, int timeout = 0);
    };

} // lib_widgets

