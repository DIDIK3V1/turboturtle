/**
 @authors Julien BLANCO, Samuel GOUBEAU, Kévin DIDIER, Idaïa METZ, Lucas PLANCHET, Alexis BOUE, Creerio, Katarina

 @section License

    Copyright (C) 2023-2024 TurboTurtle Contributors

    This file is part of TurboTurtle.

    TurboTurtle is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TurboTurtle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TurboTurtle.  If not, see http://www.gnu.org/licenses/.
*/

#include "thicknessslider.h"
#include <QStyleOptionSlider>
namespace lib_drawing_tools {
ThicknessSlider::ThicknessSlider(QWidget *parent)
    : QSlider(Qt::Horizontal, parent)
{

}
ThicknessSlider::ThicknessSlider(const Qt::Orientation &orientation, QWidget *parent)
    : QSlider(orientation, parent)
{}

void ThicknessSlider::mousePressEvent(QMouseEvent *event)
{
    QStyleOptionSlider opt;
    initStyleOption(&opt);
    QRect sr = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);

    if (event->button() == Qt::LeftButton &&
        !sr.contains(event->pos())) {
        int new_val;
        if (orientation() == Qt::Vertical) {
            double half_handle_height = (0.5 * sr.height()) + 0.5;
            int adapted_pos_Y = height() - event->position().y();
            if ( adapted_pos_Y < half_handle_height )
                adapted_pos_Y = half_handle_height;
            if ( adapted_pos_Y > height() - half_handle_height )
                adapted_pos_Y = height() - half_handle_height;
            double new_height = (height() - half_handle_height) - half_handle_height;
            double normalized_position = (adapted_pos_Y - half_handle_height)  / new_height ;

            new_val = minimum() + qRound((maximum()-minimum()) * normalized_position);
        } else {
            double half_handle_width = (0.5 * sr.width()) + 0.5;
            int adapted_pos_X = event->position().x();
            if ( adapted_pos_X < half_handle_width )
                adapted_pos_X = half_handle_width;
            if ( adapted_pos_X > width() - half_handle_width )
                adapted_pos_X = width() - half_handle_width;
            double new_width = (width() - half_handle_width) - half_handle_width;
            double normalized_position = (adapted_pos_X - half_handle_width)  / new_width ;

            new_val = minimum() + qRound((maximum()-minimum()) * normalized_position);
        }

        if (invertedAppearance())
            this->setValue( maximum() - new_val );
        else
            this->setValue(new_val);

        event->accept();
    }
    else {
        QSlider::mousePressEvent(event);
    }
}
} // namespace lib_drawing_tools
