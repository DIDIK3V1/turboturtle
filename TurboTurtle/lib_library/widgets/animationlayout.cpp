/**
 @authors Creerio, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include "animationlayout.h"
#include <QThread>
#include <QDirIterator>
#include <QProcess>
#include <QtConcurrent/QtConcurrent>
#include "lib_canvas/canvas.h"
#include "lib_widgets/statusbar.h"
#include <JlCompress.h>
#include <quazip.h>

namespace lib_library{

    AnimationLayout::AnimationLayout(QWidget *parent) : TemplatedHLayout("application/x-anim-data", parent)
    {
        // Check for backup files, in case of an app crash
        if (QDir backupFolder(lib_animation::Animation::BACKUP_LOCATION); backupFolder.exists() && !backupFolder.isEmpty()) {
            qDebug() << QString::fromStdString(get_name()) << "Found backup files, asking user for potential recovery";

            const QMessageBox::StandardButton choice = QMessageBox::question(this, "TurboTurtle : Backup Animations found", "Found backup animations.\nDo you want to load them into the app? If you do not, the backup will be removed !");

            if (choice == QMessageBox::Yes) {
                // Move folder first, then iterate
                QString recovery_path(backupFolder.absolutePath());
                recovery_path.chop(1);
                recovery_path.append("_rec");
                QDir recovery_folder(recovery_path);
                recovery_folder.removeRecursively();
                if (!QDir().rename(backupFolder.absolutePath(), recovery_folder.absolutePath())) {
                    qWarning() << QString::fromStdString(get_name()) << "Failed to recover the animations : rename failed";
                    QMessageBox::warning(this, "TurboTurtle : Backup animation", "Failed to backup the animations.\nTo avoid issues with the program the process will be terminated.\nPlease extract the images from " + backupFolder.absolutePath());
                    std::exit(1);
                }

                QDirIterator folders(recovery_folder.absolutePath(), QDir::Dirs | QDir::NoDotAndDotDot);
                while (folders.hasNext()) {
                    const QString next(folders.next());
                    qDebug() << QString::fromStdString(get_name()) << "Recovering an animation from" << next;
                    import_from_png(next, true);
                }

                // Done recovering, remove the files
                recovery_folder.removeRecursively();
            }
            else
                clear_backup_animations();
        }


        // Backup timer called every 45 seconds in another thread
        backup_timer.setInterval(1000 * 45);
        QObject::connect(&backup_timer, &QTimer::timeout, this, [this](){ QFuture<void> fut = QtConcurrent::run([this]() { this->backupAnimations(); }); });
        backup_timer.start();
    }

    AnimationLayout::~AnimationLayout() {
        // Won't be called on crash
        backup_timer.stop();
        clear_backup_animations();
    }

    const bool AnimationLayout::add(lib_animation::Animation *anim) {
        // Add cannot fail for now (no false returned), force backup the animation before it tries to delete it
        anim->is_modified = true;
        anim->backup();
        return TemplatedHLayout::add(anim);
    }

    const bool AnimationLayout::insert(lib_animation::Animation *anim, const size_t pos) {
        // Pre-check to backup first
        if (pos > m_items.size()) {
            qDebug() << QString::fromStdString(get_name()) << "failed to insert item at pos " << pos << " : Out of range";
            return false;
        }

        // Force backup
        anim->is_modified = true;
        anim->backup();

        return TemplatedHLayout::insert(anim, pos);
    }

    const bool AnimationLayout::remove(const int &pos) {
        // Remove animation ONLY if it's not the one currently loaded
        if (m_items[pos] != get_loaded()) {
            QMessageBox msgBox;
            msgBox.setText("Do you want to remove the currently selected animation?");
            msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msgBox.setDefaultButton(QMessageBox::No);
            int ret = msgBox.exec();
            if (ret != QMessageBox::Yes)
                return false;

            lib_animation::Animation *to_delete = m_items[pos]->get();
            if (TemplatedHLayout::remove(pos)) {
                // Inform the composing bench that this animation has been removed
                emit animation_removed_to_cb(pos);
                // delete frames
                for (lib_frame::Frame *frame : *to_delete) {
                    delete frame;
                }
                to_delete->clear();
                delete to_delete;
                return true;
            }
            else
                return false;

        }
        return false;
    }

    const bool AnimationLayout::move_item(const size_t &from, size_t to) {
        // If moved, inform the composing bench of the changes
        if (TemplatedHLayout::move_item(from, to)) {
            // Include position fix from move_item
            emit animation_new_id_to_cb(from, from + 1 >= to ? to : to - 1);
            return true;
        }
        return false;
    }

    void AnimationLayout::clear(const bool is_delete) {
        // Before clearing the animations, delete their frames & clear them
        for (lib_library::AnimationButton const *btn : m_items) {
            for (lib_frame::Frame *frame : *btn->get()) {
                delete frame;
            }
            // Also clear the animation storage
            btn->get()->clear();
        }


        TemplatedHLayout::clear(is_delete);

        // If we are deleting the object, stop here
        if (is_delete)
            return;

        // Add default animation and load it
        lib_animation::Animation *anim = lib_animation::Animation::new_animation();
        add(anim);
        select(0);
        emit load_item(m_items[0]);
    }

    void AnimationLayout::unselect(const int pos) {
        TemplatedHLayout::unselect(pos);
        // Update animation thumbnail when unselected, no need for that on frames
        lib_library::AnimationButton *btn = m_items[pos];

        // First frame is the thumbnail
        if (!btn->get()->empty())
            btn->set_thumbnail(btn->get()[0].get_thumbnail());
    }

    void AnimationLayout::send_animation_to_cb(int &pos) {
        if (pos < 0 || m_items.size() <= pos)
            return;

        m_items[pos]->get()->set_used_in_composing_bench(true);
        // Reload animation if not loaded
        if (!m_items[pos]->get()->is_loaded())
            m_items[pos]->get()->reload_from_backup();

        emit animation_to_cb(m_items[pos]->get(), pos);
    }

    void AnimationLayout::load_last_animation()
    {
        // In case of a backup, we want to keep the first backup frame from being erased by the canvas
        const size_t pos = m_items.size() - 1;
        select(int(pos));
        emit load_item(m_items[pos]);
    }

    void AnimationLayout::save_all_png(){
        QString save_folder = QFileDialog::getExistingDirectory(this, "Select a folder where the animation will be saved", QDir::homePath());
        if (save_folder.isEmpty()) {return;}

        for(size_t i = 0; i < m_items.size(); ++i){
            QString folder_dest = save_folder + "/" ;

            // Use default animation naming scheme + the animation number
            folder_dest.append("animation").append(QString::number(i));

            QFuture<void> fut = QtConcurrent::run([this, i, folder_dest]() {
                m_items[i]->get()->save_to_png(folder_dest);
            });

        }
    }

    void AnimationLayout::save_all_gif(){
        std::string prog("ffmpeg -h ");
        #ifdef Q_OS_WIN
        prog.append("> nul");
        #else
        prog.append("&> /dev/null");
        #endif
        if (system(prog.c_str()) != 0) {
            QMessageBox::warning(this, "Gif export", "ffmpeg was not found");
            return;
        }

        QString save_folder = QFileDialog::getExistingDirectory(this, "GIF save folder", QDir::homePath());
        if (save_folder.isEmpty()) {return;}

        double animation_framerate = QInputDialog::getDouble(nullptr,                   //parent widget
                                                             tr("animation framerate"), //title
                                                             tr("framerate: "),         //input label
                                                             10,                        //default framerate
                                                             0,                         //minimum framerate
                                                             1000,                      //maximum framerate
                                                             2);                        //decimal precision

        for(size_t i = 0; i < m_items.size(); ++i) {
            QFuture<void> fut = QtConcurrent::run([this, i, save_folder, animation_framerate]() {
                m_items[i]->get()->save_to_gif(save_folder + "/animation" + QString::number(i) + ".gif", animation_framerate);
            });
        }
    }

    void AnimationLayout::save_session(const QString &zip_path){
        // Remove file if it exists
        if (QFile::exists(zip_path))
            QFile::remove(zip_path);

        // Use the backup system as a way to prepare the session
        for (AnimationButton const *btn : m_items) {
            btn->get()->backup();
        }

        // Create zip file
        if (!JlCompress::compressDir(zip_path, lib_animation::Animation::BACKUP_LOCATION)) {
            qWarning() << QString::fromStdString(get_name()) << "Failed to compress the backup as a zip file to " << zip_path;
            QMessageBox::warning(this, "Turbo Turtle : Session export", "Failed to save the session. Please try again.");
        }
        emit lib_widgets::StatusBar::getInstance()->sendMessage("Saved " + zip_path + " successfully", 5 * 1000);
    }

    void AnimationLayout::load_session(const QString &zip_path) {
        if (!QFile::exists(zip_path)) {
            qWarning() << QString::fromStdString(get_name()) << "Given file" << zip_path << "does not exist !";
            return;
        }
        // Pause backup timer
        backup_timer.stop();

        // Create extract folder
        QString extract_path = lib_animation::Animation::BACKUP_LOCATION;
        extract_path.chop(1);
        extract_path.append("_extract");
        QDir extract_path_dir(extract_path);
        extract_path_dir.removeRecursively();

        if (!QDir().mkpath(extract_path)) {
            qWarning() << QString::fromStdString(get_name()) << "Failed to create the extract path" << extract_path;
            QMessageBox::warning(this, "Turbo Turtle : Session import", "Cannot extract the provided session. Please try again.");
            backup_timer.start();
            return;
        }

        // Try to extract it
        if (JlCompress::extractDir(zip_path, extract_path).empty()) {
            qWarning() << QString::fromStdString(get_name()) << "Failed to extract the provided session file " << zip_path << " to " << extract_path;
            QMessageBox::warning(this, "Turbo Turtle : Session import", "Cannot extract the provided session. Please try again.");
            backup_timer.start();
            return;
        }

        // Save old backup folder in case of an issue
        QString old_backup(lib_animation::Animation::BACKUP_LOCATION);
        old_backup.chop(3);
        QDir().rename(lib_animation::Animation::BACKUP_LOCATION, old_backup);

        // Import the animations
        std::vector<lib_animation::Animation*> anims;
        QDirIterator it(extract_path, QDir::NoSymLinks|QDir::NoDotAndDotDot|QDir::Dirs);
        while (it.hasNext()) {
            it.next();

            // Import from the zip every animation. Mark as backup recovery to avoid modifications on the backup folder and user messages
            lib_animation::Animation* new_anim = lib_animation::Animation::import_from_png(it.filePath(), true);

            if (new_anim != nullptr)
                anims.push_back(new_anim);
        }

        if (anims.empty()) {
            qWarning() << QString::fromStdString(get_name()) << "File " << zip_path << " does not contain any animation ";
            QMessageBox::warning(this, "Turbo Turtle : Session import", "No animation found.");
            extract_path_dir.removeRecursively();
            QDir().rename(old_backup, lib_animation::Animation::BACKUP_LOCATION);
            backup_timer.start();
            return;
        }

        // Remove old sessions backup and extracted paths
        QDir backup_folder(old_backup);

        if (!backup_folder.removeRecursively() || !extract_path_dir.removeRecursively()) {
            qWarning() << QString::fromStdString(get_name()) << "Failed to remove folders";
            QMessageBox::warning(this, "Turbo Turtle : Session import", "Failed to use the provided session. Please try again.");
            backup_timer.start();
            return;
        }

        // Clear animations, there is still one, but it will be removed when everything is imported
        for (lib_library::AnimationButton const *btn : m_items) {
            for (lib_frame::Frame *frame : *btn->get()) {
                delete frame;
            }
            // Also clear the animation storage
            btn->get()->clear();
        }

        TemplatedHLayout::clear();

        // Add new animations
        for (lib_animation::Animation *anim : anims) {
            add(anim);
        }

        // Load last animation and restart timer
        load_last_animation();
        backup_timer.start();
        emit lib_widgets::StatusBar::getInstance()->sendMessage("Loaded session file successfully", 5 * 1000);
    }

    void AnimationLayout::import_from_png(const QString &import_folder, const bool is_backup){
        lib_animation::Animation* new_anim = lib_animation::Animation::import_from_png(import_folder, is_backup);

        if (new_anim == nullptr)
            return;

        this->add(new_anim);

        if (!is_backup) {
            clear_selection();
            select(std::bit_cast<const AnimationButton*>(get_loaded())->get_pos());
        }
    }

    void AnimationLayout::import_from_gif(const QString &image_file)
    {
        if (!QFile::exists(image_file)) {
            qWarning() << QString::fromStdString(get_name()) << "Failed to import" << image_file << " : the file doesn't exist";
            return;
        }

        lib_animation::Animation *anim = lib_animation::Animation::import_from_gif(image_file);

        // Add to library
        add(anim);
        clear_selection();
        select(std::bit_cast<const AnimationButton*>(get_loaded())->get_pos());
    }

    void AnimationLayout::save_loaded_item(QWidget *item) {
        // First, backup the frames in case of an issue
        backup_timer.stop();
        backupAnimations();

        TemplatedHLayout::save_loaded_item(item);
        backup_timer.start();
    }

    void AnimationLayout::backupAnimations() {
        if (!QDir().mkpath(lib_animation::Animation::BACKUP_LOCATION)) {
            qWarning() << QString::fromStdString(get_name()) << "Failed to create backup location. No backup will be done";
            return;
        }

        const AnimationButton *a_btn = std::bit_cast<const AnimationButton*>(get_loaded());

        // Backup animations and unload those that aren't used
        for (AnimationButton const *btn : m_items) {
            std::scoped_lock<std::mutex> lock(backup_mutex);
            btn->get()->backup();

            // Unload only if not loaded already & if not shown
            if (btn != a_btn && btn->get()->is_loaded())
                btn->get()->unload();
        }
        qDebug() << QString::fromStdString(get_name()) << "Backup successful";
        emit lib_widgets::StatusBar::getInstance()->sendMessage("Backup successful", 5 * 1000);
    }

    void AnimationLayout::clear_backup_animations() {
        if (QDir backupFolder(lib_animation::Animation::BACKUP_LOCATION); !backupFolder.removeRecursively())
            qWarning() << QString::fromStdString(get_name()) << "Failed to remove files from backup location a popup may appear next time the application is launched";
    }

} // namespace lib_library

