/**
 @author Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef IMAGEPASTER_H
#define IMAGEPASTER_H

#include <QRubberBand>
#include <QPair>

namespace lib_canvas {
class ImagePaster : public QRubberBand
{
    Q_OBJECT
public:
    explicit ImagePaster(Shape s, QImage &i, QWidget *p = 0);
    explicit ImagePaster(Shape s, QWidget *p = 0);

    /**
     * @brief get the offset of the image displayed
     * @return a pair containing the two offset coordinates (upper left/lower right)
     */
    QPair<QPoint, QPoint> get_offsets();
protected:
    void paintEvent(QPaintEvent *event);
private:

    /**
     * @brief QImage displayed in the QRubberband
     */
    QImage m_img;

    /**
     * @brief Offset of the original coordinate (upper left)
     */
    int m_begin_offset;

    /**
     * @brief Offset of the last coordinate (lower right)
     */
    int m_end_offset;
};
}

#endif // IMAGEPASTER_H
