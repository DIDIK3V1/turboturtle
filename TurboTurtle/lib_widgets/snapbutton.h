#pragma once

#include <QToolButton>

namespace lib_widgets {

    class SnapButton : public QToolButton {
        Q_OBJECT
    public:
        explicit SnapButton(QWidget *parent = nullptr);

    signals:
        /**
         * @brief snap Signal for normal snap
         */
        void snap();

        /**
         * @brief snap_and_new_frame Signal for snap and new frame
         */
        void snap_and_new_frame();
    };
}
