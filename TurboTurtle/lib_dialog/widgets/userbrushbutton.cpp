/**
 @author Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "userbrushbutton.h"
#include "QFile"

namespace lib_dialog {

    userBrushButton::userBrushButton(const QString image_path, QWidget *parent) :
    QPushButton(parent)
    , path(image_path)
    {
        this->setIcon(QIcon(image_path));
    }

    void userBrushButton::DeleteImg()
    {
        QFile::remove(this->path);
    }

} // lib_dialog