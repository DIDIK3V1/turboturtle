/**
 @authors Creerio, Katarina, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "lib_widgets/templates/templatedhlayouttools.h"
#include "lib_timeline/widgets/framebutton.h"
#include "lib_canvas/frame.h"
#include "qcheckbox.h"

namespace lib_timeline {

    class TimelineTools : public lib_widgets::TemplatedHLayoutTools<lib_timeline::FrameButton, lib_frame::Frame> {
    Q_OBJECT

    public:
        explicit TimelineTools(QWidget *parent = nullptr);

        /**
         *  the function add and duplicate
         *  override these function from the parent class
         */
        void add();

        const int duplicate();

        void remove();

        /**
         * @brief return the value of m_is_playing
         * @return m_is_playing
         */
        const bool is_playing() const;

        /**
         * @brief is called when m_btn_play is clicked to start or stop playing the animation
         * using m_check_loop to indicate if the animation loop or not
         */
        void start_stop_animation();

        /**
         * @brief is called to reset all parameter to indicate that the animation is not playing
         */
        void end_animation();

        /**
         * @brief allow to enable or disable the possibility to play the animation
         * @param disabled if true, then the play is disabled
         */
        void enable_disable_play(const bool disabled);

    private:

        /**
         * @brief the button used to start and stop playing the animation
         */
        QPushButton m_btn_play;

        /**
         * @brief the checkbox used to indicate if the animation must loop
         */
        QCheckBox m_check_loop;

        /**
        * @brief m_ins_playing boolean to know if the animation in the timeline is playing
        */
        bool m_is_playing;

        /**
         * @brief keep the value of m_check_loop to re-set it after the play
         */
        bool m_is_looping;

        /**
         * @brief m_play_shortcut for the play button in the timepline
         */
        QShortcut m_play_shortcut;

    signals:

        /**
         * @brief signal send to TimelineLayout to start the animation
         * @param loop boolean that indicate if the animation must loop
         */
        void launch_animation(bool loop);

        /**
         * @brief signal send to TimelineLayout to stop the animation
         */
        void stop_animation();


    };

}// namespace lib_timeline
