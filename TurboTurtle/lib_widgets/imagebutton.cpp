/**
 @author Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "imagebutton.h"

#include <QDrag>
#include <utility>
#include "qevent.h"
#include "qmimedata.h"

namespace lib_widgets {
    ImageButton::ImageButton(QString mime_type, QWidget *parent, const int pos) : QPushButton(parent), position(pos), m_mime_type(std::move(mime_type))
    {
        // No border style on image buttons by default
        this->setStyleSheet("border: 0px");
    }

    inline const QImage ImageButton::get_thumbnail() const {
        return m_thumbnail;
    }

    void ImageButton::set_thumbnail(const QImage &thumbnail) {
        m_thumbnail = thumbnail;

        int w = 128; // TODO : improve that
        int h = 67  ;

        QPixmap pixmap = QPixmap::fromImage(thumbnail).scaled(w-5,h-5,Qt::KeepAspectRatio);
        this->setIconSize(QSize(w,h));
        this->setIcon(QIcon(pixmap));
        this->setFixedSize(w,h);
        this->setFlat(false);
        qDebug() << QString::fromStdString(get_name()) << "updated thumbnail";
    }

    const int ImageButton::get_pos() const {
        return position;
    }

    void ImageButton::set_pos(const size_t &new_pos) {
        position = int(new_pos);
    }

    void ImageButton::set_border(const bool &enabled) {
        if(enabled){
            //Get the right color for the border
            QColor tmp_color=this->palette().color(QPalette::Highlight);
            std::stringstream ss;
            ss << "border: 2px solid rgb(" << tmp_color.red() << "," << tmp_color.green() << "," << tmp_color.blue() << ");";

            this->setStyleSheet(QString::fromStdString(ss.str()));
            qDebug() << QString::fromStdString(get_name()) << "enabled border";
        }
        else{
            this->setStyleSheet("border: 0px");
            qDebug() << QString::fromStdString(get_name()) << "disabled border";
        }
    }

    const QString ImageButton::get_mime_type() const {
        return m_mime_type;
    }

    void ImageButton::mouseReleaseEvent(QMouseEvent *e)
    {
        // Selection initiated
        if (e->button() == Qt::LeftButton)
        {
            // CTRL + Left click
            if (e->modifiers() == Qt::ControlModifier) {
                emit on_ctrl_left_clicked(position);
            }
            // Single left click
            else {
                emit on_left_clicked(position);
                m_dragStartPosition = e->pos();
            }
        }
        else if (e->button() == Qt::RightButton)
        {
            emit on_right_click(e->globalPosition(), position);
        }
        else if (e->button() == Qt::MiddleButton)
        {
            emit on_middle_click(position);
        }
    }

    void ImageButton::mouseMoveEvent(QMouseEvent *e)
    {
        // Drag&Drop initiated
        if (!(e->buttons() & Qt::LeftButton))
            return;
        if ((e->pos() - m_dragStartPosition).manhattanLength()
            < QApplication::startDragDistance())
            return;

        QDrag drag(this);
        QMimeData *mime_data = new QMimeData();

        QByteArray button_data;
        QDataStream data_stream(&button_data, QIODevice::WriteOnly);
        data_stream << position;
        mime_data->setData(get_mime_type(), button_data);

        drag.setMimeData(mime_data);
        drag.setPixmap(QWidget::grab(this->rect()));
        drag.exec(Qt::MoveAction);
        qDebug() << QString::fromStdString(get_name()) << "initiated drag and drop";
    }

}

