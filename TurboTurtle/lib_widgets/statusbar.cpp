/**

 @authors Creerio

 @section License

 Copyright (C) 2024 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "statusbar.h"

namespace lib_widgets {

    StatusBar* StatusBar::m_instance = nullptr;

    StatusBar* StatusBar::getInstance() {
        return m_instance;
    }

    StatusBar::StatusBar(QWidget *parent) : QStatusBar(parent) {
        m_instance = this;

        connect(this, &StatusBar::sendMessage, this, &QStatusBar::showMessage);
    }
} // lib_widgets