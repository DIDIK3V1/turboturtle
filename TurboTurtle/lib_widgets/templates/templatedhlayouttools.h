/**
 @author Creerio

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <QWidget>
#include "lib_widgets/hlayouttools.h"
#include "lib_widgets/templates/templatedhlayout.h"

namespace lib_widgets {
    /**
     * @brief Templated HLayoutTools, to be used with either Frames or Animations
     */
    template<class Stored, class ToGive>
    class TemplatedHLayoutTools : public HLayoutTools
    {
    public:
        /**
         * @brief Default constructor of the template
         * @param parent Parent QWidget
         */
        explicit TemplatedHLayoutTools(QWidget *parent = nullptr);

        /**
         * @brief Used to set the linked m_layout
         * @param layout Layout used for the add, duplicate and remove actions
         */
        void set_layout(TemplatedHLayout<Stored, ToGive> *layout);

        /**
         * @brief Called when the add button is clicked
         * Adds a Frame/Animation to the Layout
         */
        virtual void add() = 0;

        /**
         * @brief Called when the duplicate button is clicked
         * Duplicates the selected frame
         * @return The position of the last inserted element. If -1, it shouldn't be used to load an item
         */
        virtual const int duplicate();

        /**
         * @brief Called when the delete button is clicked
         * Deletes the selected frame
         */
        virtual void remove();

    protected:
        /**
         * @brief Layout used with this class, when adding, deleting and removing
         */
        TemplatedHLayout<Stored, ToGive> *m_layout = nullptr;

    private slots:
        /**
         * @brief Slot called when clicking on the Add button
         */
        void on_btnAdd_clicked() final;

        /**
         * @brief Slot called when clicking on the Delete button
         */
        void on_btnDelete_clicked() final;

        /**
         * @brief Slot called when clicking on the Duplicate button
         */
        void on_btnDuplicate_clicked() final;
    };
}

#include "templatedhlayouttools.tpp"
