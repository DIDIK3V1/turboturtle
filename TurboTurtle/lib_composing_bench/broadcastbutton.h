/**
 @authors Idaïa METZ, Julien BLANCO, Kévin DIDIER, Lucas PLANCHET, Samuel GOUBEAU, Creerio, Katarina

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BROADCASTBUTTON_H
#define BROADCASTBUTTON_H

#include <QObject>
#include <QPixmap>
#include <QPushButton>
#include "lib_animation/animation.h"
#include <opencv2/core.hpp>

namespace lib_composing_bench {

/**

@brief The BroadcastButton class is a custom QPushButton used in the library to display in the composing bench animations.

The BroadcastButton class extends QPushButton.*/

class BroadcastButton : public QPushButton
{
    Q_OBJECT

    /**
    * @brief The image displayed inside the button.
    */
    QImage m_image;

    /**
    * @brief The ID of the associated animation.
    */
    size_t m_anim_id;
    /**
    * @brief The Animation object associated with the button.
    */
    lib_animation::Animation *m_anim;
    /**
    *@brief Resizes the button to fit the image.
    */
    void resize();

public:
    /**
    *@brief Constructs a BroadcastButton object with a specified Animation and ID.
    *@param anim The Animation pointer associated with the button.
    *@param id The ID of the associated animation.
    *@param parent Parent widget of the button.
    */
    BroadcastButton(lib_animation::Animation *anim, size_t id, QWidget *parent = nullptr);

    /**
    *@brief Sets the ID of the BroadcastButton.
    *@param id The new ID to set.
    */
    void set_id(size_t id);

    /**
    *@brief Returns the ID of the BroadcastButton.
    *@return The ID of the BroadcastButton.
    */
    inline size_t get_id() { return m_anim_id; };

    /**
    *@brief Sets the thumbnail image displayed in the BroadcastButton.
    *@param thumbnail The QImage to set as the thumbnail image.
    */
    void set_thumbnail(const QImage &thumbnail);

    /**
    *@brief Sets whether the BroadcastButton has a border.
   *@param b True if the BroadcastButton has a border, false if it does not.
   */
    void set_border(bool b);

    /**
    *@brief Returns the Animation pointer associated with the BroadcastButton.
    *@return The Animation object associated with the BroadcastButton.
    */
    lib_animation::Animation *get_animation();

private slots:
    /**
    * @brief Handles the mouse press event for the BroadcastButton.
    * @param e The mouse press event.
    */
    void mousePressEvent(QMouseEvent *e);

    /**
    * @brief Handles the mouse move event for the BroadcastButton.
    * @param e The mouse move event.
    */
    void mouseMoveEvent(QMouseEvent *e);
signals:
    /**
    *@brief Signal emitted when the BroadcastButton is left-clicked.
    *@param id The ID of the BroadcastButton.
    */
    void on_left_clicked(size_t id);
};
} // namespace lib_composing_bench

#endif // BROADCASTBUTTON_H
