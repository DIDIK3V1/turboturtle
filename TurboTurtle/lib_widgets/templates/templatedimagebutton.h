/**
 @authors Creerio, Aurélien LAPLAUD

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "lib_widgets/imagebutton.h"

namespace lib_widgets {
    /**
     * @brief Template class storing either an Animation or a Frame
     * @extends QPushButton
     * @tparam ToStore Animation or Frame
     */
    template<class ToStore>
    class TemplatedImageButton : public ImageButton {
    public:
        // Disable child induced constructors
        TemplatedImageButton(QWidget *parent = nullptr) = delete;

        /**
         * @brief Constructor of an ImageButton
         * @param item Item to store, either an Animation or a Frame
         * @param pos Position of the item stored, can be omitted to use -1
         * @param parent Parent of the class, can be omitted to have no parent
         */
        explicit TemplatedImageButton(ToStore *item, const QString &mime_type = "", const int pos = -1, QWidget *parent = nullptr);

        /**
         * @brief Returns the stored object in the ImageButton
         * @return An Animation or Frame
         */
        ToStore* get() const;

        virtual const std::string get_name() const = 0;


    private:
        /**
         * @brief Item stored, either an Animation or a Frame
         */
        ToStore *m_stored;
    };
}

#include "templatedimagebutton.tpp"
