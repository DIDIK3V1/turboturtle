/**
 @authors Creerio, Katarina, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "timelinetools.h"
#include "timelinelayout.h"
#include "qmessagebox.h"
#include "lib_canvas/canvas.h"

namespace lib_timeline {

    TimelineTools::TimelineTools(QWidget *parent) :
            TemplatedHLayoutTools(parent),
            m_btn_play("Play", this),
            m_check_loop("Loop", this),
            m_is_looping(false),
            m_is_playing(false),
            m_play_shortcut(QKeySequence("0"), this)
    {
        // Set QPushButton settings before adding it
        m_btn_play.setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        m_btn_play.setMinimumSize(52, 24);
        m_btn_play.setMaximumSize(52, 24);

        connect(&m_btn_play, &QPushButton::released, this, &TimelineTools::start_stop_animation);

        // Same with loop
        m_check_loop.setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        m_check_loop.setMinimumSize(52, 24);
        m_check_loop.setMaximumSize(52, 24);

        addWidget(&m_btn_play);
        addWidget(&m_check_loop);

        // Bind buttons to actions
        addWidgetShortcut({ QKeySequence("f") }, "[f]");

        duplicateWidgetShortcut({ QKeySequence("7") }, "[7]");

        deleteWidgetShortcut({ QKeySequence(Qt::Key_Delete), QKeySequence("9") }, "[Delete] or [9]");


        connect(&m_play_shortcut, &QShortcut::activated, this, &TimelineTools::start_stop_animation);
        m_btn_play.setToolTip("[0]");

        m_check_loop.setShortcut(QKeySequence("8"));
        m_check_loop.setToolTip("[8]");


    }

    void TimelineTools::add()
    {
        if(!m_is_playing){
            // Get size of the canvas
            const QSize &image_size = lib_canvas::Canvas::s_canvas_size;
            // One element should be selected
            lib_frame::Frame* new_img = new lib_frame::Frame(Qt::white);
            // Resize new frame to match canvas size
            new_img->resize(image_size);
            m_layout->add(new_img);

            // Select and load added item
            m_layout->clear_selection();
            (std::bit_cast<TimelineLayout*>(m_layout))->load_last_frame();
        }
    }

    const int TimelineTools::duplicate()
    {
        if(!m_is_playing) {
            const int pos = TemplatedHLayoutTools::duplicate();

            // This is a frame, it should be loaded
            if (pos >= 0) {
                m_layout->clear_selection();
                (std::bit_cast<TimelineLayout*>(m_layout))->load_frame(pos - 1); // -1 to stay within the storage bounds
            }

            return pos;
        }

        return -1;
    }

    void TimelineTools::remove() {
        if(!m_is_playing) {
            QMessageBox msgBox;
            msgBox.setText("Do you want to remove the currently selected frame(s)?");
            msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msgBox.setDefaultButton(QMessageBox::No);
            if (int ret = msgBox.exec(); ret != QMessageBox::Yes)
                return;
            TemplatedHLayoutTools::remove();
        }
    }


    void TimelineTools::start_stop_animation()
    {
        if(!m_is_playing) {
            m_is_playing = true;
            m_btn_play.setText("Stop");
            m_is_looping = m_check_loop.isChecked();
            emit launch_animation(m_is_looping);
            m_check_loop.setCheckable(false);
        } else {
            end_animation();
            emit stop_animation();
        }
    }

    const bool TimelineTools::is_playing() const
    {
        return m_is_playing;
    }

    void TimelineTools::end_animation()
    {
        m_is_playing = false;
        m_btn_play.setText("Play");
        m_check_loop.setCheckable(true);
        m_check_loop.setChecked(m_is_looping);
    }

    void TimelineTools::enable_disable_play(const bool disabled){
        m_btn_play.setDisabled(disabled);
    }

}// namespace lib_timeline
