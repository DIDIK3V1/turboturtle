/**
 @authors Creerio, Aurélien LAPLAUD, Alexis BOUE

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QProcess>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include "animationmenu.h"

namespace lib_library {
    AnimationMenu::AnimationMenu(AnimationButton *parent) : QMenu((QWidget*)parent), m_btn(parent), m_layout(m_btn->layout), m_loaded(std::bit_cast<const AnimationButton*>(m_layout->get_loaded())) {
        // Actions allowed
        this->addSection("Library");
        this->addAction("Insert new before", this, &AnimationMenu::insert_before);
        this->addAction("Insert new after", this, &AnimationMenu::insert_after);
        this->addAction("Duplicate", this, &AnimationMenu::duplicate);
        this->addAction("Delete", this, &AnimationMenu::remove);

        this->addSection("Animation");
        this->addAction("Load", this, &AnimationMenu::load);
        this->addAction("Save to PNG", this, &AnimationMenu::save_png);
        this->addAction("Save to GIF", this, &AnimationMenu::save_gif);
        this->addAction("Save to MP4", this, &AnimationMenu::save_mp4);

        // Unselect loaded item
        parent->layout->unselect(m_loaded->get_pos());

        // Unselect others
        int selected = parent->layout->get_current_pos();
        while (selected != -1) {
            parent->layout->unselect(selected);
            selected = parent->layout->get_current_pos();
        }
    }

    AnimationMenu::~AnimationMenu() {
        if (m_btn != nullptr)
            m_btn->set_border(false);
        if (m_loaded != nullptr) {
            m_layout->clear_selection();
            m_layout->select(m_loaded->get_pos());
        }
    }

    void AnimationMenu::insert_before() {
        m_layout->insert(lib_animation::Animation::new_animation(), m_btn->get_pos());
    }

    void AnimationMenu::insert_after() {
        m_layout->insert(lib_animation::Animation::new_animation(), m_btn->get_pos() + 1);
    }

    void AnimationMenu::duplicate() {
        m_layout->insert(new lib_animation::Animation(*m_btn->get()), m_btn->get_pos() + 1);
    }

    void AnimationMenu::remove() {
        if (m_layout->get_loaded() == m_btn) {
            QMessageBox::warning(m_btn, "Cannot remove selected animation", "This animation is currently loaded, it cannot be removed. Please select another one, then delete this one.");
            return;
        }

        m_layout->remove(m_btn->get_pos());
        m_btn = nullptr;
    }

    void AnimationMenu::load() {
        emit m_layout->load_item(m_btn);
        m_loaded = m_btn;
    }

    void AnimationMenu::save_png() {
        QString save_folder = QFileDialog::getExistingDirectory(nullptr, "Select a folder where the animation will be saved", QDir::homePath());
        if (save_folder.isEmpty()) {return;}

        // Launch in another thread to avoid a GUI freeze
        QFuture<void> fut = QtConcurrent::run([this, save_folder]() {
            m_btn->get()->save_to_png(save_folder + "/" + QString::fromStdString(m_btn->get()->get_name()));
        });
    }

    void AnimationMenu::save_gif() {
        //images URL
        QString output_file = QFileDialog::getSaveFileName(nullptr,                     //Parent widget
                                                         tr("Save animation"),          //Dialog title
                                                         QDir::homePath(),              //Base directory
                                                         tr("Animation (*.gif)"));      //File filter
        if(output_file.isEmpty()) return;

        if (!output_file.endsWith(".gif", Qt::CaseInsensitive))
            output_file.append(".gif");

        double animation_framerate = QInputDialog::getDouble(nullptr,                   //parent widget
                                                             tr("animation framerate"), //title
                                                             tr("framerate: "),         //input label
                                                             10,                        //default framerate
                                                             0,                         //minimum framerate
                                                             1000,                      //maximum framerate
                                                             2);                        //decimal precision

        m_btn->get()->save_to_gif(output_file, animation_framerate);
    }

    void AnimationMenu::save_mp4() {
        QString output_file = QFileDialog::getSaveFileName(nullptr,                   //Parent widget
                                                           tr("Save video"),          //Dialog title
                                                           QDir::homePath(),          //Base directory
                                                           tr("Video (*.mp4)"));      //File filter
        if(output_file.isEmpty()) return;

        if (!output_file.endsWith(".mp4", Qt::CaseInsensitive))
            output_file.append(".mp4");

        double animation_framerate = QInputDialog::getDouble(nullptr,                   //parent widget
                                                             tr("animation framerate"), //title
                                                             tr("framerate: "),         //input label
                                                             25,                        //default framerate
                                                             0,                         //minimum framerate
                                                             1000,                      //maximum framerate
                                                             2);                        //decimal precision

        m_btn->get()->save_to_mp4(output_file, animation_framerate);
    }
}
