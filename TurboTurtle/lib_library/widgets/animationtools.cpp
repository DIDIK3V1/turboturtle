/**
 @author Creerio

 @section License

 Copyright (C) 2023 TurboTurtle Contributors

 This file is part of TurboTurtle.
 TurboTurtle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TurboTurtle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TurboTurtle.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "animationtools.h"

namespace lib_library {
    AnimationTools::AnimationTools(QWidget *parent) : TemplatedHLayoutTools(parent) {
        remove_delete();
        remove_frame();

        // Bind buttons to actions
        addWidgetShortcut({ QKeySequence("Ctrl+N") } , "[Ctrl+N]");
    }

    void AnimationTools::add() {
        m_layout->add(lib_animation::Animation::new_animation());
        m_layout->clear_selection();
        m_layout->select(std::bit_cast<const AnimationButton*>(m_layout->get_loaded())->get_pos());
    }

}
