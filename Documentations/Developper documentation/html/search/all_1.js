var searchData=
[
  ['back_0',['back',['../classlib__color__selector_1_1_color_preview_1_1_private.html#ab3f7b83efb10382d3977485b2efccf0e',1,'lib_color_selector::ColorPreview::Private']]],
  ['background_1',['background',['../structlib__frame_1_1_frame.html#a60a17dde4924782c05632148d7c6c887',1,'lib_frame::Frame::background()'],['../classlib__color__selector_1_1_color_preview.html#ab2ffd65415f0051840d3d8ce06b57b5f',1,'lib_color_selector::ColorPreview::background()'],['../classlib__color__selector_1_1_gradient_slider.html#a03ae9dd2c0e10eb129ed64a38f59f1fb',1,'lib_color_selector::GradientSlider::background()']]],
  ['broadcastbutton_2',['BroadcastButton',['../classlib__composing__bench_1_1_broadcast_button.html#af6ffb5636c2a3f6108255abf37d11c80',1,'lib_composing_bench::BroadcastButton::BroadcastButton()'],['../classlib__composing__bench_1_1_broadcast_button.html',1,'lib_composing_bench::BroadcastButton']]],
  ['broadcaster_3',['BroadCaster',['../classlib__broadcast_1_1_broad_caster.html#a5be9348c8dacad081cd09238e1166ad9',1,'lib_broadcast::BroadCaster::BroadCaster(QObject *parent=nullptr)'],['../classlib__broadcast_1_1_broad_caster.html#a78e1a801f7de2b6b25a16d8ea0b75af5',1,'lib_broadcast::BroadCaster::BroadCaster(const BroadCaster &amp;)=delete'],['../classlib__broadcast_1_1_broad_caster.html',1,'lib_broadcast::BroadCaster']]],
  ['brush_4',['Brush',['../classlib__drawing__tools_1_1_brush.html#a8c1d6b65fbae155bbe47818671af03e1',1,'lib_drawing_tools::Brush::Brush(QImage img, int nb_var=10)'],['../classlib__drawing__tools_1_1_brush.html#ae8946260e02488e00630ae8b109a68e2',1,'lib_drawing_tools::Brush::Brush(const Brush &amp;brush)'],['../classlib__drawing__tools_1_1_brush.html',1,'lib_drawing_tools::Brush']]],
  ['brush_5fborder_5',['brush_border',['../classlib__drawing__tools_1_1_brush.html#ab5ba0aceb0a5317382ae6875058fc891',1,'lib_drawing_tools::Brush']]],
  ['brush_5fchanged_6',['brush_changed',['../classlib__canvas_1_1_canvas.html#adc055c4c2723629e6676dafb213668c6',1,'lib_canvas::Canvas']]],
  ['brushchange_7',['brushChange',['../classlib__drawing__tools_1_1_drawing_tools.html#a9098064b318bbc3da53d900355990050',1,'lib_drawing_tools::DrawingTools']]],
  ['button_5fclicked_8',['button_Clicked',['../classlib__drawing__tools_1_1_drawing_tools.html#ab7caf16e0a697ca0b5f2433926fdfd60',1,'lib_drawing_tools::DrawingTools']]]
];
